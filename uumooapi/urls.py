
from django.conf.urls import url , include
from uumooapi import views


urlpatterns = [
    #url(r'' , views.pocketSignUp) ,


    #=================== for web ====================================================
    url(r'web/dashboard-data/$' , views.webDasbboardDataApi ) ,
    url(r'web/pocket-list/$', views.webPocketUserslListDataApi ) , 
    url(r'web/drawer-list/$' , views.webDrawerDrawerUserListDataApi ) ,
    url(r'web/key-list/$', views.webAesKeyListDataApi ) ,
    url(r'web/service-fee-list/$', views.webServiceFeesDataApi ),
    url(r'web/service-fee-edit-action/$' , views.serviceFeeEditActionAdmin  ) ,
    url(r'web/pocket-user-profile-data/$', views.webPocketUserProfile ) ,
    url(r'web/overall-trasaction-graph-data/$' , views.webTrasactionsOverallGraph ) ,



    #=================== postgresql conection context ================================
    url(r'web/' , include('uumooadmin.urls')  ) ,
    

    url(r'v2/ocr/$' , views.TestOcr ) , 
    url(r'v2/ocr-reader/$', views.nidOCR ),
    url(r'v2/api-test/$' , views.testAPI ) ,

    #AES codes	
    url(r'v2/data-add/$', views.dataAdd),
    url(r'v2/push-data/$', views.pushData ),



    url(r'v2/division-district-thana/$' , views.divisionDistrictThanaList ) ,
    url(r'v2/b0dfcc659f1b9623f92d302e585ee62875/$', views.updateExistAesKey ) ,

    #====================================== aes codes list ========================================
    url(r'v2/E46E1A5357D1978E238331E5BA4EE484/$' , views.getAESAPIList ) ,


    #UI for api rest	
    url(r'v1/pocket-registration/$' , 		views.pocketSignUpUI), #pocket reg ui
    url(r'v1/pin-verification/$' , 		views.pocketPinVerificationUI), #pin verification ui
    url(r'v1/transaction-pin-verification/$' ,  views.pocketTransactionPINVerificationUI), #transaction pin verification ui
    url(r'v1/pocket-login/$'		 ,      views.pocketLoginUI) , #pocket login ui
    url(r'v1/pocket-basic-profile/$' 	, 	views.pocketBasicProfileUI) , #pocket basic profile  ui
    url(r'v1/pocket-address-profile/$'  , 	views.pocketAddressProfileUI) , #pocket address profile  ui
    url(r'v1/pocket-nominee-profile/$'  , 	views.pocketNomineeProfileUI) , #pocket nominee profile  ui






    #api test responses	
    url(r'v2/pocket-registration/$' , 			views.pocketSignUp) , #pocket reg api
    url(r'v2/pin-verification/$' , 			views.pocketPinVerification) , #pin verification api
    url(r'v2/transaction-pin-verification/$' , 		views.pocketTransactionPinVerification) , #transaction pin verification api    
    url(r'v2/pocket-login/$' 			, 	views.pocketLogin) , #pocket login api
    url(r'v2/pocket-basic-profile/$' , 		  	views.pocketBasicProfile), #pocket profile update api
    url(r'v2/pocket-address-profile/$' , 	  	views.pocketAddressProfile), #pocket address profile update api
    url(r'v2/pocket-nominee-profile/$' ,            	views.pocketNomineeProfile), #pocket nominee profile update api
    url(r'v2/pocket-nominee-profile-information/$' , 	views.pocketNomineeInfo), #pocket nominee profile information api



    url(r'v2/balance-transfer/$', 		views.balanceTransfer), #balance transfer api
    url(r'v2/pocket-image-upload/$', 		views.imageFileseInformationUpdate) , #pocket image upload information update api	
    url(r'v2/cashdrawer-image-upload/$',        views.imageFileseInformationUpdateCashbox),
    url(r'v2/pocket-qr-code-scan/$' , 		views.qrCodeScan) , #pocket qr code scan api
    url(r'v2/pocket-account-deactivate/$' , 	views.threeTimeWrongPassword) , #pocket account deactivate (3 times wrong password type) api
    

    #transaction part	
    #url(r'v2/balance-transfer/$', 		views.balanceTransfer), #balance transfer api  	
    url(r'v2/balance-pay/$',                    views.balancePay), #balance pay api


    url(r'v2/balance-topup-pocket-request-to-cashbox/$', views.balanceTopUpWalletRequestToMerchant), 
    #balance topup wallet request to merchant api


    url(r'v2/balance-cashout/$' , views.cashOutRequest ),
    url(r'v2/balance-query/$',    views.customerCurrentBalance ),



    url(r'v2/transaction-list/$' ,   views.transactionList),
    url(r'v2/qr-code-pdf-version/$', views.qrCodePdfVersion),


    #forgot app password	
    url(r'v2/forgot-password/$' ,       views.forgotPassword) ,
    url(r'v2/forgot-password-update/$', views.forgotPasswordUpdate) , 


    #forgot transaction password
    url(r'v2/forgot-transaction-pin/$',		 views.forgotTransactionPin ),
    url(r'v2/forgot-transaction-pin-update/$' ,  views.forgotTransactionPINUpdate) ,

    #======= cashdrawer finder locations =============================
    url(r'v2/cashdrawer-finder-locations/$', views.drawersGeoLocations ) , 


    #=========================forgot both pocket======================
    url(r'v2/forgot-both/$', 			 views.forgotBoth ),

    #forgot both drawer
    url(r'v2/forgot-both-drawer/$' , views.forgotBothDrawer),


    #cashdrawer  registration
    url(r'v2/cashdrawer-registration/$', 			views.cashboxSignUp),	 
    url(r'v2/cashdrawer-pin-verification/$', 			views.cashboxPinVerification ),
    url(r'v2/cashdrawer-transaction-pin-verification/$' , 	views.cashboxTransactionPinVerification ),
    url(r'v2/cashbox-login/$' , 				views.cashboxLogin ), # cashbox login api



    #drawer change password
    url(r'v2/cashdrawer-change-password/$',			 views.drawerChangePassword),
    url(r'v2/cashdrawer-change-password-update/$',               views.drawerChangePasswordUpdate) ,


    #drawer change transaction pin
    url(r'v2/cashdrawer-change-transaction-pin/$' , views.drawerChnageTransactionPIN ) ,
    url(r'v2/cashdrawer-change-transaction-pin-update/$', views.drawerChangeTransactionPINUpdate ),





    #drawer qr code scan api 
    url(r'v2/cashdrawer-qr-code-scan/$', 			views.drawerQRCodeScan ),


    #drawer balance api(with db sql function)
    url(r'v2/drawer-balance-query/$', 				views.drawerCurrentBalance ),

   
    #drawer refund api
    url(r'v2/cashdrawer-withdraw-refund-request/$', 		views.withdrawRejectOrRefund ),


    #drawer deposit accept or reject api
    url(r'v2/cashdrawer-deposit-merchant-request/$', 		views.cashboxDepositAccept ), 


    #drawer profile update api 
    url(r'v2/cashdrawer-basic-profile/$', views.cashboxBasicProfile ),
    url(r'v2/cashdrawer-address-profile/$', views.cashboxAddressProfile ) ,
    url(r'v2/cashdrawer-nominee-profile/$' , views.drawerNomineeInformationUpdate ),    



    #drawer business profile 
    url(r'v2/cashdrawer-business-profile-create/$', 	 views.drawerBusinessProfileCreate ) ,
    url(r'v2/cashdrawer-business-info-create/$',	 views.drawerBusinessInfoUpdate ) ,
    url(r'v2/cashdrawer-business-info-image-upload/$',   views.cashboxBusinessProfileImageUpload ),


    url(r'v2/cashdrawer-nominee-information/$', views.drawerNomineeInformation ) ,

    #drawer transaction api
    url(r'v2/drawer-payment/$' ,  views.drawerPayment ) ,
    url(r'v2/drawer-deposit/$' ,  views.drawerDeposit ),
    url(r'v2/drawer-withdraw/$' , views.drawerWithdraw),


    #pocket log api
    url(r'v2/pay-log-data/$', 	   views.payLogData) , #pay Log data api 
    url(r'v2/transfer-log-data/$', views.transferLogData ), #transfer Log data api
    url(r'v2/cashout-log-data/$',  views.cashoutLogData ), # cashout Log data api
    url(r'v2/deposit-log-data/$' , views.topupLogData), #topup or deposit data api 

    #drawer log api

    #merchant pay log as drawer
    url(r'v2/drawer-incoming-pay-log-data/$',       views.drawerIncomingPayLogData ),
    #merchant deposit log as drawer	 
    url(r'v2/drawer-incoming-deposit-log-data/$',   views.drawerIncomingDepositLogData ),
    #merchant withdraw log ad drawer
    url(r'v2/drawer-incoming-withdraw-log-data/$',  views.drawerIncomingWithdrawLogData ),




    #merchant pay log as personal	
    url(r'v2/drawer-outgoing-pay-log-data/$',       views.drawerOutgoingPayLogData ),
    #merchant deposit log as personal
    url(r'v2/drawer-outgoing-deposit-log-data/$',   views.drawerOutgoingDepositLogData ),
    #merchant withdraw log as personal
    url(r'v2/drawer-outgoing-withdraw-log-data/$' , views.drawerOutgoingWithdrawLogData ),



    #contact list number check from uumodatabase	
    url(r'v2/contact-list-number-check/$' , 	   views.balanceTransferFromContactListCheckNumber ) , 
    url(r'v2/fcm-token-update/$', 		   views.fcmTokenUpdate ), #update fcm token 
    url(r'v2/resample-image/$', 		   views.profileImageReDefined),





    #for test
    url(r'v2/sample-qr/$', views.sampleQR), #test for qr with logo checking,	
    
    url(r'v2/sample-notification/$', views.sampleNotification ),
    #.cashboxNotification ) , # views.sampleNotification), #sample push notification with fcm  

    url(r'v2/common-notification/$', views.sendCommonNotification),


    #======================== test =============================================
    url(r'v2/enc/$' , views.dbEncryptTest),
    url(r'v2/dec/$' , views.dbDecryptTest),
    url(r'v2/customer-name/$' , views.customerNameDetail) ,   

    url(r'v2/check-header-enc/$', views.checkHeaderInfo1) ,
    url(r'v2/check-header-dec/$', views.checkHeaderInfo2) ,


]

