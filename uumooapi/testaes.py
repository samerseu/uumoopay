import importlib
from Crypto.Cipher import AES
import base64
import hashlib
import sys

importlib.reload(sys)

class PyAesCrypt(object):
    def __init__(self,iv=None,encoding=True):
        self.iv = iv
        self.encoding = encoding

    def encrypt(self,key,message):
        cipher = AES.new(key=self._hashkey(key=key))
        cipher_text = cipher.encrypt(self.pkcs7padding(data=message))
        if self.encoding:
            return base64.b64encode(s=cipher_text)
        return cipher_text

    def pkcs7padding(self,data):
        bs = 256 #8
        padding = bs - len(data) % bs
        padding_text = chr(padding) * padding
        return data + padding_text

    def pkcs7decode(self, text):
        if type(text) is bytes:
            pad = ord( text.decode("utf-8" ,errors="ignore" )[-1])
            return text[:-pad]
        else:
            raise RuntimeError("bytes required found %s" % type(text))

    def _hashkey(self,key):
        return hashlib.sha256(key.encode()).digest()

    def decrypt(self,key,message):
        cipher = AES.new(key=self._hashkey(key=key))
        if self.encoding:
            resp = cipher.decrypt(ciphertext=base64.b64decode(s=message)) #.decode("utf-8" , errors="ignore"))
        else:
            resp = cipher.decrypt(ciphertext=message)
        return self.pkcs7decode(text=resp)


"""	

    def hell(enc_cipher):
	decodetext =  base64.b64decode(enc_cipher)
	aes = AES.new(key, AES.MODE_CBC, iv)
	cipher = aes.decrypt(decodetext)
	pad_text = encoder.decode(cipher)



import sys
import base64
from Crypto.Cipher import AES


class AESCipher(object):
    def __init__(self, key):
        self.bs = 16
        self.cipher = AES.new(key, AES.MODE_ECB)

    def encrypt(self, raw):
        raw = self._pad(raw)
        encrypted = self.cipher.encrypt(raw)
        encoded = base64.b64encode(encrypted)
        return str(encoded, 'utf-8')

    def decrypt(self, raw):
        decoded = base64.b64decode(raw)
        decrypted = self.cipher.decrypt(decoded)
        return str(self._unpad(decrypted), 'utf-8')

    def _pad(self, s):
        return s + (self.bs - len(s) % self.bs) * chr(self.bs - len(s) % self.bs)

    def _unpad(self, s):
        return s[:-ord(s[len(s)-1:])]



"""
