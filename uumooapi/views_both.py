#encoding= utf-8
from django.views.decorators.csrf import csrf_exempt


from datetime import datetime , date , timedelta
from urllib.request import *
from django.core import serializers
from django.shortcuts import render
from django.http import JsonResponse
from PIL import Image
from django.db.models import Q
from uumooapi.models import * 
from django.utils import *
from django.core.files import File
from PIL import Image
from resizeimage import resizeimage
import random
import sys
import os
from codecs import encode, decode
import locale
import requests
import qrcode
import pyqrcode
# import QRCode
from uumoogrid.settings import MEDIA_URL

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
import base64
import hashlib
import uuid
import binascii,os
import requests
import psycopg2
import json
import decimal
import os
from fpdf import FPDF
#from simple_aes_cipher import AESCipher, generate_secret_key
from uumooapi.testaes import *

#====================== for test ================================================


@csrf_exempt
def testAes(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		data = r['data']

		key = 'hogefuga34567890hogefuga34567890'
		#cipher = AESCipher(key)
		print("%s"% data)
 
		#decrypted = cipher.decrypt(data)
		#print('Decrypted: %s' % decrypted)
		#assert decrypted == plaintext

		a = PyAesCrypt()
		d = a.decrypt(key , data )	

		print("%s"%    str( d )) #    str(d ).decode( "utf-8") )

		return JsonResponse({'success':'true', 'r': str(d ) })
	else:
		return JsonResponse({'error':'Please check your request method'})


@csrf_exempt
def dbEncryptTest(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		con = psycopg2.connect(
			host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
			database = 'uumoogrid_dev',
			user = 'power_user',
			password= 'PsnT@uumoo2018',
			port = '5432'
		)
		user_str = r['data']
		cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		pass_txt = user_str #request.GET.get("pass") #'12345'
		ukey = 'uumoo'
		rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
		one = cur.fetchone()	
		enc_pass = b"".join( one['pt'])

		print ("Enc: \\x%s" % str( binascii.b2a_hex( enc_pass), 'utf-8') )	

		return JsonResponse({ "success":"true" , "enc": "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8')  })
	else:
		return JsonResponse({'error':'Please check your request method'})
 

@csrf_exempt
def dbDecryptTest(request):
	if request.method == 'POST':
		r = json.loads(request.body)	
		con = psycopg2.connect(
			host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
			database = 'uumoogrid_dev',
			user = 'power_user',
			password= 'PsnT@uumoo2018',
			port = '5432'
		)
		ukey = 'uumoo'
		enc_pass = str(r['data'])
		curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curr.execute("select convert_from(decrypt('"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
		two = curr.fetchone()
		data = str(two[0])
		return JsonResponse({'success':'true' , 'dec': data })
	else:
		return JsonResponse({'error':'Please check your request method'})

#====================== for test=================================================



@csrf_exempt
def imageFileseInformationUpdateCashbox(request):
	if request.method == 'POST':
		#r = json.loads(request.body)
		customer_mobile = request.POST.get('customer_mobile')	
		image_type = request.POST.get('image_type')
		pocket_image = request.FILES['avatar'].name  #r['pocket_image')
		extension = pocket_image.split(".")[-1]
	

		if(not customer_mobile):
			return JsonResponse({'error':'Mobile Number must not be Empty.'})	
		elif(not image_type):
			return JsonResponse({'error':'Image Type must not be Empty.'})
		elif(not pocket_image):
			return JsonResponse({'error':'Image must not be Empty.'})
		else:
			token_generate = '{0:06}'.format(random.randint(1,10000000))			
			salt = uuid.uuid4().hex
			img =  hashlib.sha256(salt.encode() + token_generate.encode()).hexdigest()+'.'+extension
			handle_uploaded_file(request.FILES['avatar'] , img)

			counter = Cashbox.objects.filter(cashbox_mobileno__exact = customer_mobile ).select_related()

			if(counter.count() > 0):
				customer_id = counter[0].cashbox_customer_id
				if(image_type == 'profile_image'):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_photo=img)
				#elif(image_type == 'nid'):
				#	Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(pocket_nid_image=img)
				#elif(image_type == 'nominee_nid'):
				#	Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(pocket_nominee_voterid_scancopy=img)
				else:
					return JsonResponse({'error':'Image type error'})

				
				return JsonResponse({'status':'true','filename': img , 'msg': 'Information updated successfully.'})
			else:
				return JsonResponse({'error':'Mobile Number Not Exist'})

	else:
		return JsonResponse({'error':'check your request method'})



@csrf_exempt
def cashboxLogin(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number = r['mobile_number']
		deviceid = r['deviceid']
		simid    = r['simid']      
		app_pass = r['app_pass']
		#app_pass_enc = dbEncrypt(app_pass)		
		print("%s %s %s %s" % (mobile_number,deviceid,simid, app_pass))

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not simid):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not app_pass):
			return JsonResponse({'error':'App Password cannot be empty'})
		else:
			
			counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile_number) & Q(cashbox_app_pass__exact = app_pass)  ).select_related()
			print("%s" % counter.count())
			if(counter.count() > 0):
				#credential ok
				return JsonResponse({'success':'true','msg':'Login Successfully.', 'user_type': counter[0].customer_type_id ,'mobile_number' : counter[0].cashbox_mobileno ,'device_id' : counter[0].cashbox_deviceid ,'sim_id' : counter[0].cashbox_simid ,'sim_id_two': '' ,'access_token' : counter[0].cashbox_access_token ,'authorization_key' : counter[0].cashbox_authorization_key,'fname': counter[0].cashbox_fname ,'mname': counter[0].cashbox_mname ,'lname': counter[0].cashbox_lname ,'dob': counter[0].cashbox_dob ,'gender': counter[0].cashbox_gender ,'vid': counter[0].cashbox_vodetid ,'nid': counter[0].cashbox_nationalD ,'email':'','alternate_mobile': '' ,'photo': counter[0].cashbox_photo ,'nid_image': '' ,'qrcode': counter[0].cashbox_qrcode ,'nominee_fname': counter[0].cashbox_nominee_fname ,'nominee_mname': counter[0].cashbox_nominee_mname ,'nominee_lname': counter[0].cashbox_nominee_lname , 'nominee_contactno': counter[0].cashbox_nominee_contactno ,'nominee_voterid': '' ,'nominee_nid': '' ,'nominee_relation': counter[0].cashbox_nominee_relation ,'nominee_voterid_image': '' ,'permanent_address':'' ,'present_address': ''})
			else:
				return JsonResponse({'error':'Invalid mobile number'})		
		
	else:
		return JsonResponse({'error':'Please check your request method'})


#=========================  part 01 ===============================================


def customerNameDetail(userid):	
	c = Pocket.objects.filter( customer_id__exact=userid).select_related()
	if(c.count() > 0 ):
		return c[0].pocket_fname+' '+c[0].pocket_lname+'|'+c[0].pocket_photo
	else:
		cc = Cashbox.objects.filter(cashbox_customer_id__exact=userid).select_related()
		if(cc.count() >0):
			return cc[0].cashbox_fname+' '+cc[0].cashbox_lname+'|'+cc[0].cashbox_photo
		else: 
			return 0

	
	

#==============================================================================================================

#cashout log data code = 10004
@csrf_exempt
def cashoutLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']


		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10004'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			sending_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
					else:
						ss_name,ss_photo = ss.split('|')

					if(p==0):
						name = ''
						photo =''
					else:
						name,photo = p.split('|')


					print("%s %s %s"% (sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%H:%M")
					}
					sending_data.append(sending_log)


			else:
				return JsonResponse({'error':'No data found'})


			receiving_data = []
			receiving_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(receiving_logs.count() >0):
				for rl in receiving_logs:
					p = customerNameDetail(rl.receiver_userid)
					if(p==0):
						name=''
						photo=''
					else:
						name,photo = p.split('|')

					receiving_log = {
						'trxid':rl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'amount': str(decimal.Decimal(rl.transaction_amount))+'TK' ,
						'transaction_date': (rl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (rl.transaction_timestamp).strftime("%H:%M")
					}
					receiving_data.append(receiving_log)
			else:
				return JsonResponse({'error':'No Data found'})			
	
		return JsonResponse({'success':'true','sending_log':sending_data , '':receiving_data })
	else:
		return JsonResponse({'error':'Please Check your request method'})






#pay log data code = 10002
@csrf_exempt
def payLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(log_type != '10002'):
			return JsonResponse({'error':'Invalid Log Type'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)			
			sending_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(sending_logs.count()>0):
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
					else:
						ss_name,ss_photo = ss.split('|')

					if(p ==0):
						name = ''
						photo = ''
					else:
						name,photo = p.split('|')

					print("%s %s %s"% (sl.sender_userid , sl.receiver_userid , name))					

					sending_log = {
						'trxid': sl.customer_transaction_id ,
						'name': name ,
						'photo': photo ,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'amount':str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%H:%M")
					}
					sending_data.append(sending_log)
			else:
				return JsonResponse({'error':'No Data Found'})




			receiving_data = []
			receiving_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(receiving_logs.count() >0):
				for rl in receiving_logs:
					p = customerNameDetail(rl.receiver_userid)				
					if(p==0):
						name = ''
						photo =''
					else:
						name,photo = p.split('|')


					print("%s %s %s"% (rl.sender_userid , rl.receiver_userid , name))
					
					receiving_log = {
						'trxid': rl.customer_transaction_id ,
						'name': name ,
						'photo': photo ,
						'amount': str(decimal.Decimal(rl.transaction_amount))+'TK' ,
						'transaction_date': (rl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (rl.transaction_timestamp).strftime("%H:%M")
					}
					receiving_data.append(receiving_log)

			else:
				return JsonResponse({'error':'No Data Found'})

			return JsonResponse({'success':'true', 'sending_log': sending_data , 'receiving_log':receiving_data })
	else:
		return JsonResponse({'error':'Please check your request method'})




#transfer log data code = 10001
@csrf_exempt
def transferLogData(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		
		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10001'):
			return JsonResponse({'error':'Invalid Log Type'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)	
			sending_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(sending_logs.count()>0):
				sending_data = []
				for l in sending_logs:
					#print("%s %s", (l.sender_userid , l.receiver_userid))
					#if(l.receiver_userid != user_id):
					p = customerNameDetail(l.receiver_userid)
					ss = customerNameDetail(l.sender_userid)

					if(ss ==0):
						ss_name = ''
						ss_photo = ''
					else:
						ss_name,ss_photo = ss.split('|') 

					if(p == 0):
						name = ''
						photo = ''
					else:
						name,photo= p.split('|')
				
					print("%s %s %s"% (l.sender_userid , l.receiver_userid , name))									
					sending_log = {
						'trxid':l.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'amount': str(decimal.Decimal(l.transaction_amount))+'TK' ,
						'transaction_date': (l.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (l.transaction_timestamp).strftime("%H:%M")
					}
					sending_data.append(sending_log)
			else:
				return JsonResponse({'error':'No data found'})




			receiving_data = []
			receiving_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			if(receiving_logs.count() >0):
				for rl in receiving_logs:
					p = customerNameDetail(rl.receiver_userid)
					if(p == 0):
						name = ''
						photo = ''
					else:
						name,photo= p.split('|')

					print("%s %s %s"% (rl.sender_userid , rl.receiver_userid , name))

					receiving_log = {
						'trxid':rl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'amount': str(decimal.Decimal(rl.transaction_amount))+'TK' ,
						'transaction_date': (rl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (rl.transaction_timestamp).strftime("%H:%M")
					}
					receiving_data.append(receiving_log)
				#return JsonResponse({'success':'true', 'sending_log': sending_data , 'receiving_log': receiving_data })			
			else:
				return JsonResponse({'error':'No data found'})			


		return JsonResponse({'success':'true', 'sending_log': sending_data , 'receiving_log': receiving_data  })
	else:
		return JsonResponse({'error':'Please check your request method'})


@csrf_exempt
def forgotBoth(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']		
		dob = r['dob']
		nominee_name = r['nominee_name']
		flag = r['flag']
		nid_vid = r['nid_vid']
		last_transaction_amount = r['last_transaction_amount']

		user_id = getCustomerInfo(mobile_number , user_type)
		
		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not dob):
			return JsonResponse({'error':'Date of Birth cannot be empty'})
		elif(not nominee_name):
			return JsonResponse({'error':'Nominee Name cannot be empty'})
		elif(not flag):
			return JsonResponse({'error':'Flag cannot be empty'})
		elif(not nid_vid):
			return JsonResponse({'error':'NID or Voter ID cannot be empty'})		
		elif(not last_transaction_amount):
			return JsonResponse({'error':'Last Transaction amount cannot be empty'})
		elif(user_id == 0):
			return JsonResponse({'error':'Mobile number not found'})
		else:
			if(user_type=='101'):			
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()								
				if(pocket_counter.count() == 0):
					return JsonResponse({'error':'Mobile number not found'})
				else:				
					pocket_user_id = pocket_counter[0].customer_id
					check_dob = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & Q(pocket_dob__exact=dob)).select_related()
					check_nominee_name = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & Q(pocket_nominee_lname__exact=nominee_name)).select_related()
					check_nid_vid = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & (Q(pocket_nid__exact=nid_vid) | Q(pocket_voterlD__exact=nid_vid)) ).select_related()


			

					transaction = CustomerTransaction.objects.filter(Q(transaction_amount__exact=last_transaction_amount) & (Q(sender_userid__exact=pocket_user_id) | Q(receiver_userid__exact=pocket_user_id))).order_by('-transaction_timestamp').select_related()					
					print ("id:%s " % pocket_user_id)
					if(check_dob.count() ==0):
						return JsonResponse({'error':'Date of birthd not matched'})
					elif(check_nominee_name.count() ==0 ):
						return JsonResponse({'error':'Nominee name not matched'})
					elif(check_nid_vid.count() == 0):
						return JsonResponse({'error':'NID/VID not matched'})	
					elif(transaction.count() == 0):
						return JsonResponse({'error':'No trasaction found'})
					else:						
						return JsonResponse({'success':'true','msg':'All answer is matched'})

			elif(user_type=='102'):
				cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one)).select_related()				
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Mobile number not found' })				
				else:
					cashbox_user_id = cashbox_counter[0].cashbox_customer_id
					transaction = CustomerTransaction.objects.filter(Q(transaction_amount__exact=last_transaction_amount) & (Q(sender_userid__exact=cashbox_user_id) | Q(receiver_userid__exact=cashbox_user_id))).order_by('-transaction_timestamp').select_related()


					checkdob = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_dob__exact=dob)).select_related()

					checknomineeinfo = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_nominee_lname__exact=nominee_name)).select_related()

					checknidvid = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & (Q(cashbox_nationalD__exact=nid_vid) | Q(cashbox_vodetid__exact=nid_vid))).select_related()

					if(checkdob.count() ==0 ):
						return JsonResponse({'error':'Date of birth not matched'})
					elif(checknomineeinfo.count() == 0):
						return JsonResponse({'error':'Nominee name not matched'})
					elif(checknidvid.count() == 0):
						return JsonResponse({'error':'NID/VID not matched'})
					elif(transaction.count() == 0):
						return JsonResponse({'error':'No Transaction found'})
					else:
						return JsonResponse({'success':'true', 'msg':'All answer is matched' })
			else:
				return JsonResponse({'error':'Invalid User Type'});
			
	else:
		return JsonResponse({'error':'Please check your request method'})



@csrf_exempt
def forgotTransactionPin(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		app_pass = r['app_pass']
		app_password = dbEncrypt(app_pass)		

		user_id = getCustomerInfo(mobile_number , user_type)

		if(user_id==0):
			return JsonResponse({'error':'Invalid User'})
		elif(not mobile_number):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID 1 cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'Usert Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device Id cannot be empty'})
		elif(not app_pass):
			return JsonResponse({'error':'App Password cannot be empty'})
		else:
			
			if(user_type=='101'):
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_app_pass = app_password)).select_related()								
				
				if(pocket_counter.count() ==0):
					return JsonResponse({'error':'Mobile Number is not found'})
				else:
					return JsonResponse({'success':'true','msg':'App Password is correct'})

			elif(user_type=='102'):
				cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_app_pass__exact= app_password )).select_related()				
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Mobile number is not found'})
				else:
					return JsonResponse({'success':'true','msg':'App Password is correct'})
			else:
				return JsonResponse({'error':'Invalid User Type'})
	else:
		return JsonResponse({'error':'Please Check your request method'})




@csrf_exempt
def forgotTransactionPINUpdate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		new_transaction_pin = r['new_transaction_pin']		
		confirm_new_transaction_pin = r['confirm_new_transaction_pin']

		user_id = getCustomerInfo(mobile_number , user_type)

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not new_transaction_pin):
			return JsonResponse({'error':'New Transaction PIN cannot be empty'})
		elif(not confirm_new_transaction_pin):
			return JsonResponse({'error':'Confirm New Transaction PIN cannot be empty'})
		elif(new_transaction_pin != confirm_new_transaction_pin):
			return JsonResponse({'error':'New Transaction PIN and Confirm New Transaction PIN cannot matched'})
		else:
			if(user_type=='101'):
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number)& Q(pocket_deviceid__exact=device_id) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()
				if(pocket_counter.count()== 0):
					return JsonResponse({'error':'Mobile not found'})
				else:
					transaction_pin = dbEncryptTwo(new_transaction_pin)
					Pocket.objects.filter(customer_id__exact = user_id).update(pocket_transaction_pass = transaction_pin )
					return JsonResponse({'success':'true' , 'msg':'Transaction PIN is updated'}) 	

			elif(user_type=='102'):
				cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_transaction_pass__exact = transaction_pin)).select_related()
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Mobile Number not found'})
				else:
					transaction_pin = dbEncryptTwo(new_transaction_pin)
					Cashbox.objects.filter(cashbox_customer_id__exact = user_id).update(cashbox_transaction_pass = transaction_pin ) 					
					return JsonResponse({'success':'true','msg':'Transaction PIN is updated'})
			else:
				return JsonResponse({'error':'Invalid User Type'})			
		
	else:
		return JsonResponse({'error':'Please Check you request method'})






@csrf_exempt
def forgotPassword(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		transaction_pin = r['transaction_pin']
		transaction_pass = dbEncryptTwo(transaction_pin)		


		user_id = getCustomerInfo(mobile_number , user_type)
		if(user_id == 0):
			return JsonResponse({'error':'Invalid User'})
		elif(not mobile_number):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'Sim ID 1 cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device Id cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not transaction_pin):
			return JsonResponse({'error':'Transaction PIN cannot be empty'})
		else:
			if(user_type=='101'):
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_transaction_pass = transaction_pass)).select_related()				
				if(pocket_counter.count() == 0):
					return JsonResponse({'error':'Invalid Transaction PIN'})
				else:
					return JsonResponse({'success':'true', 'msg':'Transaction PIN is correct.'})

			elif(user_type == '102'):			
				cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_transaction_pass__exact=transaction_pass)).select_related()
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Invalid Transaction PIN' })
				else:	
					return JsonResponse({'success':'true' , 'msg':'Transaction PIN is correct' })
					
			else:
				return JsonResponse({'error':'Invalid User Type' })
	else:
		return JsonResponse({'error': 'Please Check Your Request Method'})




@csrf_exempt
def forgotPasswordUpdate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		new_app_pass = r['new_app_pass']		
		confirm_new_app_pass = r['confirm_new_app_pass']
	
		user_id = getCustomerInfo(mobile_number , user_type)
		if(user_id == 0):
			return JsonResponse({'error':'Invalid User'})
		elif(not mobile_number):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'Sim ID 1 cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not new_app_pass):
			return JsonResponse({'error':'New App Password cannot be empty'})
		elif(not confirm_new_app_pass):
			return JsonResponse({'error':'Confirm New App Password cannot be empty'})
		elif(new_app_pass != confirm_new_app_pass ):
			return JsonResponse({'error':'New App Password and Confirm New App Password not matched'})
		else:
			if(user_type=='101'):
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number)& Q(pocket_deviceid__exact=device_id) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()
				if(pocket_counter.count() == 0):
					return JsonResponse({'error':'Mobile Number not found'})
				else:
					#app pass update
					app_pass = dbEncrypt(new_app_pass)
					Pocket.objects.filter(customer_id__exact = user_id).update(pocket_app_pass = app_pass ) 					
					return JsonResponse({'success':'true', 'msg':'Appp Password is updated.'})			
			elif(user_type=='102'):
				cashbox_counter = Cashbox.objects.filter( Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one)).select_related()
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Mobile Number is not found' })
				else:
					app_pass = dbEncrypt(new_app_pass)
					Cashbox.objects.filter(cashbox_customer_id__exact = user_id).update(cashbox_app_pass = app_pass ) 
					return JsonResponse({'success':'true' , 'msg':'App Password is  updated' })
			else:
				return JsonResponse({'error':'Invalid User Type'})

	else:
		return JsonResponse({'error':'Please check your request method'})



@csrf_exempt
def qrCodePdfVersion(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']

		if(not mobileno):
			return JsonResponse({'error':'Mobile Number Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			if(user_id ==0):
				return JsonResponse({'error':'Invalid Mobile Number'})
			else:
				if(user_type=='101'):
					pc = Pocket.objects.filter(Q(pocket_mobileno__exact = mobileno)  ).select_related()
					qr_Code = pc[0].pocket_qrcode
					
					pdf = FPDF()
					# compression is not yet supported in py3k version
					pdf.compress = False
					pdf.add_page()
					# Unicode is not yet supported in the py3k version; use windows-1252 standard font
					pdf.set_font('Arial', '', 14)  
					pdf.ln(10)
					#pdf.write(5, 'hello world %s áó' % sys.version)
					pdf.image("/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0"+ qr_code+".png" , 50, 50)
					pdf.output('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0'+qr_code+'.png', 'F')
					
					qr_pdf_url = "http://ec2-54-144-212-196.compute-1.amazonaws.com:80/profilepic/0"+qr_code+".png"

					return JsonResponse({'success':'true' , 'qr_pdf_url': qr_pdf_url })
				elif(user_type=='102'):
					cb = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobileno)  ).select_related()
					qr_code = cb[0].cashbox_qrcode

					pdf = FPDF()
					# compression is not yet supported in py3k version
					pdf.compress = False
					pdf.add_page()
					# Unicode is not yet supported in the py3k version; use windows-1252 standard font
					pdf.set_font('Arial', '', 14)
					pdf.ln(10)
					#pdf.write(5, 'hello world %s Ã' % sys.version)
					pdf.image("/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0"+ qr_code+".png" , 50, 50)
					pdf.output('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0'+qr_code+'.png', 'F')

					
					qr_pdf_url = "http://ec2-54-144-212-196.compute-1.amazonaws.com:80/profilepic/0"+qr_code+".png"

					return JsonResponse({'success':'true' , 'qr_pdf_url': qr_pdf_url })
				else:
					return JsonResponse({'error':'Please Check User Type'})
		

	else:
		return JsonResponse({'error':'Please Check Your Request Method'})


@csrf_exempt
def transactionList(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']
		deviceid  = r['deviceid']
		simid_one = r['simid_one']
		simid_two = r['simid_two']
		transaction_pin = r['transaction_pin']
		transaction_type = r['transaction_type']
		search_type = r['search_type']

		if(not mobileno):
			return JsonResponse({'error': 'Mobile Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Tyepe Cannot be Empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid Cannot be Empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SimID One Cannot be Empty'})
		elif(not transaction_pin):
			return JsonResponse({'error':'Transaction PIN Cannot be Empty'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			if (user_id ==0):
				return JsonResponse({'error':'Please Check your Mobile Number'})
			else:
				request_date = date.today()
				if(search_type == '7'):
					one_week_date = date.today() - datetime.timedelta(days=7)
					transaction_list = CustomerTransaction.objects.filter(Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=(one_week_date,request_date)) ).select_related()
					return JsonResponse({'success':'true'})
				elif(search_type == '30'):
					one_month_date  = date.today() - datetime.timedelta(days=30)
					transaction_list = CustomerTransaction.objects.filter(Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=(one_week_date,request_date))).select_related()
					return JsonResponse({'status':'true' })
				else:
					return JsonResponse({'error':'Please Check Your Search Type'})
		
	else:
		return JsonResponse({'error':'Please Check Your Request'})



@csrf_exempt
def customerCurrentBalance(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']
		deviceid  = r['deviceid']
		simid_one     = r['simid_one']      
		simid_two = r['simid_two']
		transaction_pin = r['transaction_pin']

		if( not mobileno):
			return JsonResponse({'error': 'Mobile Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Tyepe Cannot be Empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid Cannot be Empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SimID One Cannot be Empty'})
		#elif(not transaction_pin):
		#	return JsonResponse({'error': 'Transaction PIN Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			user_det = CustomerAccounting.objects.filter( Q(userid__exact = user_id) ).select_related()
			customer_balance = decimal.Decimal(user_det[0].customer_balance)			

			return JsonResponse({'success':'true','current_balance': str(customer_balance)   })
	else:
		return JsonResponse({'error':'Please Check your request'})	



#customer and merchant id 
def getCustomerInfo(mobile , customer_type):
	if (customer_type == '101'):
		pc = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile)  ).select_related()
		if ( pc.count() > 0):
			return pc[0].customer_id
		else:
			return 0
	else:
		ch = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile)  ).select_related()
		if ( ch.count() > 0):
			return ch[0].cashbox_customer_id
		else:
			return 0
	


#cashout api
@csrf_exempt
def cashOutRequest(request):
	if request.method == 'POST' :
		r = json.loads(request.body)
		sender_mobile = r['sender_mobile']
		sender_type = r['sender_type']
		receiver_mobile = r['receiver_mobile']
		receiver_type = r['receiver_type']
		cashout_amount = r['cashout_amount']
		transaction_pass = r['transaction_pass']

		if ( not sender_mobile):
			return JsonResponse({'error': 'Sender Mobile Cannot be Empty'})
		elif ( not receiver_mobile):
			return JsonResponse({'error': 'Receiver Mobile Cannot be Empty'})
		elif ( not cashout_amount):
			return JsonResponse({'error': 'Cashout Amount Cannot be Empty'})
		elif ( receiver_type == '101'):
			return JsonResponse({'error': 'Cashbox Cannot be Pocket'})
		elif ( decimal.Decimal(cashout_amount) < 100 ):
			return JsonResponse({'error': 'Minimum Cashout limit 100TK'})
		elif ( not transaction_pass ):
			return JsonResponse({'error': 'Transaction Password Cannot be Empty' })
		else:
			sender_info = getCustomerInfo(sender_mobile , sender_type)
			receiver_info = getCustomerInfo(receiver_mobile , receiver_type)
			print("%s , %s"% (sender_info,receiver_info))
			if(sender_info == 0):
				return JsonResponse({'error':'Sender Mobile Not Found'})
			else:			
				if(receiver_info == 0):
					return JsonResponse({'error':'Receiver Mobile Not Found'})
				else:
					service_fee = (decimal.Decimal(cashout_amount)*1)/100;
					total = decimal.Decimal(cashout_amount)+decimal.Decimal(service_fee)
					
					cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
					sender_balance = decimal.Decimal(cas[0].customer_balance)

					if(decimal.Decimal(sender_balance) > decimal.Decimal(total) ):
						ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
						receiver_balance = decimal.Decimal(ras[0].customer_balance) 

						sender_current_balance = decimal.Decimal(sender_balance)-decimal.Decimal(total)
						receiver_current_balance = decimal.Decimal(receiver_balance)+decimal.Decimal(total)
						print("%s , %s , %s , %s , %s"% (str(decimal.Decimal(total)), sender_balance , receiver_balance , sender_current_balance,receiver_current_balance ) )


						trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))

						#Customer Transaction portion
						cts = CustomerTransaction()
						cts.customer_transaction_id = trx
						cts.sender_userid = sender_info
						cts.receiver_userid = receiver_info
						cts.transaction_amount = decimal.Decimal(cashout_amount)
						cts.transaction_type = '10004'
						cts.transaction_message = 'TRXID: '+ str(trx) + ' Cashout Amount:'+ str(decimal.Decimal(total)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
						cts.cashbox_business_id = ''
						cts.auxilary_userid = ''
						cts.referral_id = ''
						cts.transaction_timestamp = datetime.datetime.now()
						cts.save()						

						CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).update( customer_balance = sender_current_balance )
						CustomerAccounting.objects.filter( Q(userid__exact = receiver_info) ).update( customer_balance = receiver_current_balance )

						
			

						return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee))+'','cashout_amount': str(decimal.Decimal(total))+'TK' , 'msg':'Cashout Complete'})
					else:
						return JsonResponse({'error': 'Insufficient Balance.Current Balance:'+str(decimal.Decimal(sender_balance))+'TK'  })
	else:
		return JsonResponse({'error':'Please check your request method'})
			



@csrf_exempt
def balanceTopUpWalletRequestToMerchant(request):
	if request.method  == 'POST' :
		r = json.loads(request.body)
		sender_customer_mobile   = r['sender_mobile'].strip()
		receiver_customer_mobile = r['receiver_mobile'].strip()
		sender_customer_type     = r['sender_customer_type'].strip()
		receiver_customer_type   = r['receiver_customer_type'].strip()
		topup_amount             = r['topup_amount'].strip()


		if(not sender_customer_mobile ):
			return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
		elif(not receiver_customer_mobile):
			return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
		elif(not sender_customer_type):
			return JsonResponse({'error' : 'Sender Customer Type not Empty'})
		elif(not receiver_customer_type):
			return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
		elif(not topup_amount):
			return JsonResponse({'error' : 'TopUp Amount not Empty'})
		elif(sender_customer_type == '101'):
			return JsonResponse({'error' : 'Sender Cannot be Wallet'})	
		else:
			sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
			receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )
			if ( sender_info != 0):
				if ( receiver_info != 0):
					cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
					sender_balance = decimal.Decimal(cas[0].customer_balance)

					ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
					receiver_balance = decimal.Decimal(ras[0].customer_balance) 

					if ( decimal.Decimal(sender_balance) <  decimal.Decimal(topup_amount) ):
						return JsonResponse({'error': 'Insufficient Balance.Your Current Balance:'+ str(cas[0].customer_balance)  })
					elif ( decimal.Decimal(sender_balance) >  decimal.Decimal(topup_amount) ):
						sbal = decimal.Decimal(sender_balance) - decimal.Decimal(topup_amount)
						#CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).update( customer_balance = sbal )
						rbal = decimal.Decimal(receiver_balance) + decimal.Decimal(topup_amount)
						#CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)  ).update(customer_balance = rbal )

						trx = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))

						ct = CustomerTopUpTransaction()
						ct.customer_transaction_id = trx
						ct.topup_sender_userid =  sender_info
						ct.topup_receiver_userid = receiver_info
						ct.topup_transaction_amount = decimal.Decimal(topup_amount)
						ct.topup_transaction_type	=  '1002'
						ct.topup_transaction_message = 'TRXID:'+trx+' TopUp Request Send.'
						ct.topup_cashbox_business_id =   ''
						ct.topup_auxilary_userid = ''  
						ct.topup_referral_id =  ''
						ct.topup_transaction_status = 'pending'
						ct.topup_transaction_initiator_request_timestamp = datetime.datetime.now() 	
						ct.topup_transaction_completrion_request_timestamp = datetime.datetime.now()
						ct.save()			           

						return JsonResponse({ 'success' : 'true' , 'msg' : 'TRXID: '+ str(trx) +' Topup Request Send .' })													
					else:
						return JsonResponse({'error': 'Block---4'})

				else:
					return JsonResponse({'error': 'Receiver Mobile Not Found'})
			else:
				return JsonResponse({'error': 'Sender Mobile Not Found'})
		
	else:
		return JsonResponse({'error': 'Check your request method'})


@csrf_exempt
def balancePay(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		sender_customer_mobile   = r['sender_mobile'].strip()
		receiver_customer_mobile = r['receiver_mobile'].strip()
		sender_customer_type     = r['sender_customer_type'].strip()
		receiver_customer_type   = r['receiver_customer_type'].strip()
		pay_amount               = r['pay_amount'].strip()
		transaction_pass         = r['transaction_pass'].strip()

		if ( not sender_customer_mobile ):
			return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
		elif ( not receiver_customer_mobile ):
			return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
		elif ( not sender_customer_type ):
			return JsonResponse({'error' : 'Sender Customer Type not Empty'})
		elif ( not receiver_customer_type ):
			return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
		elif ( not pay_amount ):
			return JsonResponse({'error' : 'Pay Amount not Empty'})
		elif ( not transaction_pass ):
			return JsonResponse({'error' : 'Transaction Amount not Empty'})
		elif ( sender_customer_type == '102'):
			return JsonResponse({'error' : 'Sender Cannot be Merchant'})
		elif ( receiver_customer_type == '101' ):
			return JsonResponse({'error' : 'Sender Cannot be Wallet'})
		else:
			sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
			receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )

			if(sender_info==0):
				return JsonResponse({'error': 'Sender Mobile Number Not Found'})
			else:
				if(receiver_info==0):
					return JsonResponse({'error':'Receiver Mobile Number Not Found'})
				else:
					if(decimal.Decimal(pay_amount)<= 50):
						service_fee = 0
					elif(decimal.Decimal(pay_amount)>50 and decimal.Decimal(pay_amount)<=1000):
						service_fee = 2
					else:
						service_fee = 5



					cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
					sender_balance = decimal.Decimal(cas[0].customer_balance)
					
					total = decimal.Decimal(pay_amount)+decimal.Decimal(service_fee)

					if(decimal.Decimal(sender_balance) < decimal.Decimal(total) ):
						return JsonResponse({'error':'Insufficient Balance.Current Balance:'+str(decimal.Decimal(sender_balance))+'TK' })
					else:
						ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
						receiver_balance = decimal.Decimal(ras[0].customer_balance) 

						sender_current_balance = decimal.Decimal(sender_balance)-decimal.Decimal(total)
						receiver_current_balance = decimal.Decimal(receiver_balance)+decimal.Decimal(total)					
						print("%s, %s , %s ,%s, %s"% (service_fee,sender_balance,total,sender_current_balance,receiver_current_balance) )
						trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))



						cts = CustomerTransaction()
						cts.customer_transaction_id = trx
						cts.sender_userid = sender_info
						cts.receiver_userid = receiver_info
						cts.transaction_amount = decimal.Decimal(pay_amount)
						cts.transaction_type = '10002'
						cts.transaction_message = 'TRXID: '+ str(trx) + ' Pay amount:'+ str(decimal.Decimal(pay_amount)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
						cts.cashbox_business_id = ''
						cts.auxilary_userid = ''
						cts.referral_id = ''
						cts.transaction_timestamp = datetime.datetime.now()
						cts.save()


						CustomerAccounting.objects.filter( Q(userid__exact = sender_info)).update( customer_balance = sender_current_balance )
						CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)).update( customer_balance = receiver_current_balance )

						
						return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee)),'msg':'TRXID:'+str(trx)+'.Pay Amount:'+ str(decimal.Decimal(total))+'TK.Current Balance:'+ str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+ str(decimal.Decimal(service_fee))+'TK.'  })
				#else:
				#	return JsonResponse({'error':'Receiver Mobile Number Not Found'})								
	else:
		return JsonResponse({'error':'Check your request method'})



@csrf_exempt
def balanceTransfer(request):
	if request.method == 'POST':
		r = json.loads(request.body)		
		sender_customer_mobile   = r['sender_mobile'].strip()
		receiver_customer_mobile = r['receiver_mobile'].strip()
		sender_customer_type     = r['sender_customer_type'].strip()
		receiver_customer_type   = r['receiver_customer_type'].strip()
		transfer_amount          = r['transfer_amount'].strip()
		transaction_pass         = r['transaction_pass'].strip()

		if ( not sender_customer_mobile ):
			return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
		elif ( not receiver_customer_mobile ):
			return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
		elif ( not sender_customer_type ):
			return JsonResponse({'error' : 'Sender Customer Type not Empty'})
		elif ( not receiver_customer_type ):
			return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
		elif ( not transfer_amount ):
			return JsonResponse({'error' : 'Transfer Amount not Empty'})
		elif ( not transaction_pass ):
			return JsonResponse({'error' : 'Transaction Amount not Empty'})
		elif ( sender_customer_type == '102'):
			return JsonResponse({'error': 'Sender Mobile Cannot be Merchant'})
		elif ( receiver_customer_type == '102' ):
			return JsonResponse({'error': 'Receiver Mobile Cannot be Merchant' })
		else:
			sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
			receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )



			if ( sender_info != 0 ):
				if( receiver_info != 0 ):
					receiver_id = receiver_info
					sender_id = sender_info
					cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_id) ).select_related()
					sender_balance = decimal.Decimal(cas[0].customer_balance)
					ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_id ) ).select_related()
					receiver_balance = decimal.Decimal(ras[0].customer_balance) 


					if ( decimal.Decimal(sender_balance) <  decimal.Decimal(transfer_amount) ):
						return JsonResponse({'error': 'Insufficient Balance.Your Current Balance:'+ str(cas[0].customer_balance)  })
					elif ( decimal.Decimal(sender_balance) >  decimal.Decimal(transfer_amount) ):
						sbal = decimal.Decimal(sender_balance) - decimal.Decimal(transfer_amount)
						CustomerAccounting.objects.filter( Q(userid__exact = sender_id) ).update( customer_balance = sbal )
						
						rbal = decimal.Decimal(receiver_balance) + decimal.Decimal(transfer_amount)
						CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)  ).update(customer_balance = rbal )
						trx = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))

						#Customer Transaction portion
						cts = CustomerTransaction()
						cts.customer_transaction_id = trx
						cts.sender_userid = sender_id
						cts.receiver_userid = receiver_id
						cts.transaction_amount = decimal.Decimal(transfer_amount)
						cts.transaction_type = '10001'
						cts.transaction_message = 'TRXID: '+ str(trx) + ' Balance transfer amount: '+ str(transfer_amount) + 'TK. Current Balance: '+str(sbal)+'TK'
						cts.cashbox_business_id = ''
						cts.auxilary_userid = ''
						cts.referral_id = ''
						cts.transaction_timestamp = datetime.datetime.now()
						cts.save()

						return JsonResponse({ 'success' : 'true' , 'msg' :'TRXID: '+  str(trx) +' Balance ' + str(transfer_amount) + 'BDT Transfered. Current Balance:'+ str(sbal) + 'TK' })	
					else:
						return JsonResponse({'error': 'Block---4'})
				else:
					return JsonResponse({'error': 'Receiver Mobile Number Not Exist'})
			else:
				return JsonResponse({'error': 'Sender Mobile Number Not Exist'})	



					



@csrf_exempt
def pocketNomineeInfo(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number = r['mobile_number'].strip()
		sim_id_one = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		device_id = r['device_id'].strip()
		
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_deviceid__exact= device_id)).select_related()
		if ( counter.count() > 0 ):

			#if ( "1111-11-11" not in (counter[0].nomineedobs)): dob = None
			#else:	dob = counter[0].nomineedobs


			return JsonResponse({'status':'true' , 'first_name': counter[0].pocket_nominee_fname , 'middle_name': counter[0].pocket_nominee_mname , 'last_name': counter[0].pocket_nominee_lname , 'dob': counter[0].nomineedobs , 'address': counter[0].pocket_nominee_address , 'contactno': counter[0].pocket_nominee_contactno , 'voter_id': counter[0].pocket_nominee_voterid , 'nid': counter[0].pocket_nominee_nid , 'relation': counter[0].pocket_nominee_relation , 'voter_or_nid_image': counter[0].pocket_nominee_voterid_scancopy  })
		else:
			return JsonResponse({'error': 'Mobile Number is Not Exist'})
	else:
		return JsonResponse({'error': 'Check your request method'})		


@csrf_exempt
def checkHeaderInfo(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		data  = r['data'].strip()

		#print data
		#print locale.getpreferredencoding()

		key = 'abcdefghijklmnopqrstuvwxyz123456'
		encodedEncrypted = data  #urllib2.unquote(quotedEncodedEncrypted)

		cipher = AES.new(key)
		decrypted = cipher.decrypt(base64.b64decode(encodedEncrypted))[:16]

		for i in range(1, len(base64.b64decode(encodedEncrypted))/16):
			cipher = AES.new(key, AES.MODE_CBC, base64.b64decode(encodedEncrypted)[(i-1)*16:i*16])
			decrypted += cipher.decrypt(base64.b64decode(encodedEncrypted)[i*16:])[:16]		



		return  JsonResponse({'r': str(decrypted).encode('ascii') , 'data': data })







"""
       		#iv = os.urandom(16)
	   	key = 'abcdefghijklmnopqrstuvwxyz123456'
		d = hashlib.sha256(key.encode()).digest()
                m =  base64.b64decode(data) #.decode('utf-8'  ,errors='ignore')
		iv = m[:16]
		obj2 = AES.new( d , AES.MODE_CBC, iv)
		res = obj2.decrypt(m)

		
		#print data
		pass_phrase = "abcdefghijklmnopqrstuvwxyz123456"
		secret_key = generate_secret_key(pass_phrase)

		# generate cipher
		cipher = AESCipher(secret_key)

		#raw_text = "abcdefg"
		#encrypt_text = cipher.encrypt(raw_text)
		#assert raw_text != encrypt_text

		decrypt_text = cipher.decrypt(data)
		#assert encrypt_text != decrypt_text
		#assert decrypt_text == raw_text
		

		print res # , errors='ignore')
		return JsonResponse({'r': res.encode('ascii' , errors='ignore')  , 'data': data})
	
	#print( request.META )
 """


#dummy cashbox entry
def dummyCashbox():
	ca = CustomerAccounting()
	ca.userid = '59136810201718556677598419'
	ca.user_type_id = '102'
	ca.customeraccount_transaction_type = 'pay'
	ca.customeraccount_debit = '0.00'
	ca.customeraccount_credit = '0.00'
	ca.comission_amount = '0.00'   #(ex:per transaction fee)   
	ca.commission_slave_amount = '0.00'
	ca.service_fee_amount = '0.00'
	ca.customer_referral_withdrawl_amount =  '0.00'
	ca.customer_balance = '0.00'
	ca.business_id = '2'
	ca.auxilary_accounting_userid = ''  #(comma separated value)
	ca.referral_id = ''  #(comma separated value)
	ca.save()

"""
	customerIDNum = '{0:06}'.format(random.randint(1,10000000))+'102'+'01718556677'+'{0:06}'.format(random.randint(1,1000000))
	customer_pin = '{0:06}'.format(random.randint(1,1000000)) + 'UUCAS'+'01718556677'+ '{0:06}'.format(random.randint(1,1000000))
	pocket_verification_pin = '{0:06}'.format(random.randint(1,1000000))
	salt_val = uuid.uuid4().hex
	qrCodeNo = hashlib.sha256( salt_val.encode() + pocket_verification_pin.encode() ).hexdigest()

	c = Cashbox()
	c.cashbox_customer_id = customerIDNum
	c.cashbox_id = customer_pin
	c.cashbox_fname = "ABC"
	c.cashbox_mname = "Modern"
	c.cashbox_lname = "House"
	c.cashbox_mobileno = "01718556677"
	c.cashbox_dob = '1987-08-05'
	c.cashbox_gender = 'Male'
	c.cashbox_nationalD = '62343805967-0837948384'
	c.cashbox_vodetid = '56636726777848389849'
	c.cashbox_photo = ''
	c.cashbox_access_token = 'hfska73872'
	c.cashbox_authorization_key = 'B3823694I9L878CYBDYB8FDQ2ZZLL2AQQEIF1T9'
	c.cashbox_deviceid = 'i89kh7373h8j893397f90'
	c.cashbox_simid = 'uy77389fh7338fh8890'
	c.cashbox_verified = '1'
	c.cashbox_app_pass = '3456789'
	c.cashbox_transaction_pass = '12344567'
	c.cashbox_qrcode = '0AK7ITWMR2HX8I2CJFGP2.png'
	c.cashbox_status_ative_frozen = '0'
	c.cashbox_business_id = '1'
	c.cashbox_refferedby_customer_id = ''
	c.customer_type_id = '102'
	c.cashbox_nominee_fname = 'abul'
	c.cashbox_nominee_mname = 'hasan'
	c.cashbox_nominee_lname = 'hasan'
	c.cashbox_nominee_address = 'Badda,Dhaka'
	c.cashbox_nominee_contactno = '01723456788'
	c.cashbox_nominee_relation = 'Cousin'
	c.cashbox_registration_timestamp = '2018-08-09 10:25:37.947243+00'
	c.save()
"""



#sample qr
@csrf_exempt
def sampleQR(request):
	#dummyCashbox()
	
	qr = qrcode.QRCode(
		    version=6,
		    error_correction=qrcode.constants.ERROR_CORRECT_H,
		    box_size=10,
		    border=4,
	)
	qr.add_data('https://www.uumoo.biz/?AK7ITWMR2HX8I2CJFGP2')
	#1a36251df6f379cfcd198964122f1b6fec06950cc4c54bb9179b3955e583b145')   #AK7ITWMR2HX8I2CJFGP2')
	qr.make(fit=True)
	img = qr.make_image(fill_color="#000000", back_color="#ffffff")
	img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/test.png');
	im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/test.png')
	im = im.convert("RGBA")
	logo = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/uumoo_logo.png')
	box =(200,200,300,300)   #(250,250,350,350) #(200,200,270,270)   # (265,265,300,300)#(135,135,235,235)
	im.crop(box)
	region = logo
	region = region.resize((box[2] - box[0], box[3] - box[1]))
	im.paste(region,box)
	#im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/01a36251df6f379cfcd198964122f1b6fec06950cc4c54bb9179b3955e583b145.png')
	im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0AK7ITWMR2HX8I2CJFGP2.png');
	im.show()
	

	return JsonResponse({'msg': 'Sample QR'})



def iv():
	return chr(0) * 32

def dbDecrypt(enc_pass):
	con = psycopg2.connect(
                host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
                database = 'uumoogrid_dev',
                user = 'power_user',
                password= 'PsnT@uumoo2018',
                port = '5432'
        )
	ukey = 'uumoo'
	curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	curr.execute("select convert_from(decrypt('"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
	two = curr.fetchone()
	data = str(two[0])
	
	return data
	

def dbDecryptTwo(enc_pass):
        con = psycopg2.connect(
                host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
                database = 'uumoogrid_dev',
                user = 'power_user',
                password= 'PsnT@uumoo2018',
                port = '5432'
        )
        ukey = 'uumoo2'
        curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
        curr.execute("select convert_from(decrypt('\\x"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
        two = curr.fetchone()
        data = str(two[0])

        return data




def dbEncrypt(user_str):
	con = psycopg2.connect(
		host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
		database = 'uumoogrid_dev',
		user = 'power_user',
		password= 'PsnT@uumoo2018',
		port = '5432'
	)

	cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	pass_txt = user_str #request.GET.get("pass") #'12345'
	ukey = 'uumoo'
	rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
	one = cur.fetchone()	
	enc_pass = b"".join( one['pt'])

	print ("Enc: \\x%s" % str( binascii.b2a_hex( enc_pass), 'utf-8') )
	return "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8') 



def dbEncryptTwo(user_str):
        con = psycopg2.connect(
                host = 'ec2-54-144-212-196.compute-1.amazonaws.com' ,
                database = 'uumoogrid_dev',
                user = 'power_user',
                password= 'PsnT@uumoo2018',
                port = '5432'
        )

        cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
        pass_txt = user_str #request.GET.get("pass") #'12345'
        ukey = 'uumoo2'
        rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
        one = cur.fetchone()
        enc_pass =  b"".join( one['pt'])
	# str(one[0]).encode('hex')

        return "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8')




#pocket nominee profileupdate ui
def pocketNomineeProfileUI(request):
	context = {}
	return render(request , "api/nominee_profile.html" , context)




@csrf_exempt
def pocketNomineeProfile(request):
	#dd = request.POST
	if request.method == 'POST':
		r = json.loads(request.body)

		pocket_mobileno = r["pocket_mobileno"].strip()
		pocket_nominee_fname = r["pocket_nominee_fname"].strip()
		pocket_nominee_mname = r["pocket_nominee_mname"].strip()
		pocket_nominee_lname = r["pocket_nominee_lname"].strip()
		nomineedobs = r["nomineedob"].strip()
		pocket_nominee_contactno = r["pocket_nominee_contactno"].strip()
		pocket_nominee_relation = r["pocket_nominee_relation"].strip()
		pocket_nominee_nid_voter = r["pocket_nominee_nid_voter"].strip()
		pocket_nominee_nid_or_voter = r["pocket_nominee_nid_or_voter"].strip()
		pocket_nominee_address = r['pocket_nominee_address'].strip()		

	
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()
	
	

	
		pocket_nominee_nid = None
		pocket_nominee_voterlD = None
		if  pocket_nominee_nid_or_voter == 'NID':
			pocket_nominee_nid = pocket_nominee_nid_voter
			pocket_nominee_voterlD = ""
		else:
			pocket_nominee_voterlD = pocket_nominee_nid_voter
			pocket_nominee_nid = ""






		if (not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			if ( counter.count() > 0 ):
				customer_id = counter[0].customer_id
	

				
				if (pocket_nominee_fname is not None and pocket_nominee_fname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_fname = pocket_nominee_fname )
				if (pocket_nominee_mname is not None and pocket_nominee_mname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_mname = pocket_nominee_mname )
				if (pocket_nominee_lname is not None and pocket_nominee_lname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_lname = pocket_nominee_lname )
				if (nomineedobs is not None and nomineedobs != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(nomineedobs = nomineedobs )
				if (pocket_nominee_relation is not None and pocket_nominee_relation != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_relation= pocket_nominee_relation)
				if (pocket_nominee_nid is not None and pocket_nominee_nid !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_nid = pocket_nominee_nid )
				if (pocket_nominee_voterlD is not None and pocket_nominee_voterlD !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_voterid = pocket_nominee_voterlD)
				if (pocket_nominee_contactno is not None and pocket_nominee_contactno != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_contactno=pocket_nominee_contactno)
				if (pocket_nominee_address is not None and pocket_nominee_address != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_address=pocket_nominee_address)
				
	

				return JsonResponse({'status':'true' ,'msg':'Nominee information updated.' })
			else:
				return JsonResponse({'msg':'Please check Mobileno,DeviceID,SIMID'})			
	else:
		return JsonResponse({'error':'check your request method'})



#qr-code scan and 
@csrf_exempt
def qrCodeScan(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobile = r['mobile_number'].strip()
		sim_id_one  = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		sender_user_type = r['sender_user_type'].strip()
		device_id = r['device_id'].strip()
		qr_token = '0' + r['qr_token'].strip() +'.png'

	
		counter = Pocket.objects.filter(Q(pocket_qrcode__exact = qr_token )).select_related()
		if (counter.count() > 0 ):
				return JsonResponse({'success':'true' , 'customer_mobile': counter[0].pocket_mobileno , 'first_name': counter[0].pocket_fname , 'last_name': counter[0].pocket_lname , 'middle_name': counter[0].pocket_mname , 'profile_image': counter[0].pocket_photo , 'user_type': counter[0].pocket_customer_type  })
		
		else:
			cash = Cashbox.objects.filter(Q(cashbox_qrcode__exact = qr_token )).select_related()
			if ( cash.count() > 0 ):
				return JsonResponse({'success':'true' , 'customer_mobile': cash[0].cashbox_mobileno , 'first_name': cash[0].cashbox_fname , 'last_name': cash[0].cashbox_lname , 'middle_name': cash[0].cashbox_mname , 'profile_image': cash[0].cashbox_photo , 'user_type': cash[0].customer_type_id   })
				
			else:
				return JsonResponse({'error':'Invalid Mobile Number'})  #**************
	else:
		return JsonResponse({'error':'Check your request method.'})
		



# Create your views here.
@csrf_exempt
def pocketSignUp(request):
	if request.method == 'POST':
		r = json.loads(request.body)

		first_name = r["pocket_fname"]
		last_name = r["pocket_lname"]		
		pocket_mobileno = r["pocket_mobileno"]
		pocket_dob = r["pocket_dob"]	
		pocket_deviceid = r["pocket_deviceid"]
		pocket_simid = r["pocket_simid"]
		pocket_simid_two = r["pocket_simid_two"]
		pocket_app_pass = r["pocket_app_pass"]
		pocket_app_retype_pass = r["pocket_app_retype_pass"]

		pocket_verified = 1
	               	
		accessToken = '{0:05}'.format(random.randint(2,100000))
		salt = uuid.uuid4().hex
		pocket_access_token =  hashlib.sha256(salt.encode() + accessToken.encode()).hexdigest()
		
		#'{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		pocket_authorization_key =  '{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		nomineedobs = "1111-11-11"

		counter = Pocket.objects.filter(pocket_mobileno__exact = pocket_mobileno).count()

		if counter > 0:
			return JsonResponse({'error':'Mobile Number already exist.'})	
		elif (not pocket_app_pass):
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif len(pocket_app_pass) < 6:
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif len(pocket_app_pass) > 32:
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif (last_name == None):
			return JsonResponse({'error':'Last Name Required.'})
		elif (pocket_mobileno == None):
			return JsonResponse({'error':'Mobile Number Reqired.'})
		elif (pocket_deviceid == None):
			return JsonResponse({'error':'Device ID Reqired.'})
		elif (pocket_simid == None):
			return JsonResponse({'error':'SIM ID Required.' })
		elif (pocket_dob == None):
			return JsonResponse({'error':'Date of Birth Required.'})
		elif (pocket_app_retype_pass == None):
			return JsonResponse({'error':'Retype Password Required.'})
		elif (pocket_app_retype_pass != pocket_app_pass):
			return JsonResponse({'error':'Retype and Password Not Matched.'})
		else:
			customerIDNum = '{0:06}'.format(random.randint(1,10000000))+'101'+pocket_mobileno+'{0:06}'.format(random.randint(1,1000000))
			customer_pin = '{0:06}'.format(random.randint(1,1000000)) + 'UUPOC'+pocket_mobileno+ '{0:06}'.format(random.randint(1,1000000))
			pocket_verification_pin = '{0:06}'.format(random.randint(1,1000000))



			salt_val = uuid.uuid4().hex
			qrCodeNo = hashlib.sha256( salt_val.encode() + pocket_verification_pin.encode() ).hexdigest()

			#print customerIDNum
			p = Pocket()
			p.customer_id = customerIDNum			
			p.pocket_id = customer_pin
			p.pocket_fname = first_name 
			p.pocket_lname = last_name
			p.pocket_mobileno = pocket_mobileno
			p.pocket_dob = pocket_dob
			p.pocket_access_token = pocket_access_token
			p.pocket_authorization_key = pocket_authorization_key
			p.pocket_deviceid = pocket_deviceid
			p.pocket_simid = pocket_simid
			p.pocket_simid_two = pocket_simid_two
			p.pocket_app_pass = dbEncrypt( pocket_app_pass)
			p.pocket_verified = '1'
			p.pocket_verification_pin = pocket_verification_pin
			p.nomineedobs = nomineedobs
			p.pocket_qrcode = '0' + qrCodeNo +'.png' 
			p.pocket_customer_type = '101'
			p.save()


			

			#qr_content = p.pocket_fname+' '+p.pocket_lname+ ' '+p.pocket_mobileno+' '+p.customer_id		
			salt = uuid.uuid4().hex
			qr_content = "https://www.uumoo.biz/?" + qrCodeNo  
			qr = qrcode.QRCode(
				version=6,
				error_correction=qrcode.constants.ERROR_CORRECT_H,
				box_size=10,
				border=4,
			)
			qr.add_data(qr_content)
			qr.make(fit=True)
			img = qr.make_image(fill_color="#000000", back_color="#ffffff")
			img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png');
			im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png')
			im = im.convert("RGBA")
			logo = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/uumoo_logo.png') 
			box = (250,250,350,350) #tested middle position and black background logo
			#box = (200,200,270,270) (270,270,380,380) (135,135,235,235)
			im.crop(box)
			region = logo
			region = region.resize((box[2] - box[0], box[3] - box[1]))
			im.paste(region,box)
			im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0'+ qrCodeNo  +'.png')
			im.show()

			#q = pyqrcode.create(str(qr_content));
			#q.png('profilepic/'+ str(pocket_verification_pin) +'.png' , scale=10)
			#print('QR Code Generated')
			#img.save('/home/uumoogrid/uumoogrid/profilepic/'+ str(pocket_verification_pin) +'.png')
				
		
			ca_counter = CustomerAccounting.objects.filter(Q(userid__exact = p.customer_id )  ).select_related()
			if ( ca_counter.count() <= 0 ):
				ca = CustomerAccounting()	
				ca.userid = p.customer_id
				ca.user_type_id = '101'
				ca.customeraccount_transaction_type = ''
				ca.customeraccount_debit = 0.00
				ca.customeraccount_credit = 0.00
				ca.comission_amount = 0.00
				ca.commission_slave_amount = 0.00
				ca.service_fee_amount = 0.00
				ca.customer_referral_withdrawl_amount = 0.00
				ca.customer_balance = 0.00
				ca.business_id = ''
				ca.auxilary_accounting_userid = ''
				ca.referral_id = ''
				ca.save()




			#sms sending
			#sms_url = "http://api.zaman-it.com/api/sendsms/plain?user=01720208362&password=d01720208362&sender=Friend&SMSText=Your Pocket Verification Code:" + str(pocket_verification_pin) +"&GSM=88"+ str(pocket_mobileno) +""
			#resp = requests.get(sms_url)
			#print resp.content

 
		return JsonResponse({'success':'true','msg':'Pocket Registraion Finished.','verification_pin': pocket_verification_pin })
	else:
		return JsonResponse({'error':'check your request method'})
		




#pin verification api test
@csrf_exempt
def pocketPinVerification(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)
	
		pocket_mobileno = r['pocket_mobileno']
		pocket_deviceid = r['pocket_deviceid']
		pocket_simid = r['pocket_simid']
		pocket_verification_code = r['pocket_verification_code']

		#cond1 = Q(pocket_mobileno__contains = pocket_mobileno)
		#cond2 = Q(pocket_deviceid__contains = pocket_deviceid)
		#cond3 = Q(pocket_simid__contains = pocket_simid)
		#cond4 = Q(pocket_verification_pin__contains = pocket_verification_code)	
		#counter = Pocket.objects.filter(cond1 & cond2 & cond3 & cond4).count()

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_deviceid__exact = pocket_deviceid) & Q(pocket_simid__exact = pocket_simid) & Q(pocket_verification_pin__exact = pocket_verification_code)).select_related()
	

		if counter.count() > 0 : 
			return JsonResponse({'success':'true' , 'msg': 'PIN Verified Successfully.' })
		elif(not pocket_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not pocket_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not pocket_simid):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not pocket_verification_code):
			return JsonResponse({'error': 'Verification PIN Required.'})
		else:		
			return JsonResponse({'error': 'Wrong pin verification.'})
	else:
		return JsonResponse({'error':'check your request method'})



	
#transaction pin verification api test
@csrf_exempt
def pocketTransactionPinVerification(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobileno = r['pocket_mobileno']
		pocket_deviceid = r['pocket_deviceid']
		pocket_simid = r['pocket_simid']
		pocket_transaction_pin = r['pocket_transaction_pin']
		pocket_confirm_transaction_pin = r['pocket_confirm_transaction_pin']
        
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_deviceid__exact = pocket_deviceid) & Q(pocket_simid__exact = pocket_simid) ).select_related()

	
		if counter.count() <= 0 :
			return JsonResponse({'error': 'Mobile Number Not Exist.' })
		elif(not pocket_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not pocket_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not pocket_simid):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not pocket_transaction_pin):
			return JsonResponse({'error': 'Transaction PIN Required.'})
		elif(not pocket_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction Confirm PIN Required.'})
		elif(pocket_transaction_pin != pocket_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction and  Confirm PIN Not Matched.'})
		else:             
			customer_id = counter[0].customer_id
			transaction_pass = dbEncryptTwo(pocket_transaction_pin)
			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_transaction_pass = transaction_pass)

			return JsonResponse({'success':'true', 'msg': 'Your Transaction PIN updated successfullly.'  })

	else:
		return JsonResponse({'error':'check your request method'})




#pocket login api test
@csrf_exempt
def pocketLogin(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobileno = r['pocket_mobileno']
		pocket_deviceid = r['pocket_deviceid']
		pocket_simid    = r['pocket_simid']      
		pocket_app_pass = dbEncrypt(r['pocket_app_pass'])
		pocket_status_active_frozen = '0'
		

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_deviceid__exact = pocket_deviceid) & Q(pocket_simid__exact = pocket_simid) & Q(pocket_app_pass__exact = pocket_app_pass)  ).select_related()


		#& Q(pocket_app_pass__exact = pocket_app_pass )
 
		if(not pocket_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not pocket_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not pocket_simid):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not pocket_app_pass):
			return JsonResponse({'error': 'Password Required.'})
		else:
			if(counter.count() <= 0):
				return JsonResponse({'error': 'Please check your username and password.' ,'success':'false' })
			else:	
				if(counter[0].pocket_status_active_frozen == '0'):
					return JsonResponse({'error': 'Account freeze' })
				else: 	
					permanent_info = PocketAddress.objects.filter(Q(address_id__exact = counter[0].pocket_permanent_address_id) ).select_related()
					if(permanent_info.count() >0):
						permanent_address = {'id': permanent_info[0].address_id ,	'address': permanent_info[0].reg_address ,'division': permanent_info[0].division ,'thana': permanent_info[0].thana ,'district': permanent_info[0].district ,'postcode': permanent_info[0].postcode , 'longitude': permanent_info[0].longitude , 'latitude': permanent_info[0].latitude }
					else:
						permanent_address = {'id': '' ,'address': '' ,'division': '' , 'thana': '' ,'district': '' , 'postcode': '' , 'longitude': '' , 'latitude': '' }



					present_info = PocketAddress.objects.filter(Q(address_id__exact = counter[0].pocket_present_address_id)).select_related()
					if(present_info.count() > 0 ):
						present_address = {'id': present_info[0].address_id ,'address': present_info[0].reg_address ,'division': present_info[0].division ,'thana': present_info[0].thana ,'district': present_info[0].district , 'postcode': present_info[0].postcode , 'longitude': present_info[0].longitude , 'latitude': present_info[0].latitude  }
					else:
						present_address = {'id': '' ,'address': '', 'division': '', 'thana': '' , 'district': '' ,'postcode': '' ,'longitude': '' ,'latitude': '' }





					return JsonResponse({'success':'true', 'msg': 'Logined Successfully.','user_type': counter[0].pocket_customer_type , 'mobile_number' : counter[0].pocket_mobileno ,'device_id' : counter[0].pocket_deviceid , 'sim_id' : counter[0].pocket_simid ,'sim_id_two': counter[0].pocket_simid_two , 'access_token' : counter[0].pocket_access_token , 'authorization_key' : counter[0].pocket_authorization_key,'fname': counter[0].pocket_fname ,'mname': counter[0].pocket_mname ,'lname': counter[0].pocket_lname , 	'dob': counter[0].pocket_dob ,'gender': counter[0].pocket_gender ,'vid': counter[0].pocket_voterlD ,'nid': counter[0].pocket_nid ,'email':counter[0].pocket_email,'alternate_mobile': counter[0].pocket_alternate_mobileno  ,'photo': counter[0].pocket_photo ,'nid_image': counter[0].pocket_nid_image ,'qrcode': counter[0].pocket_qrcode ,'nominee_fname': counter[0].pocket_nominee_fname ,'nominee_mname': counter[0].pocket_nominee_mname ,'nominee_lname': counter[0].pocket_nominee_lname , 	'nominee_contactno': counter[0].pocket_nominee_contactno ,'nominee_voterid': counter[0].pocket_nominee_voterid ,'nominee_nid': counter[0].pocket_nominee_nid ,	'nominee_relation': counter[0].pocket_nominee_relation , 'nominee_voterid_image': counter[0].pocket_nominee_voterid_scancopy ,'permanent_address':permanent_address , 'present_address': present_address  })
	else:
		return JsonResponse({'error':'check your request method'})





#illegal login attempt consequently 3 times wrong password
@csrf_exempt
def threeTimeWrongPassword(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobileno = r['pocket_mobile']

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

		if (counter.count() > 0 ):		
			Pocket.objects.filter(customer_id__exact = counter[0].customer_id ).update(pocket_status_active_frozen  = '1' )
			return JsonResponse({'success':'true' , 'msg':'Acount deactivated successfully.'})
		else:
			return JsonResponse({'error':'Mobile Number not Exist'})
	else:
		return JsonResponse({'error':'Please Check your request method'})




#for file upload function

def handle_uploaded_file(file, filename):
    if not os.path.exists('profilepic/'):
        os.mkdir('profilepic/')
 
    with open('profilepic/' + filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)



#common file upload method
@csrf_exempt
def imageFilesUpload(request):
	#r = requests.post(url, files=files)
	file = request.FILES #['file']

	url = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'#'http://httpbin.org/post'
	files = {'file': one(file , 'rb')}
	values = {'author': 'John Smith'}
	requests.post(url, files=files, data=values)
	#filename = file.name

	#handle_uploaded_file(file, filename):


@csrf_exempt
def imageFileseInformationUpdate(request):
	if request.method == 'POST':
		#r = json.loads(request.body)
		customer_mobile = request.POST.get('customer_mobile')	
		image_type = request.POST.get('image_type')
		pocket_image = request.FILES['avatar'].name  #r['pocket_image')
		extension = pocket_image.split(".")[-1]
	

		if(not customer_mobile):
			return JsonResponse({'error':'Mobile Number must not be Empty.'})	
		elif(not image_type):
			return JsonResponse({'error':'Image Type must not be Empty.'})
		elif(not pocket_image):
			return JsonResponse({'error':'Image must not be Empty.'})
		else:
			token_generate = '{0:06}'.format(random.randint(1,10000000))			
			salt = uuid.uuid4().hex
			img =  hashlib.sha256(salt.encode() + token_generate.encode()).hexdigest()+'.'+extension
			handle_uploaded_file(request.FILES['avatar'] , img)

			counter = Pocket.objects.filter(pocket_mobileno__exact = customer_mobile ).select_related()

			if(counter.count() > 0):
				customer_id = counter[0].customer_id
				if(image_type == 'profile_image'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_photo=img)
				elif(image_type == 'nid'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nid_image=img)
				elif(image_type == 'nominee_nid'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_voterid_scancopy=img)
				else:
					return JsonResponse({'error':'Image type error'})


				
				return JsonResponse({'status':'true','filename': img , 'msg': 'Information updated successfully.'})
			else:
				return JsonResponse({'error':'Mobile Number Not Exist'})

	else:
		return JsonResponse({'error':'check your request method'})


#pocket profile api test
@csrf_exempt
def pocketBasicProfile(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_fname = r['pocket_fname']
		pocket_mname = r['pocket_mname']
		pocket_lname = r['pocket_lname']
		pocket_gender = r['pocket_gender']
		pocket_alternate_mobileno = r['pocket_alternate_mobileno']

		pocket_email = r['pocket_email']
		pocket_mobileno = r['pocket_mobileno']
	        
		pocket_nid_voter = r['pocket_nid_voter']
		pocket_nid_or_voter = r['pocket_nid_or_voter']
	
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

		#profile_photo = '{0}_{1}'.format("100x100" , request.FILES['pocket_photo'])
		#handle_uploaded_file( File(profile_photo) , "100x100_"+ str(request.FILES['pocket_photo']) )
	
		pocket_nid = None
		pocket_voterlD = None 
		if pocket_nid_or_voter == 'NID':
			pocket_nid =  pocket_nid_voter
			pocket_voterlD = ""
		else:
			pocket_voterlD = pocket_nid_voter
			poket_nid = ""

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

	        #profile_photo = '{0}_{1}'.format("100x100" , request.FILES['pocket_photo'])
	        #handle_uploaded_file( File(profile_photo) , "100x100_"+ str(request.FILES['pocket_photo']) )

		if(not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			if(counter.count() > 0 ):
				customer_id = counter[0].customer_id
				if(pocket_fname is not None and pocket_fname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_fname = pocket_fname )
				if(pocket_mname is not  None and pocket_mname !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_mname = pocket_mname )
				if(pocket_lname is not None and pocket_lname!=''):	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_lname = pocket_lname )
				if(pocket_gender is not None and pocket_gender!=''):	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_gender = pocket_gender )
				if(pocket_nid is not None and pocket_nid !=''): 	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nid = pocket_nid )
				if(pocket_voterlD is not None and pocket_voterlD !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_voterlD = pocket_voterlD )
				if(pocket_alternate_mobileno is not None and pocket_alternate_mobileno != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_alternate_mobileno = pocket_alternate_mobileno )
				if(pocket_email is not None and pocket_email !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_email = pocket_email )
		    	

				#if (request.FILES['pocket_photo'] is not None and request.FILES['pocket_photo'] != ''):	
				#handle_uploaded_file( request.FILES['pocket_photo'] , request.FILES['pocket_photo'].name )
				#image1 = Image.open("/home/ec2-user/uumoogrid/uumoogrid/profilepic/"+ request.FILES['pocket_photo'].name)
				#new_image1 = image1.resize((100, 100))
				#new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/100x100_'+request.FILES['pocket_photo'].name)
                        	#new_image1 = image1.resize((40, 40))
                        	#new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/40x40_'+request.FILES['pocket_photo'].name)
				#new_image1 = image1.resize((25, 25))                        
		                #new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/25x25_'+request.FILES['pocket_photo'].name)
				#Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_photo = request.FILES['pocket_photo'].name )


				return JsonResponse({'msg':'profile update complete'})
			else:
				return JsonResponse({'msg':'Please check Mobileno,DeviceID,SIMID'})
	else:
		return JsonResponse({'error':'check your request method'})




#pocket address profile update api test
@csrf_exempt
def pocketAddressProfile(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)

		permanent_address = r["permanent_address"]
		permanent_division = r["permanent_division"]
		permanent_district = r["permanent_district"]
		permanent_thana = r["permanent_thana"]
		permanent_postcode = r["permanent_postcode"]

		present_address = r["present_address"]
		present_division = r["present_division"]
		present_district = r["present_district"]
		present_thana  = r["present_thana"]
		present_postcode = r["present_postcode"]

		pocket_mobileno = r['pocket_mobileno']

		if(not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) ).select_related()
			customer_id = counter[0].customer_id
			per_id = counter[0].pocket_permanent_address_id			
			pre_id = counter[0].pocket_present_address_id


		if(per_id == None):
			#print('block-001')
			pa = PocketAddress()
			pa.reg_address = permanent_address
			pa.division = permanent_division
			pa.thana =  permanent_thana
			pa.district = permanent_district
			pa.postcode = permanent_postcode
			pa.longitude = ''
			pa.latitude = ''
			pa.save()

			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_permanent_address_id = pa.address_id )      
		else:
			if(permanent_address is not None and permanent_address !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(reg_address = permanent_address)
			if(permanent_division is not None and permanent_division !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(division = permanent_division)
			if(permanent_thana is not None and permanent_thana !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(thana = permanent_thana)
			if(permanent_district is not None and permanent_district !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(district = permanent_district)
			if(permanent_postcode is not None and permanent_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(postcode = permanent_postcode)

		#present address
		if(pre_id == None):

			paa = PocketAddress()
			paa.reg_address = present_address
			paa.division = present_division
			paa.thana =  present_thana
			paa.district = present_district
			paa.postcode = present_postcode
			paa.longitude = ''
			paa.latitude = ''
			paa.save()

			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_present_address_id = paa.address_id )      

		else:
			if(present_address is not None and present_address !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(reg_address = present_address)
			if(present_division is not None and present_division !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(division = present_division)
			if(present_thana is not None and present_thana !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(thana = present_thana)
			if(present_district is not None and present_district !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(district = present_district)
			if(present_postcode is not None and present_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(postcode = present_postcode)

		

		
		return JsonResponse({'msg':'profile address updated.' }) 
	else:
		return JsonResponse({'error':'check your request method'})





#pocket registration api ui test
def pocketSignUpUI(request):
	context = {}
	return render(request , 'api/pocket_reg.html' , context)

#pin verification api test ui
def pocketPinVerificationUI(request):
	context = {}
	return render(request , 'api/pin_verification.html' , context)

#pocket transaction pin verification ui test
def pocketTransactionPINVerificationUI(request):
	context = {}
	return render(request , 'api/transaction_pin_verification.html' , context)

#pocket login ui test
def pocketLoginUI(request):
	context = {}
	return render(request , 'api/pocket_login.html' , context )

#pocket profile ui test
def pocketBasicProfileUI(request):
	context = {}
	return render(request , 'api/pocket_profile.html', context )
 
#pocket address profile ui test
def pocketAddressProfileUI(request):
	context = {}
	return render(request , 'api/pocket_address_profile.html' , context )



