# -*- coding: utf-8 -*-
#encoding=utf-8
from django.views.decorators.csrf import csrf_exempt



import os
import sys
import time
import math
from datetime import datetime , date , timedelta
from urllib.request import *
from django.core import serializers
from django.shortcuts import render
from django.http import JsonResponse , HttpResponse
from PIL import Image
from django.db.models import Q
from uumooapi.models import * 
from django.utils import *
from django.core.files import File



from PIL import Image , ImageDraw, ImageFont , ImageEnhance, ImageFilter
from PIL.ImageOps import posterize
from resizeimage import resizeimage

import codecs
import pyocr
import pyocr.builders as pb
from pyocr import tesseract as tool
import cv2
import numpy as np

import random

from codecs import encode, decode
import locale
import requests
import qrcode
import pyqrcode
# import QRCode
from uumoogrid.settings import MEDIA_URL ,HOSTNAME,DATABASE,USERNAME,PASSWORD,PORTNO,BKEY , DRAWER_FCM_KEY

from Crypto import Random
from Crypto.Cipher import AES
from Crypto.Hash import SHA256
import base64
import hashlib
import uuid
import binascii,os
import requests
import psycopg2
import json
import decimal

import zlib
import calendar

from fpdf import FPDF
from pyfcm import FCMNotification

from uumooapi.testaes import *
from uumooapi.simpleaes import *

from resizeimage import resizeimage
from uumooapi.pytesseract import *




#==================================================
@csrf_exempt
def dataAdd(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		data = r['data']

		#APIAesDetails.objects.filter(Q(app_name__exact='drawer')).update(broken_key='hsedalgnabiytrewq321@ylilrabrud#')
		#d = Districts()
		#d.id = 4
		#d.division_id = 3
		#d.district_name = 'Gopalganj'
		#d.district_bn_name = u'.........'
		#d.save()


		return JsonResponse({'success':'true' , 'msg':'data added'})
	else:
		return JsonResponse({'error':'Please check your request method'})





@csrf_exempt
def testAPI(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		sender_mobile = r['sender_customer_mobile']
		sender_type = r['sender_customer_type']
		receiver_mobile = r['receiver_customer_mobile']
		receiver_type = r['receiver_customer_type']
		transaction_pass = dbEncrypt(r['transaction_pass'])
		trans_pass = r['transaction_pass']
		pay_amount = r['pay_amount']

		con = psycopg2.connect(
			host = str(HOSTNAME) ,
			database = str(DATABASE), 
			user = str(USERNAME), 
			password= str(PASSWORD), 
			port = str(PORTNO) 
		)
		cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		strs = "select drawer_payment('"+ sender_mobile  +"','"+ sender_type  +"','"+ receiver_mobile +"','"+ receiver_type +"','"+ transaction_pass +"','"+ pay_amount +"') as actual_amount;";
		print(strs)
		rw = cur.execute(strs)
		one = cur.fetchone()
		actual_amount =  one['actual_amount']
		cur.close()
		con.close()

		print(actual_amount)

		return JsonResponse({'success':'true' , 'msg':'API Testing'})

	else:
		return JsonResponse({'error':'Please check your request method'})







#####============================================================================================

@csrf_exempt
def nidOCR(request):
	if request.method == 'POST':
		ocr_image = request.FILES['avatar'].name
		print(ocr_image)

		extension = ocr_image.split(".")[-1]
		handle_uploaded_file(request.FILES['avatar'] , ocr_image)

		
		#================================================ image enhanceing ===============================
		img = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ ocr_image ) 
		img.filter(ImageFilter.SHARPEN)
		#img.filter(ImageFilter.EDGE_ENHANCE)
		#img.filter(ImageFilter.FIND_EDGES)
   
		#img = img.filter(ImageFilter.MedianFilter())
		ImageEnhance.Brightness(img).enhance(1.5)
		enhancer = ImageEnhance.Contrast(img)
		img = enhancer.enhance(2)
		img = img.convert('L')
		img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp222201.jpg' , 'JPEG' , quality=100)


		#================ OCR Working ===========================
		UL = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp222201.jpg'
		txt = image_to_string(Image.open(UL),lang="eng",config="--psm 6 --oem=3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")		


		#txt_res = image_to_boxes(UL)
		#res = txt.split('\n')
		#x,full_name = (res[5]).split(':')
		#y,dob = (res[8]).split(':')
		#z,idno = (res[10]).split(':')
		#data = full_name+' , '+ dob + ' , '+ idno
		print(txt)
		#print(txt_res)

		return JsonResponse({'success':'true' , 'data': txt })
	else:
		return JsonResponse({'error':'Please check your request method'})





#====================== for test ================================================
#=======================OCR -Testing ============================================
@csrf_exempt
def TestOcr(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		img = r['img']
		print(MEDIA_URL)

		#UL = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp21.jpg'		
		#print(UL)
		print('OCR Result:')
		print("Will use tool '%s'" % (tool.get_name()))	


		#tessdata_dir_config = r'--psm 6 --tessdata-dir "/usr/local/share/tessdata"'
		#txt = image_to_string(Image.open(UL) ,lang="eng" ,config='-psm 6 --oem=3 -c tessedit_char_whitelist=-01234567890XYZ:' )
		#============================read successfully======================================================
		#txt = image_to_string(Image.open(UL),lang="eng",config="--psm 6 --oem=3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")
		#print(txt)


		#=========================================only for png format =======================================
		#UL = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp225.png'
		#im = cv2.imread('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/enhance_downloadfile-scale-1.png' , 0)
		#ret , thresh = cv2.threshold(im, 12, 255, cv2.THRESH_OTSU)
		#print ("Threshold selected : %s"% ret)
		#cv2.imwrite(UL , thresh)

		#=========================================only for jpg format ===============================
		#im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/nid2.jpg') #downloadfile-scale-1.jpeg')#the second one 
		#im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/downloadfile-scale-10.jpg')
		#im = im.filter(ImageFilter.MedianFilter())
		#ImageEnhance.Brightness(im).enhance(1.0)
		#enhancer = ImageEnhance.Contrast(im)
		#im = enhancer.enhance(6)
		#im = im.convert('1')
		#im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp22220.jpg') 
		
		#im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/downloadfile-scale-1.jpeg')		
		#im.resize((1400,1200), Image.ANTIALIAS)
		#im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp22220.jpg' , 'JPEG' , quality=100)

		img = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/tuhin_vay.jpg') #downloadfile-scale-1.jpeg')
		img = img.filter(ImageFilter.MedianFilter())
		ImageEnhance.Brightness(img).enhance(1.5)
		enhancer = ImageEnhance.Contrast(img)
		img = enhancer.enhance(2)
		img = img.convert('L')

		img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp222201.jpg' ,'JPEG',quality=100) 

		UL = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/temp222201.jpg'
		txt = image_to_string(Image.open(UL),lang="eng",config="--psm 6 --oem=3 -c tessedit_char_whitelist=0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz")		

		print(txt)

		#res = txt.split('\n')
		#print("%s %s %s"% (res[5],res[8],res[10]))
		#print("------------------------------------------------------------------------------------------------------")
		#print(res[5])		
		#print(res[8])
		#print(res[10])			
		#print("------------------------------------------------------------------------------------------------------")
		#x,full_name = (res[5]).split(':')
		#y,dob = (res[8]).split(':')
		#z,idno = (res[10]).split(':')
		#print(full_name)
		#print(idno)
		#print(dob)
		#dobr = datetime.datetime.strptime(dob.strip() , '%d %b %Y').strftime('%Y-%m-%d')
		#print(dobr)
		print("------------------------------------------------------------------------------------------------------")

		#print(txt)

		return JsonResponse({'success':'OCR Reader'})
	else:
		return JsonResponse({'error':'Please check your request method'})



#===================================================================================================================================
#========================================================== API Start ==============================================================
#===================================================================================================================================


def changeAesKey():

	details = APIAesDetails.objects.filter(Q(admin_id__exact = '111')).select_related()
	aes_data = {}
	aes_data["type"] = "esakey"
	for dt in details:
		aes_data[ str(dt.aescode) ] = str(dt.broken_key)


	print(aes_data)
	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
	pockets = Pocket.objects.exclude( Q(customer_id__isnull = True) ).select_related()
	for p in pockets:
		result = push_service.single_device_data_message(registration_id = p.pocket_fcm_token , data_message = aes_data )
		print(result)


	cashboxs = Cashbox.objects.exclude(Q(cashbox_customer_id__isnull = True )).select_related()
	for ch in cashboxs:
		push_service.single_device_data_message(registration_id = ch.cashbox_fcm_token , data_message = aes_data )	








@csrf_exempt
def updateExistAesKey(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		dt = r['data']

		
		details = APIAesDetails.objects.filter(Q(admin_id__exact = '111')).select_related()
		aes_data = {}
		aes_data["type"] = "esakey" 
		for dt in details:
			aes_data[ str(dt.aescode) ] = str(dt.broken_key) 



		print(aes_data)
		push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
		pockets = Pocket.objects.exclude( Q(customer_id__isnull = True) ).select_related()
		for p in pockets:		
			result = push_service.single_device_data_message(registration_id = p.pocket_fcm_token , data_message = aes_data )
			print(result)



		cashboxs = Cashbox.objects.exclude(Q(cashbox_customer_id__isnull = True )).select_related()
		for ch in cashboxs:
			push_service.single_device_data_message(registration_id = ch.cashbox_fcm_token , data_message = aes_data )





		return JsonResponse({'success':'true' , 'data': 'data pushed '  })
	else:
		return JsonResponse({'error':'Please check your requet method'})






@csrf_exempt
def divisionDistrictThanaList(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']

		divisions = Divisions.objects.exclude(division_name__isnull = True ).select_related()
		data = []
		for d in divisions:
			districts = Districts.objects.filter(Q(division_id__exact = d.id)).select_related()
			district_data = []
			for dt in districts:
				thanas = Thanas.objects.filter(Q(district_id__exact = dt.id )).select_related()
				thana_list = []
				for th in thanas:
					thl = {
						'thana_id': str(th.id) ,
						'thana_name': th.thana_name  ,
						'thana_bn_name': th.thana_bn_name 
					}
					thana_list.append(thl)
				dl = {
					'district_id': str(dt.id) ,
					'district_name': dt.district_name ,
					'district_bn_name': dt.district_bn_name,
					'thana': thana_list
				}
				district_data.append(dl)

			datas = {
				'division_id': str(d.id) ,
				'division_name': d.division_name ,
				'division_bn_name':  d.division_bn_name,
				'district': district_data
			}
			data.append(datas)


		return JsonResponse({'success':'true', 'data': data })
	else:
		return JsonResponse({'error':'Please check your request method'})







#========================================================== AES API Details ========================================================
@csrf_exempt
def getAESAPIList(request):
	if request.method == 'POST':
		try:
			r = json.loads(request.body)
			mobile_number  = r['mobile_number']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			user_type = r['user_type']
			device_id = r['device_id']		

			print("%s %s %s %s %s"%(mobile_number,sim_id_one, sim_id_two,user_type,device_id))
			details = APIAesDetails.objects.filter(Q(admin_id__exact = '111')).select_related()
			data = []

			for dt in details:
				data_holder = {
					'board': dt.aescode ,
					'bread': dt.broken_key 
				}		
				data.append(data_holder)

			return JsonResponse({'success':'true' , 'data': data })
		except:
			return JsonResponse({'error':'Please check your request parameters' })
                        
	else:
		return JsonResponse({'error':'Please check your request method'})
	



#=========================== Test API for PostGreSQL Function Call ========================================
@csrf_exempt
def pushData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		sender_mobile = r['sender_customer_mobile']
		sender_type = r['sender_customer_type']
		receiver_mobile = r['receiver_customer_mobile']
		receiver_type = r['receiver_customer_type']
		transaction_pass = dbEncrypt(r['transaction_pass'])
		pay_amount = r['pay_amount']

		con = psycopg2.connect(
			host = str(HOSTNAME) ,
			database = str(DATABASE), 
			user = str(USERNAME), 
			password= str(PASSWORD), 
			port = str(PORTNO) 
		)
		cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		strs = "select drawer_payment('"+ sender_mobile  +"','"+ sender_type  +"','"+ receiver_mobile +"','"+ receiver_type +"','"+ transaction_pass +"','"+ pay_amount +"') as actual_amount;";
		#strs = "select get_customer_info('"+ str(userid)  +"','"+ customer_type  +"') as actual_amount;";
		#strs = "select get_drawer_current_balance('"+ str(userid)  +"') as actual_amount;"				
		print(strs)
		rw = cur.execute(strs)
		one = cur.fetchone()	
		actual_amount =  one['actual_amount']

		cur.close()
		con.close()

		return JsonResponse({'amount': str(actual_amount)  })
	else:
		return JsonResponse({'error':'Please check your request method.'})












#======================== Test Image resize ==============================================
@csrf_exempt
def profileImageReDefined(request):

	url = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/20e4bce334b1fc435b02026df41157e44241951d9d225aba7c7707b82bdfb9bbxx.jpg'
	image = Image.open(url)
	image = image.resize((60, 60))
	#image.resize((w,h) , Image.ANTIALIAS)
	draw = ImageDraw.Draw(image)
	draw.ellipse((0,0, 59, 59) , outline="black" )# fill=(255,0,0,0))
	#draw.ellipse((x-r, y-r, x+r, y+r), fill=(255,0,0,0))	
	image.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/20e4bce334b1fc435b02026df41157e44241951d9d225aba7c7707b82bdfb9bbxxxx.jpg')

	return JsonResponse({'success':'true'})




#================================== Sample Notification ===================================================
@csrf_exempt
def sampleNotification(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		sender_token = r['sender_token']
		receiver_token =r['receiver_token']
		print("%s %s"%(sender_token , receiver_token))	

		push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")
		data_message = {
			"msg_title":"Pay Successful" ,
			"trxid":"YWUFh4898hkswufmd985Hf",
			"sender":"01719347580",
			"receiver":"01714516763" ,
			"amount":"100.00TK",
			"service_fee":"2.00TK",
			"transaction_type":"deposit",
			"type":"req",
			"date":"2018-09-04 17:04"
		}
		data_message1 = {
			"msg_title":"Transfer Successful" ,
			"trxid":"YWUFh4898hkswufmd985Hf",
			"sender":"01719347580",
			"receiver":"01714516763" ,
			"amount":"100.00TK",
			"service_fee":"2.00TK",
			"transaction_type":"transfer",
			"type":"trn",
			"date":"2018-09-04 17:04"
		}

		#for single data notification
		result1 = push_service.single_device_data_message(registration_id=receiver_token , data_message=data_message)

		result2 = push_service.single_device_data_message(registration_id=sender_token , data_message=data_message1)

		#result = push_service.multiple_devices_data_message(registration_ids=registration_ids, data_message=data_message)	
		print("Push response: %s"% result1)

		return JsonResponse({'success':'true','msg':'Notification send successfully'})
	else:
		return JsonResponse({'error':'Please check your request method'})





#===================== PostGresql Encrypt=================================
@csrf_exempt
def dbEncryptTest(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		con = psycopg2.connect(
			host = str(HOSTNAME) ,
			database = str(DATABASE) ,
			user = str(USERNAME) , 
			password= str(PASSWORD),
			port = str(PORTNO) 
		)
		user_str = r['data']
		cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		pass_txt = user_str #request.GET.get("pass") 
		ukey = 'uumoo'
		rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
		one = cur.fetchone()	
		enc_pass = b"".join( one['pt'])

		print ("Enc: \\x%s" % str( binascii.b2a_hex( enc_pass), 'utf-8') )	
		cur.close()
		con.close()
		
		return JsonResponse({ "success":"true" , "enc": "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8')  })
	else:
		return JsonResponse({'error':'Please check your request method'})
 



#======================= PostGresql Decrypt ================================================
@csrf_exempt
def dbDecryptTest(request):
	if request.method == 'POST':
		r = json.loads(request.body)	
		con = psycopg2.connect(
			host = str(HOSTNAME) , 
			database = str(DATABASE) , 
			user = str(USERNAME) ,
			password= str(PASSWORD) ,
			port =  str(PORTNO) 
		)
		ukey = 'uumoo'
		enc_pass = str(r['data'])
		curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
		curr.execute("select convert_from(decrypt('"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
		two = curr.fetchone()
		data = str(two[0])
		curr.close()
		
		return JsonResponse({'success':'true' , 'dec': data })
	else:
		return JsonResponse({'error':'Please check your request method'})

#====================== for test=================================================







#===========================Drawer Common Notification  =======================================================
def sendCommonNotificationForDrawer(fcm_token , title , msg_body ,msg_type , msg_date,  custom_date, msg_time , mobile_no , profile_pic ):
	
	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")
	
	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
	#geo = bussiness logo
	if(msg_type == 'sys'):
		data_message = {
			"msg_title": title ,
			"msg": msg_body , 
			"type": msg_type  ,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no": mobile_no ,   #"01300000000" ,
			"time": msg_time 		
		}	
	elif(msg_type == 'geo'):
		data_message = {
			"msg_title": title ,
			"msg": msg_body ,
			"type": msg_type  ,
			"img": profile_pic,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no":mobile_no ,  #"01300000000" ,
			"time": msg_time
		}
	else:
		data_message = {
			"msg_title": title ,
			"msg": msg_body ,
			"type": msg_type  ,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no": mobile_no , #"01300000000" ,
			"time": msg_time
		}


	result = push_service.single_device_data_message(registration_id=fcm_token , data_message=data_message)
	#result = push_service.notify_single_device(registration_id=fcm_token , message_title=title , message_body=msg_body)
	return result



def sendTransactionNotificationWithName(flag,sender_full_name,fcm_token,title,trxid,sender,receiver,amount,service_fee,transaction_type,transaction_date):
	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")

	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
                      
	if(flag == 1):
		#sender
		data_message = {
			"msg_title": title ,
			"trxid": trxid ,
			"msg": "To:"+ receiver +", Amount:"+amount ,
			"name": sender_full_name ,
			"sender": sender ,
			"receiver": receiver ,
			"amount": amount ,
			"service_fee": service_fee ,
			"transaction_type": transaction_type ,
			"type":"trn" ,
			"date": transaction_date
		}
	else:
		#receiver
		data_message = {		
			"msg_title": title ,
			"trxid": trxid ,
			"msg": "From:"+ sender +", Amount:"+amount ,
			"name": sender_full_name ,
			"sender": sender ,
			"receiver": receiver ,
			"amount": amount ,
			"service_fee": service_fee ,
			"transaction_type": transaction_type ,
			"type":"trn" , #trn=Transaction
			"date": transaction_date
		}


        #for single data notification
	result = push_service.single_device_data_message(registration_id=fcm_token , data_message=data_message)


	return result







#=====================Cashbox Forgot Both ===========================
@csrf_exempt
def forgotBothDrawer(request):
	if request.method == 'POST':
		data = json.loads(request.body)
		params = getDecryptedValue(data['data'] , BKEY)
		r = json.loads(params)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']		
		dob = r['dob']
		nominee_name = r['nominee_name']
		flag = r['flag']
		nid_vid = r['nid_vid']
		last_transaction_amount = r['last_transaction_amount']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not dob):
			return JsonResponse({'error':'Date of Birth cannot be empty'})
		elif(not nominee_name):
			return JsonResponse({'error':'Nominee Name cannot be empty'})
		elif(not flag):
			return JsonResponse({'error':'Flag cannot be empty'})
		elif(not nid_vid):
			return JsonResponse({'error':'NID or Voter ID cannot be empty'})		
		elif(not last_transaction_amount):
			return JsonResponse({'error':'Last Transaction amount cannot be empty'})
		else:
			cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one)).select_related()				
			if(cashbox_counter.count() == 0):
				return JsonResponse({'error':'Mobile number not found' })				
			else:
				cashbox_user_id = cashbox_counter[0].cashbox_customer_id
				transaction = CustomerTransaction.objects.filter(Q(transaction_amount__exact=last_transaction_amount) & (Q(sender_userid__exact=cashbox_user_id) | Q(receiver_userid__exact=cashbox_user_id))).order_by('-transaction_timestamp').select_related()


				checkdob = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_dob__exact=dob)).select_related()

				checknomineeinfo = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_nominee_lname__exact=nominee_name)).select_related()

				checknidvid = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & (Q(cashbox_nationalD__exact=nid_vid) | Q(cashbox_vodetid__exact=nid_vid))).select_related()

				if(checkdob.count() ==0 ):
					return JsonResponse({'error':'Date of birth not matched'})
				elif(checknomineeinfo.count() == 0):
					return JsonResponse({'error':'Nominee name not matched'})
				elif(checknidvid.count() == 0):
					return JsonResponse({'error':'NID/VID not matched'})
				elif(transaction.count() == 0):
					return JsonResponse({'error':'No Transaction found'})
				else:
					return JsonResponse({'success':'true', 'msg':'All answer is matched' })



	else:
		return JsonResponse({'error':'Please check your request method.'})











@csrf_exempt
def drawerChangeTransactionPINUpdate(request):
	if request.method == 'POST':
		data = json.loads(request.body)
		params = getDecryptedValue(data['data'], BKEY)
		r = json.loads(params)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		new_transaction_pin = r['new_transaction_pin']
		confirm_new_transaction_pin = r['confirm_new_transaction_pin']


		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not new_transaction_pin):
			return JsonResponse({'error':'New Transaction PIN cannot be empty'})
		elif(not confirm_new_transaction_pin):
			return JsonResponse({'error':'Confirm New Transaction PIN cannot be empty'})
		elif(confirm_new_transaction_pin != new_transaction_pin):
			return JsonResponse({'error':'New Transaction PIN and Confirm New Transaction PIN Not Matched'})
		else:
			cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) ).select_related()
			if(cashbox_counter.count() == 0):
				return JsonResponse({'error':'Invalid Mobile Number'})
			else:
				transaction_pass = dbEncrypt(new_transaction_pin)
				Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) ).update(cashbox_transaction_pass = transaction_pass)
				return JsonResponse({'success':'true','msg':'Transaction PIN Updated Successfullly.'})



	else:
		return JsonResponse({'error':'Please check your request method'})






@csrf_exempt
def drawerChnageTransactionPIN(request):
	if request.method == 'POST':
		data = json.loads(request.body)
		params = getDecryptedValue(data['data'], BKEY)
		r = json.loads(params)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		app_pass = r['app_pass']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not app_pass):
			return JsonResponse({'error':'Drawer Password cannot be empty'})
		else:
			drawer_pass = dbEncrypt(app_pass)
			cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) & Q(cashbox_app_pass__exact = drawer_pass)).select_related()
			if(cashbox_counter.count() == 0):
				return JsonResponse({'error':'Invalid Mobile Number or Drawer Password'})
			else:
				return JsonResponse({'success':'true','msg':'Drawer Password is correct'})

	else:
		return JsonResponse({'error':'Please check your request method'})







@csrf_exempt
def drawerChangePasswordUpdate(request):
	if request.method  == 'POST':
		data = json.loads(request.body)
		params = getDecryptedValue(data['data'], BKEY)
		r = json.loads(params)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		new_app_pass = r['new_app_pass']		
		confirm_new_app_pass = r['confirm_new_app_pass']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not new_app_pass):
			return JsonResponse({'error':'New Drawer Password cannot be empty'})
		elif(not confirm_new_app_pass):
			return JsonResponse({'error':'Confirm New Drawer Password cannot be empty'})
		elif(new_app_pass != confirm_new_app_pass):
			return JsonResponse({'error':'New Drawer Password and Confirm New Drawer Password cannot be empty'})
		else:
			newPassword = dbEncrypt(new_app_pass)
			cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) ).select_related()
			if(cashbox_counter.count() == 0):
				return JsonResponse({'error':'Invalid Mobile Number'})
			else:
				 
				Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) ).update(cashbox_app_pass = newPassword)	

				
				
				return JsonResponse({'success':'true' , 'msg': 'Drawer Password Updated Successfullly.' })


	else:
		return JsonResponse({'error':'Please check your request method'})






@csrf_exempt
def drawerChangePassword(request):
	if request.method == 'POST':
		data = json.loads(request.body)	
		params = getDecryptedValue(data['data'], BKEY)		
		r = json.loads(params)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']
		transaction_pin = r['transaction_pin']
		transaction_pass = dbEncrypt(transaction_pin)

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(user_type != '102'):
			return JsonResponse({'error':'Invalid User Type'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not transaction_pin):
			return JsonResponse({'error':'Transaction PIN cannot be empty'})
		else:
			cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid_one__exact=sim_id_one) & Q(cashbox_transaction_pass__exact = transaction_pass)).select_related()
			print("mob:%s  sim:%s  device:%s pass:%s" % (mobile_number , sim_id_one , device_id , transaction_pass))
			msg = 'Transaction PIN is correct'
			enc_msg = getEncryptedValue(msg , BKEY)
			print(enc_msg)
			if(cashbox_counter.count() == 0):
				return JsonResponse({'error':'Invalid Mobile Number or Trasaction PIN'})			
			else:
				return JsonResponse({'success':'true','msg': enc_msg  })
		
	else:
		return JsonResponse({'error':'Please check your request method'})










@csrf_exempt
def drawerQRCodeScan(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		drawer_mobile = r['mobile_number'].strip()
		sim_id_one  = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		sender_user_type = r['sender_user_type'].strip()
		device_id = r['device_id'].strip()
		qr_token = '0' + r['qr_token'].strip() +'.png'

		counter = Pocket.objects.filter(Q(pocket_qrcode__exact = qr_token )).select_related()
		if(counter.count() > 0):
			return JsonResponse({'error':'Drawer have no permission to Pay to Pocket'  })
		else:
			cash = Cashbox.objects.filter(Q(cashbox_qrcode__exact = qr_token )).select_related()			
			if ( cash.count() > 0 ):
				mob = cash[0].cashbox_mobileno
				pro_img = cash[0].cashbox_photo
				bcount = Business.objects.filter(Q(app_mobile_number__exact=mob)).select_related()
				if(bcount.count() > 0):
					if(bcount[0].business_name == ''):
						fname = cash[0].cashbox_fname
						lname = cash[0].cashbox_lname
						mname = cash[0].cashbox_mname	
					else:
						fname = bcount[0].business_name
						lname = ''
						mname = ''


					if(bcount[0].business_logo == ''):
						logo = cash[0].cashbox_photo
					else:
						logo = bcount[0].business_logo
				else:
					fname = cash[0].cashbox_fname
					lname = cash[0].cashbox_lname
					mname = cash[0].cashbox_mname
					logo = cash[0].cashbox_photo


				return JsonResponse({'success':'true' , 'customer_mobile': cash[0].cashbox_mobileno , 'first_name': fname , 'last_name': lname , 'middle_name': mname , 'profile_image': logo , 'user_type': cash[0].customer_type_id   })
			else:
				return JsonResponse({'url':'https://www.uumoo.biz/'})

	else:
		return JsonResponse({'error':'Please check your request method'})




#====================================drawer cashout or withdraw ==============================
@csrf_exempt
def drawerWithdraw(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21906G') ).select_related()
		broken_key = hooker[0].broken_key

		try:

			data = json.loads(request.body)
			param = getDecryptedValue(data['data'] , BKEY)
			r = json.loads(param)
			sender_mobile = r['sender_mobile']
			sender_type = r['sender_type']
			receiver_mobile = r['receiver_mobile']
			receiver_type = r['receiver_type']
			cashout_amount = r['cashout_amount']
			transaction_pass = r['transaction_pass']		
			withdraw_reference = r['withdraw_reference']


			if ( not sender_mobile):
				return JsonResponse({'error': 'Sender Mobile Cannot be Empty'})
			elif ( not receiver_mobile):
				return JsonResponse({'error': 'Receiver Mobile Cannot be Empty'})
			elif ( not cashout_amount):
				return JsonResponse({'error': 'Withdraw Amount Cannot be Empty'})
			elif ( not transaction_pass ):
				return JsonResponse({'error': 'Transaction Password Cannot be Empty' })
			elif ( decimal.Decimal(cashout_amount) < 100 ):
				return JsonResponse({'error': 'Minimum Withdraw Amount: 100 TK'})
			else:
			
				sender_info = getCustomerInfo(sender_mobile , sender_type)
				receiver_info = getCustomerInfo(receiver_mobile , receiver_type)
				print("%s , %s"% (sender_info,receiver_info))

				cashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact = sender_info) & Q(cashbox_transaction_pass__exact=dbEncrypt(transaction_pass))).select_related()

				if(sender_info == 0):
					return JsonResponse({'error':'Sender Mobile Not Found'})
				elif(cashboxCount.count() == 0):
					return JsonResponse({'error':'Wrong Transaction Password'})
				else:
					if(receiver_info == 0):
						return JsonResponse({'error':'Receiver Mobile Not Found'})
					else:
						service_fee = (decimal.Decimal(cashout_amount)*1)/100
						total = decimal.Decimal(cashout_amount)+decimal.Decimal(service_fee)

						print("fee:%s, total:%s"% (service_fee , total))

						cas  = CustomerAccounting.objects.filter(Q(userid__exact= sender_info)).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)

						if(decimal.Decimal(sender_balance) > decimal.Decimal(total) ):
							ras = CustomerAccounting.objects.filter(Q(userid__exact=receiver_info)).select_related()
							receiver_balance_total = decimal.Decimal(ras[0].customer_balance)

							blockCounter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=receiver_info) & Q(topup_transaction_status__exact='pending')).select_related()	
							if(blockCounter.count() > 0 ):						
								block_amount = blockCounter[0].topup_transaction_amount
							else:
								block_amount = 0



							receiver_balance = decimal.Decimal(receiver_balance_total) - decimal.Decimal(block_amount)
							sender_current_balance = decimal.Decimal(sender_balance)-decimal.Decimal(total)
							receiver_current_balance = decimal.Decimal(receiver_balance_total)+decimal.Decimal(total)
							print("%s , %s , %s , %s , %s"% (str(decimal.Decimal(total)), sender_balance , receiver_balance_total , sender_current_balance,receiver_current_balance ) )


							if(decimal.Decimal(cashout_amount) > decimal.Decimal(receiver_balance) ):
								return JsonResponse({'error':'Withdraw not occured.Drawer insufficient balance'})
							else: 
								trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))
								ts = time.gmtime()
								currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)							

								#Customer Transaction portion
								cts = CustomerTransaction()
								cts.customer_transaction_id = trx
								cts.sender_userid = sender_info
								cts.receiver_userid = receiver_info
								cts.reference_txt = withdraw_reference
								cts.transaction_amount = decimal.Decimal(cashout_amount)
								cts.transaction_type = '10004'
								cts.transaction_message = 'TRXID: '+ str(trx) + ' Cashout Amount:'+ str(decimal.Decimal(total)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
								cts.cashbox_business_id = ''
								cts.auxilary_userid = ''
								cts.referral_id = ''
								cts.transaction_status = ''
								cts.sender_longitute = ''
								cts.sender_latitude = ''
								cts.receiver_longitute = ''
								cts.receiver_latitude = ''				
								cts.transaction_timestamp = datetime.datetime.now()
								cts.save()						



								CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).update( customer_balance = sender_current_balance )
								CustomerAccounting.objects.filter( Q(userid__exact = receiver_info) ).update( customer_balance = receiver_current_balance )


								#=============================== notification ============================
								sender_cust_detail = customerInfo(sender_info)
								receiver_cust_detail = customerInfo(receiver_info)
								if(sender_cust_detail == 0):
									sender_num = ''
									sender_photo = ''
								else:
									sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')


								if(receiver_cust_detail == 0):
									receiver_num = ''
									receiver_photo = ''
								else:
									receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')


								#==============================notification for initiator merchant=================
								if(sender_type == '102'):	
									msg_title = "Withdraw Request"
									msg = str(cashout_amount) +"TK Deposit request To:"+ receiver_num 
									drawer_fcm_token = receiver_fcm_token
									sender_number = sender_num
									receiver_number = receiver_num
									photo = sender_photo
									transaction_type = "withdraw"
									log_view_type = "personal"
									req_type = "trn"											
									print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
									senderDet = customerNameDetailInfo(sender_info)
									sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
									cashboxNotification(sender_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, cashout_amount, photo, service_fee,transaction_type, req_type,log_view_type ,currentTime)






								if( receiver_type == '102'):
								#===============receviver merchant ===================
									msg_title = "Withdraw Request"
									msg = str(cashout_amount) +"TK Withdraw request From:"+ sender_num
									drawer_fcm_token = receiver_fcm_token
									sender_number = sender_num
									receiver_number = receiver_num
									photo = sender_photo
									transaction_type = "withdraw"
									log_view_type = "drawer"
									req_type = "req"
									print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
									senderDet = customerNameDetailInfo(sender_info)
									sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
									cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, cashout_amount, photo, service_fee,transaction_type, req_type,log_view_type,currentTime)
								else:
								#===============receiver pocket ======================
									receiverDet = customerNameDetailInfo(receiver_info)
									receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
									fcm_title = str(cashout_amount) +"TK Withdraw From:"+ sender_num
									sendTransactionNotification(1,receiver_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(cashout_amount))+"TK" ,str(service_fee)+"TK","withdraw", currentTime  )



								return JsonResponse({'success':'true','service_fee':str(service_fee),'msg':'Withdraw request send successfully'})


		except:
			changeAesKey()

	else:
		return JsonResponse({'error':'Please check your request method'})





#==============================drawer deposit or topup =======================================
@csrf_exempt
def drawerDeposit(request):
	if request.method == 'POST':

		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21907H') ).select_related()
		broken_key = hooker[0].broken_key


		try:

			data = json.loads(request.body)
			param = getDecryptedValue(data['data'] , BKEY)
			r = json.loads(param)
			sender_customer_mobile   = r['sender_mobile'].strip()
			receiver_customer_mobile = r['receiver_mobile'].strip()
			sender_customer_type     = r['sender_customer_type'].strip()
			receiver_customer_type   = r['receiver_customer_type'].strip()
			deposit_amount           = r['deposit_amount'].strip()
			deposit_reference        = r['deposit_reference'].strip()

			if(not sender_customer_mobile ):
				return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
			elif(not receiver_customer_mobile):
				return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
			elif(not sender_customer_type):
				return JsonResponse({'error' : 'Sender Customer Type not Empty'})
			elif(not receiver_customer_type):
				return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
			elif(not deposit_amount):
				return JsonResponse({'error' : 'Deposit Amount not Empty'})
			elif(decimal.Decimal(deposit_amount)==0):
				return JsonResponse({'error':'Deposit Amount could not be 0'})
			else:		
				sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
				receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )
				if(sender_info != 0):
					if(receiver_info!=0):	
						cas = CustomerAccounting.objects.filter(Q(userid__exact = sender_info)).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)

						ras = CustomerAccounting.objects.filter(Q(userid__exact = receiver_info)).select_related()
						receiver_balance = decimal.Decimal(ras[0].customer_balance)

						blockCounter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=receiver_info) & Q(topup_transaction_status__exact='pending')).select_related()	
						if (blockCounter.count() > 0):
							block_amount = blockCounter[0].topup_transaction_amount
						else:
							block_amount = 0



					
						drawer_balance = decimal.Decimal(receiver_balance) - decimal.Decimal(block_amount)
						print("sender:%s receiver:%s mainbal:%s rbal:%s block:%s"% (sender_info,receiver_info,drawer_balance , receiver_balance, block_amount))
						if(decimal.Decimal(deposit_amount) > decimal.Decimal(drawer_balance) ):					
							return JsonResponse({'error':'Insufficient Balance.'  })
						else:

							sbal = decimal.Decimal(sender_balance)
							rbal = decimal.Decimal(drawer_balance)- decimal.Decimal(deposit_amount)
							service_fee = 0.00

							ts = time.gmtime()
							currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)					
							trx = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))


							#================= topup transaction entry ==================
							ct = CustomerTopUpTransaction()
							ct.topup_transaction_id = trx
							ct.topup_sender_userid   = sender_info
							ct.topup_receiver_userid = receiver_info
							ct.topup_reference_txt = deposit_reference
							ct.topup_transaction_amount = decimal.Decimal(deposit_amount)
							ct.topup_transaction_type = '10003'
							ct.topup_transaction_message = 'TRXID:'+trx+' Deposit Request Send.'
							ct.topup_cashbox_business_id =   ''
							ct.topup_auxilary_userid = ''  
							ct.topup_referral_id =  ''
							ct.topup_transaction_status = 'pending'
							ct.topup_transaction_initiator_request_timestamp =   currentTime   #datetime.datetime.now() 	
							ct.topup_transaction_completrion_request_timestamp = currentTime   #datetime.datetime.now()
							ct.save()			           


							#================ notification ==================================
							sender_cust_detail = customerInfo(sender_info)
							receiver_cust_detail = customerInfo(receiver_info)
							if(sender_cust_detail == 0):
								sender_num = ''	
								sender_photo = ''
							else:
								sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')


							if(receiver_cust_detail == 0):
								receiver_num = ''
								receiver_photo = ''
							else:
								receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')




							#==============================notification for initiator merchant=================			
							if(sender_customer_type == '102'):
								msg_title = "Deposit Request"
								msg = str(deposit_amount) +"TK Deposit request To:"+ receiver_num 
								drawer_fcm_token = receiver_fcm_token
								sender_number = sender_num
								receiver_number = receiver_num
								photo = sender_photo
								transaction_type = "deposit"
								log_view_type = "personal"
								req_type = "trn"											
								print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
								senderDet = customerNameDetailInfo(sender_info)
								sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
								cashboxNotification(sender_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, deposit_amount, photo, service_fee,transaction_type, req_type,log_view_type,currentTime)








							if(receiver_customer_type == '102'):
							#===============receviver merchant ===================
								msg_title = "Deposit Request"
								msg = str(deposit_amount) +"TK Deposit request From:"+ sender_num
								drawer_fcm_token = receiver_fcm_token
								sender_number = sender_num
								receiver_number = receiver_num
								photo = sender_photo
								transaction_type = "deposit"
								log_view_type = "drawer"
								req_type = "req"
								print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
								senderDet = customerNameDetailInfo(sender_info)
								sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
								cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, deposit_amount, photo, service_fee,transaction_type, req_type,log_view_type,currentTime)
							else:
								#===============receiver pocket ======================
								receiverDet = customerNameDetailInfo(receiver_info)
								receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
								fcm_title = str(pay_amount) +"TK Payment From:"+ sender_num
								sendTransactionNotification(1,receiver_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(deposit_amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )



							return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee)),'msg':'TRXID:'+str(trx)+'.Deposit Amount:'+ str(decimal.Decimal(deposit_amount))+'TK.Current Balance:'+ str(decimal.Decimal(drawer_balance))+'TK.Service Fee:'+ str(decimal.Decimal(service_fee))+'TK.'  })
					else:
						return JsonResponse({'error':'Receiver Mobile Number Not Found'})
				else:
					return JsonResponse({'error':'Sender Mobile Number Not Found'})

		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})	
	else:
		return JsonResponse({'error':'Please check your request method'})








#========================================drawer payment ============================================
@csrf_exempt
def drawerPayment(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21905F') ).select_related()
		broken_key = hooker[0].broken_key


		try:

			data = json.loads(request.body)
			param = getDecryptedValue(data['data'] , broken_key )
			r = json.loads(param)
			sender_customer_mobile   = r['sender_mobile'].strip()
			receiver_customer_mobile = r['receiver_mobile'].strip()
			sender_customer_type     = r['sender_customer_type'].strip()
			receiver_customer_type   = r['receiver_customer_type'].strip()
			pay_amount               = r['pay_amount'].strip()
			transaction_pass         = r['transaction_pass'].strip()
			payment_reference        = r['payment_reference'].strip()

			if ( not sender_customer_mobile ):
				return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
			elif ( not receiver_customer_mobile ):
				return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
			elif ( not sender_customer_type ):
				return JsonResponse({'error' : 'Sender Customer Type not Empty'})
			elif ( not receiver_customer_type ):
				return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
			elif ( not pay_amount ):
				return JsonResponse({'error' : 'Pay Amount not Empty'})
			elif ( not transaction_pass ):
				return JsonResponse({'error' : 'Transaction Amount not Empty'})
			else:
				sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
				receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )		

				cashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact = sender_info) & Q(cashbox_transaction_pass__exact=dbEncrypt(transaction_pass))).select_related()



				counterPocket = Pocket.objects.filter(Q(customer_id__exact = receiver_info )).select_related()
				if(sender_info == 0):
					return JsonResponse({'error':'Sender Mobile Number Not Found'})
				elif(cashboxCount.count() == 0 ):
					return JsonResponse({'error':'Wrong Transaction Password'})
				elif(counterPocket.count() > 0 ):
					return JsonResponse({'error':'Drawer Cannot Payment to Pocket'})		
				else:			
					#service fee add
					if(decimal.Decimal(pay_amount) <= 50):
						service_fee = 0
					elif(decimal.Decimal(pay_amount)> 50 and decimal.Decimal(pay_amount) <= 1000):
						service_fee = 2
					else:
						service_fee = 5





				cas = CustomerAccounting.objects.filter(Q(userid__exact = sender_info )).select_related()
				sender_amount = decimal.Decimal(pay_amount)+decimal.Decimal(service_fee)
				blockCounter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=sender_info) & Q(topup_transaction_status__exact='pending')).select_related()	



				print("sender Amount: %s"% sender_amount)
				if(blockCounter.count() > 0 ):
					block_amount = blockCounter[0].topup_transaction_amount
				else:
					block_amount = 0


				total = decimal.Decimal(cas[0].customer_balance) - decimal.Decimal(block_amount)
				if(decimal.Decimal(sender_amount) > decimal.Decimal(total) ):
					return JsonResponse({'error':'Insufficient Balance.Current Balance:'+ str(decimal.Decimal(total))   })
				else:
					ras = CustomerAccounting.objects.filter(Q(userid__exact=sender_info)).select_related()
					receiver_balance = decimal.Decimal(ras[0].customer_balance)

					sender_current_balance = decimal.Decimal(total) -  decimal.Decimal(sender_amount)
					receiver_current_balance = decimal.Decimal(receiver_balance) + decimal.Decimal(sender_amount)

					trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))				
				

					#============================== transaction entry ========================================
					cts = CustomerTransaction()
					cts.customer_transaction_id = trx
					cts.sender_userid = sender_info
					cts.receiver_userid = receiver_info
					cts.reference_txt = payment_reference
					cts.transaction_amount = decimal.Decimal(pay_amount)
					cts.transaction_type = '10002'
					cts.transaction_message = 'TRXID: '+ str(trx) + ' Pay amount:'+ str(decimal.Decimal(pay_amount)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
					cts.cashbox_business_id = ''
					cts.auxilary_userid = ''
					cts.referral_id = ''
					cts.transaction_status = ''
					cts.sender_longitute = ''
					cts.sender_latitude = ''
					cts.receiver_longitute = ''
					cts.receiver_latitude = ''
					cts.transaction_timestamp = datetime.datetime.now()
					cts.save()				


					#========================= accounting update ===============
					CustomerAccounting.objects.filter( Q(userid__exact = sender_info)).update( customer_balance = sender_current_balance )
					CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)).update( customer_balance = receiver_current_balance )




					#=========================notification =====================
					ts = time.gmtime()
					currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)

					sender_cust_detail = customerInfo(sender_info)
					receiver_cust_detail = customerInfo(receiver_info)
					if(sender_cust_detail == 0):
						sender_num = ''
						sender_photo = ''
					else:
						sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')


					if(receiver_cust_detail == 0):
						receiver_num = ''
						receiver_photo = ''
					else:
						receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')





					#============== for sender notification =============================
					msg_title = "Payment Successfully"
					msg = str(pay_amount) +"TK Payment To:"+ receiver_num
					drawer_fcm_token = sender_fcm_token
					sender_number = sender_num
					receiver_number = receiver_num
					photo = sender_photo
					transaction_type = "payment"
					log_view_type = "personal"
					req_type = "trn"											
					print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
					senderDet = customerNameDetailInfo( sender_info )
					sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
					cashboxNotification(sender_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, str(pay_amount) , photo, str(service_fee),transaction_type, req_type,log_view_type,currentTime)




					if(receiver_customer_type =='102'):
						#===================for drawer===============
						msg_title = "Payment Successfully"
						msg = str(pay_amount) +"TK Payment From:"+ sender_num
						drawer_fcm_token = sender_fcm_token
						sender_number = sender_num
						receiver_number = receiver_num
						photo = sender_photo
						transaction_type = "payment"
						log_view_type = "drawer"
						req_type = "trn"
						print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
						senderDet = customerNameDetailInfo( receiver_info )
						sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
						cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, str(pay_amount) , photo, str(service_fee),transaction_type, req_type,log_view_type,currentTime)



					else:
						#===========for pocket =========================
						receiverDet = customerNameDetailInfo(receiver_info)
						receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
						fcm_title = str(pay_amount) +"TK Payment From:"+ sender_num
						sendTransactionNotification(1,receiver_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(pay_amount))+"TK" ,str(service_fee)+"TK","payment", currentTime  )



				


					return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee)),'msg':'TRXID:'+str(trx)+'.Pay Amount:'+ str(decimal.Decimal(pay_amount))+'TK.Current Balance:'+ str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+ str(decimal.Decimal(service_fee))+'TK.'  })

		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})
	else:
		return JsonResponse({'error':'Please check your request method'})





#====================================drawer outgoing withdraw or cashout =========================================
@csrf_exempt
def drawerOutgoingWithdrawLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10004'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')


			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()





			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')

					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)





				return JsonResponse({'success':'true','data': sending_data  })
			else:
				return JsonResponse({'error':'Not Payment Data Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})




#================================drawer incoming withdraw or cashout ================================
@csrf_exempt
def drawerIncomingWithdrawLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10004'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')

			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()



			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')

					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)





				return JsonResponse({'success':'true','data': sending_data  })			
			else:
				return JsonResponse({'error':'Not Payment Data Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})




#================================drawer outgoing deposit or topup =================================================
@csrf_exempt
def drawerOutgoingDepositLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']		

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10003'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')


			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()



			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')


					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	
					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)

				return JsonResponse({'success':'true','data': sending_data  })			
			else:
				return JsonResponse({'error':'No Deposit Data Found'})


	else:
		return JsonResponse({'error':'Please check your request method'})



#=============================================drawer incoming deposit or topup ==================================================
@csrf_exempt
def drawerIncomingDepositLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']



		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10003'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')

			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()





			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')


					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)




				return JsonResponse({'success':'true','data': sending_data  })			
			else:
				return JsonResponse({'error':'No Deposit Data Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})







#==========================drawer outgoing payment log============================================
@csrf_exempt
def drawerOutgoingPayLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10002'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')

			if(is_custom == 'no'):
				if(search_type=='*'):
				#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()


			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')
	
					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)






				return JsonResponse({'success':'true','data': sending_data  })
			else:
				return JsonResponse({'error':'No Payment Data Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})






#=====================================drawer incoming payment log =======================================
@csrf_exempt
def drawerIncomingPayLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10002'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')

			if(is_custom == 'no'):

				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()

			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()

			
			





			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')


					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	


					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)






				return JsonResponse({'success':'true','data': sending_data  })			
			else:
				return JsonResponse({'error':'No Payment Data Found'})
	else:
		return JsonResponse({'error':'Please check your request method'})




#======================= Pocket deposit or topup log  ==========================================
@csrf_exempt
def topupLogData(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']	

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10003'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')


			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)

					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()




			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')


					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)



				return JsonResponse({'success':'true','data':sending_data  }) 
			else:
				return JsonResponse({'error':'No Cashout Data Found'})
	else:
		return JsonResponse({'error':'Please check your request method'})




#========================== Drawer Business Information =====================================================
@csrf_exempt
def drawerBusinessInfoUpdate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		device_id = r['device_id']
		drawer_type = r['drawer_type']
		business_category = r['business_category']
		business_trade_license = r['business_trade_license']
		business_tin_number = r['business_tin_number']
		business_payment_limit = r['business_payment_limit']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID one cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not drawer_type):
			return JsonResponse({'error':'Drawer Type cannot be empty'})
		else:
			counter = Business.objects.filter(Q(app_mobile_number__exact=mobile_number) & Q(business_type__exact=drawer_type )).select_related()			
			if(counter.count() > 0):
				businessID = counter[0].business_id
				if(business_category is not None and business_category != ''):
					Business.objects.filter(Q(business_id__exact= businessID)).update(business_category = business_category)
				if(business_payment_limit is not None and business_payment_limit != ''):
					Business.objects.filter(Q(business_id__exact = businessID)).update(business_approx_monthly_transaction_amount = business_payment_limit)
				if(business_trade_license is not None and business_trade_license != ''):
					Business.objects.filter(Q(business_id__exact = businessID)).update(business_tradelicense = business_trade_license)
				if(business_tin_number is not None and business_tin_number != ''):
					Business.objects.filter(Q(business_id__exact = businessID)).update(business_tin = business_tin_number)



				
			
				return JsonResponse({'success':'true','msg':'Business Information Updated'})
			else:
				return JsonResponse({'error':'Business Information Not Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})






#================================== Drawer Business Profile ============================================
@csrf_exempt
def drawerBusinessProfileCreate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		device_id = r['device_id']
		business_name = r['business_name']
		business_mobileno = r['business_mobileno']
		business_email = r['business_email']
		business_address = r['business_address']
		business_division = r['business_division']
		business_district = r['business_district']
		business_thana = r['business_thana']
		business_postcode = r['business_postcode']
		drawer_type = r['drawer_type']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty' })
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID one cannot be empty' })
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty' })
		elif(not business_name):
			return JsonResponse({'error':'Business Name cannot be empty' })
		elif(not drawer_type):
			return JsonResponse({'error':'Drawer Type cannot be empty' })
		else:
			counter = Business.objects.filter(Q(app_mobile_number__exact=mobile_number) & Q(business_type__exact=drawer_type )).select_related()

			if(counter.count() > 0):
				#business info
				businessID = counter[0].business_id
				addressID = counter[0].business_address_id
				print("%s %s"% (businessID , addressID ))
				if(business_name is not None and business_name != ''):
					Business.objects.filter(Q(business_id__exact=businessID)).update(business_name = business_name)
				if(business_mobileno is not None and business_mobileno != ''):
					Business.objects.filter(Q(business_id__exact=businessID)).update(business_mobileno = business_mobileno)
				if(business_email is not None and business_email != ''):
					Business.objects.filter(Q(business_id__exact=businessID)).update(business_email = business_email)


						
				if(addressID == None):
					#drawer business address
					d = DrawerAddress()
					d.reg_address = business_address
					d.division = business_division
					d.district = business_district
					d.thana = business_thana
					d.postcode = business_postcode
					d.longitude = ''
					d.latitude = ''
					d.save()
					Business.objects.filter(Q(business_id__exact=businessID)).update(business_address_id=d.address_id)
				else:
					if(business_address is not None and business_address != ''):
						DrawerAddress.objects.filter(Q(address_id__exact= addressID)).update(reg_address = business_address)
					if(business_division is not None and business_division != ''):
						DrawerAddress.objects.filter(Q(address_id__exact= addressID)).update(division = business_division)
					if(business_district is not None and business_district != ''):
						DrawerAddress.objects.filter(Q(address_id__exact= addressID)).update(district = business_district)
					if(business_thana is not None and business_thana != ''):
						DrawerAddress.objects.filter(Q(address_id__exact= addressID)).update(thana = business_thana)
					if(business_postcode is not None and business_postcode != ''):
						DrawerAddress.objects.filter(Q(address_id__exact= addressID)).update(postcode = business_postcode)


				

				return JsonResponse({'success':'true', 'msg':'Business Profile Updated'})
			else:
				return JsonResponse({'error':'Business Account Already exist'})

	else:
		return JsonResponse({'error':'Please check your request method'})




#=================================== Drawer Current Balance ==================================================
@csrf_exempt
def drawerCurrentBalance(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']
		deviceid  = r['deviceid']
		simid_one = r['simid_one']      
		simid_two = r['simid_two']
		
		if( not mobileno):
			return JsonResponse({'error': 'Mobile Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Tyepe Cannot be Empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid Cannot be Empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SimID One Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			con = psycopg2.connect(
				host = str(HOSTNAME) ,
				database = str(DATABASE),
				user = str(USERNAME),
				password= str(PASSWORD),
				port = str(PORTNO)
			)
			cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
			strs = "select get_drawer_current_balance('"+ str(user_id)  +"') as actual_amount;"
			print(strs)
			rw = cur.execute(strs)
			one = cur.fetchone()
			actual_balance =  one['actual_amount']
			cur.close()
			con.close()



			#user_det = CustomerAccounting.objects.filter( Q(userid__exact = user_id) ).select_related()
			#customer_balance = decimal.Decimal( user_det[0].customer_balance )		
			#counter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=user_id) & Q(topup_transaction_status__exact='pending')).select_related()	
			#if(counter.count() > 0 ):
			#	block_amount = counter[0].topup_transaction_amount
			#else:
			#	block_amount = 0
			#print("%s bal:%s block:%s"% (user_id,customer_balance,block_amount))
			#actual_balance = decimal.Decimal(customer_balance) - decimal.Decimal(block_amount)
			#print("%s bal:%s block:%s actbal:%s"% (user_id,customer_balance,block_amount,actual_balance))

			return JsonResponse({'success':'true' , 'current_balance': str(actual_balance)  })
	else:
		return JsonResponse({'error':'Please Check your request'})	






#=========================== Drawer Nominee Information show into Form ====================================
@csrf_exempt
def drawerNomineeInformation(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number = r['mobile_number'].strip()
		sim_id_one = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		device_id = r['device_id'].strip()		

		if(not mobile_number):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID one cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		else:
			counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile_number)).select_related()			
			if(counter.count() > 0):				
				return JsonResponse({'success':'true' ,'first_name': counter[0].cashbox_nominee_fname , 'middle_name': counter[0].cashbox_nominee_mname , 'last_name': counter[0].cashbox_nominee_lname , 'dob': counter[0].cashbox_nominee_dob , 'address': counter[0].cashbox_nominee_address , 'contactno': counter[0].cashbox_nominee_contactno , 'voter_id': counter[0].cashbox_nominee_vid , 'nid': counter[0].cashbox_nominee_nid , 'relation': counter[0].cashbox_nominee_relation , 'voter_or_nid_image': counter[0].cashbox_nominee_vid_image  })
			else:
				return JsonResponse({'error':'Drawer Not Foud'})
	else:
		return JsonResponse({'error':'Please check your request method'})




#=================== Drawer Nominee Information =========================================================
@csrf_exempt
def drawerNomineeInformationUpdate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		drawer_mobileno = r['drawer_mobileno']
		drawer_nominee_fname = r['drawer_nominee_fname']
		drawer_nominee_mname = r['drawer_nominee_mname']
		drawer_nominee_lname = r['drawer_nominee_lname']
		drawer_nominee_dob = r['drawer_nominee_dob']
		drawer_nominee_address = r['drawer_nominee_address']
		drawer_nominee_contactno = r['drawer_nominee_contactno']
		drawer_nominee_relation = r['drawer_nominee_relation']
		drawer_nominee_nid_voter = r['drawer_nominee_nid_voter']
		drawer_nominee_nid_or_voter = r['drawer_nominee_nid_or_voter']

		if(not drawer_mobileno):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		else:
			counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=drawer_mobileno)).select_related()
			if(counter.count() > 0):

				cashboxID = counter[0].cashbox_customer_id
			
				if(drawer_nominee_nid_or_voter == 'VID'):
					vid_data = drawer_nominee_nid_voter
					nid_data =''
				elif(drawer_nominee_nid_or_voter == 'NID'):
					vid_data = ''
					nid_data = drawer_nominee_nid_voter
				else:
					vid_data = ''
					nid_data = ''




				if(drawer_nominee_fname is not None and drawer_nominee_fname != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_fname=drawer_nominee_fname)
				if(drawer_nominee_mname is not None and drawer_nominee_mname != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_mname= drawer_nominee_mname)
				if(drawer_nominee_lname is not None and drawer_nominee_lname != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_lname =drawer_nominee_lname)
				if(drawer_nominee_address is not None and drawer_nominee_address != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_address= drawer_nominee_address)
				if(drawer_nominee_contactno is not None and drawer_nominee_contactno != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_contactno=drawer_nominee_contactno)
				if(drawer_nominee_relation is not None and drawer_nominee_relation != ''):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_relation=drawer_nominee_relation)
				if(drawer_nominee_nid_or_voter is not None and drawer_nominee_nid_or_voter=='NID'):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_nid = nid_data)
				if(drawer_nominee_nid_or_voter is not None and drawer_nominee_nid_or_voter=='VID'):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_vid=vid_data)
				if(drawer_nominee_dob is not None and drawer_nominee_dob !='1111-11-11'):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashboxID)).update(cashbox_nominee_dob=drawer_nominee_dob)
					
	


				return JsonResponse({'success':'true' , 'msg':'Drawer Nominee Profile Updated' })
			else:
				return JsonResponse({'error':'Drawer Not Found'})


			#if(drawer_nominee_fname is not None and drawer_nominee_fname != ''):
		
	else:
		return JsonResponse({'error':'Please your request method'})	





#============================Drawer Withdraw Reject or Refund =================================================
@csrf_exempt
def withdrawRejectOrRefund(request):
	if request.method == 'POST':

		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21805F') ).select_related()
		broken_key = hooker[0].broken_key


		try:

			data = json.loads(request.body)
			param = getDecryptedValue(data['data'] , broken_key )
			r = json.loads(param)
			mobile_number = r['mobile_number']
			user_type = r['user_type']
			device_id = r['device_id']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			trxid = r['trxid']
			req_type = r['req_type']
			transaction_pin = r['transaction_pin']

			if(not mobile_number):
				return JsonResponse({'error':'Mobile number cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'User Type cannot be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device ID cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'SIM ID one cannot be empty'})
			elif(not trxid):
				return JsonResponse({'error':'Transaction ID cannot be empty'})
			elif(not req_type):
				return JsonResponse({'error':'Request Type cannot be empty'})
			else:
				counter = CustomerTransaction.objects.filter(Q(customer_transaction_id__exact=trxid)).select_related()			
				if(counter.count()> 0 ):
					amount  = counter[0].transaction_amount
					sender_id = counter[0].sender_userid
					receiver_id = counter[0].receiver_userid
				
					ts = time.gmtime()
					currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)

					service_fee = ((decimal.Decimal(amount)*1)/100)
					total_amount = (decimal.Decimal(amount)+decimal.Decimal(service_fee))

					sDet = CustomerAccounting.objects.filter(Q(userid__exact=sender_id)).select_related()
					sBal = sDet[0].customer_balance
					rDet = CustomerAccounting.objects.filter(Q(userid__exact=receiver_id)).select_related()
					rBal = rDet[0].customer_balance
					cashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact = receiver_id) & Q(cashbox_transaction_pass__exact=dbEncrypt(transaction_pin))).select_related()

					print("%s %s"%(receiver_id , dbEncrypt(transaction_pin)))
					if(cashboxCount.count() == 0):
						return JsonResponse({ 'error' : 'Wrong Transaction Password' })
					elif(decimal.Decimal(rBal) > decimal.Decimal(total_amount)):
						rupdate_balance = decimal.Decimal(rBal) - decimal.Decimal(total_amount)
						supdate_balance = decimal.Decimal(sBal) + decimal.Decimal(total_amount)

						CustomerAccounting.objects.filter(Q(userid__exact=sender_id)).update(customer_balance=decimal.Decimal(supdate_balance) )
						CustomerAccounting.objects.filter(Q(userid__exact=receiver_id)).update(customer_balance=decimal.Decimal(rupdate_balance) )
						CustomerTransaction.objects.filter(Q(customer_transaction_id__exact=trxid)).delete()



						#notification
						sender_cust_detail = customerInfo(sender_id)
						receiver_cust_detail = customerInfo(receiver_id)
						if(sender_cust_detail == 0):
							sender_num = ''
							sender_photo = ''
						else:
							sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')



						if(receiver_cust_detail == 0):
							receiver_num = ''
							receiver_photo = ''
						else:
							receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')




						senderCashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact=receiver_id)).select_related()
						if(senderCashboxCount.count() > 0):
						
							#notification for merchant
							msg_title = "Withdraw Refund"
							msg = str(amount) +"TK and Service Fee:"+str(service_fee)+" Withdraw Refund to:"+ sender_num 
							drawer_fcm_token = receiver_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "withdraw"
							log_view_type = "drawer"
							req_type = "trn"											
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo( sender_id)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							#cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, str(amount) , photo, str(service_fee),transaction_type, req_type,log_view_type,currentTime)

							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotificationForDrawer(receiver_fcm_token , msg_title , msg , "sys" , msg_date,  custom_date, msg_time , "01300000000" , "" )








						#customer as drawer
						receiverCashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact=sender_id)).select_related()
						if(receiverCashboxCount.count() > 0):
						
							#notification for merchant
							msg_title = "Withdraw Refund"
							msg = str(amount) +"TK and Service Fee:"+str(service_fee)+"TK Withdraw Refund from:"+ receiver_num
							drawer_fcm_token = sender_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "withdraw"
							log_view_type = "personal"
							req_type = "trn"
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo( receiver_id)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							#cashboxNotification(sender_fcm_token , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, str(amount) , photo, str(service_fee),transaction_type, req_type,log_view_type,currentTime)


							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotificationForDrawer(sender_fcm_token , msg_title , msg , "sys" , msg_date,  custom_date, msg_time , "01300000000" , "" )						


						else:
							#notification for customer
							receiverDet = customerNameDetailInfo(receiver_id)
							receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
							fcm_title = str(amount) +"TK and Service Fee:"+str(service_fee) +"TK Withdraw Refund from:"+ receiver_num
							#sendTransactionNotification(1,sender_fcm_token_id,fcm_title,trxid ,sender_mobile,receiver_mobile,str(decimal.Decimal(amount))+"TK" ,str(service_fee)+"TK","withdraw", currentTime  )

							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotification(sender_fcm_token_id , "Withdraw Refund" , fcm_title , "sys" , msg_date ,custom_date , msg_time ,"01300000000" , "" )



						return JsonResponse({'success':'true','msg':'Withdraw amount:'+ str(decimal.Decimal(total_amount))+'TK Refunded to:'+sender_num   })
					else:
						return JsonResponse({'error':'Insufficient Balance.Current Balance:'+ str(decimal.Decimal(rBal))+'TK'  })


				else:
					return JsonResponse({'error':'Transaction not found'})


		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})

	else:
		return JsonResponse({'error':'Please check your request method'})





#============================== Cashbox Deposit accept or reject ===========================================
@csrf_exempt
def cashboxDepositAccept(request):
	if request.method == 'POST':

		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21805F') ).select_related()
		broken_key = hooker[0].broken_key

		try:

			data = json.loads(request.body)
			param = getDecryptedValue(data['data'] , broken_key )
			r = json.loads(param)
			mobile_number = r['mobile_number']
			user_type = r['user_type']
			device_id = r['device_id']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			trxid = r['trxid']
			flag = r['req_type']
			tran_pin = r['transaction_pin']

			ts = time.gmtime()
			currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)


			if(not mobile_number):
				return JsonResponse({'error':'Mobile number cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'User Type Cannot  be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device ID Cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'SIM ID cannot be empty'})
			elif(not trxid):
				return JsonResponse({'error':'Transaction ID cannot be empty'})
			elif(not flag):
				return JsonResponse({'error':'Request Type cannot be empty'})
			else:

				counter = CustomerTopUpTransaction.objects.filter(Q(topup_transaction_id__exact = trxid)).select_related()
				if(counter.count() > 0):
					amount = counter[0].topup_transaction_amount
					if(flag == 'accept'):
						#for accept
			
						rbalDet = CustomerAccounting.objects.filter( Q(userid__exact = counter[0].topup_receiver_userid )).select_related()
						senderID = counter[0].topup_receiver_userid
						cashboxCount = Cashbox.objects.filter(Q(cashbox_customer_id__exact = senderID ) & Q(cashbox_transaction_pass__exact=dbEncrypt(tran_pin))).select_related()
					
						if(cashboxCount.count() == 0):
							return JsonResponse({'error':'Wrong Transaction Password'})
						elif(decimal.Decimal(rbalDet[0].customer_balance) > decimal.Decimal(amount) ):
							CustomerTopUpTransaction.objects.filter(Q(topup_transaction_id__exact = trxid)).update(topup_transaction_status = 'accept',topup_transaction_completrion_request_timestamp = currentTime)


							cts = CustomerTransaction()
							cts.customer_transaction_id = trxid
							cts.sender_userid = counter[0].topup_sender_userid
							cts.receiver_userid = counter[0].topup_receiver_userid
							cts.reference_txt = counter[0].topup_reference_txt
							cts.transaction_amount = decimal.Decimal(amount)
							cts.transaction_type = '10003'
							cts.transaction_message = counter[0].topup_transaction_message
							cts.cashbox_business_id = ''
							cts.auxilary_userid = ''
							cts.referral_id = ''
							cts.transaction_status = 'paid'
							cts.sender_longitute = ''
							cts.sender_latitude = ''
							cts.receiver_longitute = ''
							cts.receiver_latitude = ''
							cts.transaction_timestamp = currentTime
							cts.save()


							
							#sender accounting
							sbalDet = CustomerAccounting.objects.filter( Q(userid__exact = counter[0].topup_sender_userid )).select_related()
							sender_bal = decimal.Decimal(sbalDet[0].customer_balance) + decimal.Decimal(amount)
							CustomerAccounting.objects.filter( Q(userid__exact = counter[0].topup_sender_userid )).update(customer_balance = decimal.Decimal(sender_bal) )					
			
							#receiving accounting
							receiver_bal = decimal.Decimal(rbalDet[0].customer_balance) - decimal.Decimal(amount)				

							CustomerAccounting.objects.filter( Q(userid__exact = counter[0].topup_receiver_userid )).update(customer_balance = decimal.Decimal(receiver_bal))
							CustomerTopUpTransaction.objects.filter(Q(topup_transaction_id__exact = trxid)).update(topup_transaction_status='accept')


							#notification
							sender_cust_detail = customerInfo(counter[0].topup_sender_userid)
							receiver_cust_detail = customerInfo(counter[0].topup_receiver_userid)
							if(sender_cust_detail == 0):
								sender_num = ''
								sender_photo = ''
							else:
								sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')



							if(receiver_cust_detail == 0):
								receiver_num = ''
								receiver_photo = ''
							else:
								receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')





							senderCounter = Cashbox.objects.filter(Q(cashbox_customer_id__exact=counter[0].topup_sender_userid)).select_related()
					
							service_fee = "0.00"
							if(senderCounter.count() > 0):
								#notification for merchant

								msg_title = "Deposit Request Accepted"
								msg = str(amount) +"TK Deposit request Acceped by :"+ receiver_num 
								drawer_fcm_token = receiver_fcm_token
								sender_number = sender_num
								receiver_number = receiver_num
								photo = sender_photo
								transaction_type = "deposit"
								log_view_type = "personal"
								req_type = "trn"											
								print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
								senderDet = customerNameDetailInfo(counter[0].topup_sender_userid)
								sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
								cashboxNotification(sender_fcm_token , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, str(amount) , photo, service_fee,transaction_type, req_type,log_view_type,currentTime)
							else:
								#notification for customer
								receiverDet = customerNameDetailInfo(counter[0].topup_sender_userid)
								receiver_full_name,receiver_photo,receiver_mobile,sender_fcm_token =receiverDet.split('|')
								fcm_title = "Deposit Request Accepted."

								sendTransactionNotification(1,sender_fcm_token,fcm_title,trxid ,sender_num,receiver_num,str(decimal.Decimal(amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )
				






							cashbox_counter = Cashbox.objects.filter(Q(cashbox_customer_id__exact=counter[0].topup_receiver_userid)).select_related()
						
							if(cashbox_counter.count() > 0):
								print("i am executing")
								msg_title = "Deposit Request Accepted"
								msg = str(amount) +"TK Deposit request to:"+ receiver_num
								drawer_fcm_token = sender_fcm_token
								sender_number = sender_num
								receiver_number = receiver_num
								photo = sender_photo
								transaction_type = "deposit"
								log_view_type = "drawer"
								req_type = "trn"
								print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
								senderDet = customerNameDetailInfo(counter[0].topup_receiver_userid)
								sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
								cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, str(amount) , photo, service_fee,transaction_type, req_type,log_view_type,currentTime)

		

							else:
								#notification for customer
								receiverDet = customerNameDetailInfo(counter[0].topup_receiver_userid)
								receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
								fcm_title = "Deposit Request Accepted."

								sendTransactionNotification(1,receiver_fcm_token,fcm_title,trxid ,sender_mobile,receiver_mobile,str(decimal.Decimal(amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )





								return JsonResponse({'success':'true','msg':'Drawer Accept Deposit successfully' })
						else:
							return JsonResponse({'error':'Insufficient Amount'})
					elif(flag == 'reject'):
						#for reject
						rbalDet = CustomerAccounting.objects.filter( Q(userid__exact = counter[0].topup_receiver_userid )).select_related()
						CustomerTopUpTransaction.objects.filter(Q(topup_transaction_id__exact = trxid)).update(topup_transaction_status = 'reject',topup_transaction_completrion_request_timestamp = currentTime)			



						amount = counter[0].topup_transaction_amount

						#notification
						sender_cust_detail = customerInfo(counter[0].topup_sender_userid)
						receiver_cust_detail = customerInfo(counter[0].topup_receiver_userid)
						if(sender_cust_detail == 0):
							sender_num = ''
							sender_photo = ''
						else:
							sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')



						if(receiver_cust_detail == 0):
							receiver_num = ''
							receiver_photo = ''
						else:
							receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')



						service_fee = "0.00"
						cashbox_sender_counter = Cashbox.objects.filter(Q(cashbox_customer_id__exact=counter[0].topup_sender_userid)).select_related()
						if(cashbox_sender_counter.count() > 0 ):
							sender_type_id = cashbox_sender_counter[0].customer_type_id
						else:
							pcounter = Pocket.objects.filter(Q(customer_id__exact=counter[0].topup_sender_userid)).select_related()
							sender_type_id = pcounter[0].pocket_customer_type




						print("sender type id:%s"%(sender_type_id))
						if(sender_type_id == 102):
							print("sender notification block")
							service_fee = "0.00"
							#notification for merchant
							msg_title = "Deposit Request Cancelled"
							msg = str(amount) +"TK Deposit request Cancelled from:"+ receiver_num 
							drawer_fcm_token = receiver_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "deposit"
							log_view_type = "personal"
							req_type = "trn"											
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo(counter[0].topup_sender_userid)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							#cashboxNotification(sender_fcm_token_id , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, amount, photo, service_fee,transaction_type, req_type,log_view_type,currentTime)

							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotificationForDrawer(sender_fcm_token_id , msg_title , msg , "sys" , msg_date,  custom_date, msg_time , "01300000000" , "" )

						else:
							senderDet = customerNameDetailInfo(counter[0].topup_sender_userid)
							receiver_full_name,receiver_photo,receiver_mobile,sender_fcm_token_id =senderDet.split('|')
							fcm_title = "Deposit Request Cancelled."
							#sendTransactionNotification(1,sender_fcm_token_id,fcm_title,trxid ,sender_num,receiver_num,str(decimal.Decimal(amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )	

							msg = str(amount) +"TK Deposit request Cancelled from:"+ receiver_num
							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotification(sender_fcm_token_id , "Deposit Request Cancelled" , msg , "sys" , msg_date ,custom_date , msg_time ,"01300000000" , "" )







						cashbox_counter = Cashbox.objects.filter(Q(cashbox_customer_id__exact=counter[0].topup_receiver_userid)).select_related()
						if(cashbox_counter[0].customer_type_id == 102):
							print("i am executing reject")
							msg_title = "Deposit Request Cancelled"
							msg = str(amount) +"TK Deposit request cancelled to:"+ receiver_num
							drawer_fcm_token = sender_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "deposit"
							log_view_type = "drawer"
							req_type = "trn"
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo(counter[0].topup_receiver_userid)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							#cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trxid , sender_number, receiver_number, str(amount) , photo, service_fee,transaction_type, req_type,log_view_type,currentTime)


							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotificationForDrawer(receiver_fcm_token , msg_title , msg , "sys" , msg_date,  custom_date, msg_time , "01300000000" , "" )						

						else:			
							#notification for customer
							receiverDet = customerNameDetailInfo(counter[0].topup_receiver_userid)
							receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
							fcm_title = "Deposit Request Cancelled."
							msg = str(amount) +"TK Deposit request cancelled to:"+ receiver_num
							#sendTransactionNotification(1,receiver_fcm_token,fcm_title,trxid ,sender_mobile,receiver_mobile,str(decimal.Decimal(amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )

							msg_date = date.today().strftime('%A, %d %B %Y')
							custom_date = date.today().strftime('%Y-%m-%d')
							msg_time = time.strftime('%I:%M %p')
							sendCommonNotification(receiver_fcm_token , "Deposit Request Cancelled" , msg , "sys" , msg_date ,custom_date , msg_time ,"01300000000" , "" )



						return JsonResponse({'success':'true' , 'msg':'Drawer Deposit Cancelled successfully'})
					else:
						return JsonResponse({'error':'Request Type Mismatched'})				

				else:
					return JsonResponse({'error':'No Transaction found'})

		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})
	else:
		return JsonResponse({'error':'Please check your request method'})





#===================Cashbox Basic Profile ==============================================
@csrf_exempt
def cashboxBasicProfile(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		cashbox_fname = r['drawer_fname']
		cashbox_mname = r['drawer_mname']
		cashbox_lname = r['drawer_lname']
		cashbox_gender = r['drawer_gender']
		cashbox_email = r['drawer_email']
		cashbox_mobileno = r['drawer_mobileno']	        
		cashbox_alternate_mobileno = r['drawer_alternate_mobileno']
		cashbox_nid_voter = r['drawer_nid_voter']
		cashbox_nid_or_voter = r['drawer_nid_or_voter']

		counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = cashbox_mobileno)  ).select_related()		

		if(not cashbox_mobileno):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		else:
			if(counter.count() > 0 ):
				cashbox_nid = None
				cashbox_voterlD = None 
				if cashbox_nid_or_voter == 'NID':
					cashbox_nid =  cashbox_nid_voter
					cashbox_voterlD = ""
				else:
					cashbox_voterlD = cashbox_nid_voter
					cashbox_nid = ""				


				customer_id = counter[0].cashbox_customer_id
				if(cashbox_fname is not None and cashbox_fname != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_fname = cashbox_fname )
				if(cashbox_mname is not  None and cashbox_mname !=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_mname = cashbox_mname )
				if(cashbox_lname is not None and cashbox_lname!=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_lname = cashbox_lname )
				if(cashbox_gender is not None and cashbox_gender!=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_gender = cashbox_gender )
				if(cashbox_alternate_mobileno is not None and cashbox_alternate_mobileno != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_alternate_mobileno= cashbox_alternate_mobileno)
				if(cashbox_nid is not None and cashbox_nid !=''): 
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nationalD = cashbox_nid )
				if(cashbox_voterlD is not None and cashbox_voterlD !=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_vodetid = cashbox_voterlD )
				if(cashbox_email is not None and cashbox_email !=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_email = cashbox_email )

				


				return JsonResponse({'success':'true','msg':'Profile update complete'})

			else:
				return JsonResponse({'error':'Mobile number not found'})
		
	else:
		return JsonResponse({'error':'Please check your request method'})




#============================= Cashbox Address Profile ==================================================
@csrf_exempt
def cashboxAddressProfile(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		permanent_address = r["permanent_address"]
		permanent_division = r["permanent_division"]
		permanent_district = r["permanent_district"]
		permanent_thana = r["permanent_thana"]
		permanent_postcode = r["permanent_postcode"]

		present_address = r["present_address"]
		present_division = r["present_division"]
		present_district = r["present_district"]
		present_thana  = r["present_thana"]
		present_postcode = r["present_postcode"]

		cashbox_mobileno = r["drawer_mobile_no"]
		drawer_lat = r["drawer_lat"]
		drawer_lon = r["drawer_lon"]


		if(not cashbox_mobileno):
			return JsonResponse({'error':'Mobile number not found'})
		else:
			counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = cashbox_mobileno ) ).select_related()
			customer_id = counter[0].cashbox_customer_id
			per_id = counter[0].cashbox_permanent_address_id
			pre_id = counter[0].cashbox_present_address_id

			
		if(per_id == None):
			pa = PocketAddress()
			pa.reg_address = permanent_address
			pa.division = permanent_division
			pa.thana = permanent_thana
			pa.district = permanent_district
			pa.postcode = permanent_postcode
			pa.longitude = ''
			pa.latitude = ''
			pa.save()

			Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_permanent_address_id = pa.address_id )
		else:
			if(permanent_address is not None and permanent_address !=''):			
				PocketAddress.objects.filter(address_id__exact = per_id).update(reg_address = permanent_address)
			if(permanent_division is not None and permanent_division !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(division = permanent_division)
			if(permanent_thana is not None and permanent_thana !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(thana = permanent_thana)
			if(permanent_district is not None and permanent_district !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(district = permanent_district)
			if(permanent_postcode is not None and permanent_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(postcode = permanent_postcode)



		if(pre_id == None):
			paa = PocketAddress()
			paa.reg_address = present_address
			paa.division = present_division
			paa.thana =  present_thana
			paa.district = present_district
			paa.postcode = present_postcode
			paa.longitude = drawer_lon
			paa.latitude = drawer_lat
			paa.save()

			Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_present_address_id = paa.address_id )      
		else:
			if(present_address is not None and present_address !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(reg_address = present_address)
			if(present_division is not None and present_division !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(division = present_division)
			if(present_thana is not None and present_thana !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(thana = present_thana)
			if(present_district is not None and present_district !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(district = present_district)
			if(present_postcode is not None and present_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(postcode = present_postcode)
			if(drawer_lat is not None and drawer_lat != ''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(latitude=drawer_lat )
			if(drawer_lon is not None and drawer_lon !='' ):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(longitude= drawer_lon)


		return JsonResponse({'success':'true' , 'msg':'Address Profile update' })
	else:
		return JsonResponse({'error':'Please check your request method'})




#=================================== Cashbox Nominee Profile ====================================================
@csrf_exempt
def cashboxNomineeProfile(request):
	if request.method == 'POST':

		cashbox_mobileno = r["cashbox_mobileno"].strip()
		cashbox_nominee_fname = r["cashbox_nominee_fname"].strip()
		cashbox_nominee_mname = r["cashbox_nominee_mname"].strip()
		cashbox_nominee_lname = r["cashbox_nominee_lname"].strip()
		nomineedobs = r["nomineedob"].strip()
		cashbox_nominee_contactno = r["cashbox_nominee_contactno"].strip()
		cashbox_nominee_relation = r["cashbox_nominee_relation"].strip()
		cashbox_nominee_nid_voter = r["cashbox_nominee_nid_voter"].strip()
		cashbox_nominee_nid_or_voter = r["cashbox_nominee_nid_or_voter"].strip()
		cashbox_nominee_address = r['cashbox_nominee_address'].strip()		

		counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = cashbox_mobileno)  ).select_related()

		
		cashbox_nominee_nid = None
		cashbox_nominee_voterlD = None
		if  cashbox_nominee_nid_or_voter == 'NID':
			cashbox_nominee_nid = cashbox_nominee_nid_voter
			cashbox_nominee_voterlD = ""
		else:
			cashbox_nominee_voterlD = cashbox_nominee_nid_voter
			cashbox_nominee_nid = ""


		if (not cashbox_mobileno):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		else:
			if(counter.count() > 0 ):
				customer_id = counter[0].cashbox_customer_id


				if (cashbox_nominee_fname is not None and cashbox_nominee_fname != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_fname = cashbox_nominee_fname )
				if (cashbox_nominee_mname is not None and cashbox_nominee_mname != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_mname = cashbox_nominee_mname )
				if (cashbox_nominee_lname is not None and cashbox_nominee_lname != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_lname = cashbox_nominee_lname )
				if (nomineedobs is not None and nomineedobs != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(nomineedobs = nomineedobs )
				if (cashbox_nominee_relation is not None and cashbox_nominee_relation != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_relation= cashbox_nominee_relation)
				if (cashbox_nominee_nid is not None and cashbox_nominee_nid !=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_nid = cashbox_nominee_nid )
				if (cashbox_nominee_voterlD is not None and cashbox_nominee_voterlD !=''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_voterid = cashbox_nominee_voterlD)
				if (cashbox_nominee_contactno is not None and cashbox_nominee_contactno != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_contactno=cashbox_nominee_contactno)
				if (cashbox_nominee_address is not None and cashbox_nominee_address != ''):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_address=cashbox_nominee_address)



				return JsonResponse({'success':'true', 'msg':'Profile Nominee update'})				

			else:
				return JsonResponse({'error':'Mobile number not found'})
	
	else:
		return JsonResponse({'error':'Please check your request method'})





#===================== Cashbox Common Notification ========================================================
def cashboxCommonNotification(fcm_token,title , msg):
	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")


	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
	result = push_service.notify_single_device(registration_id=fcm_token , message_title=title, message_body=msg)
	return result





#======================= Customer Detail Helper =================================================
def customerInfo(customer_id):
	counter = Pocket.objects.filter(Q(customer_id__exact = customer_id )).select_related()
	if(counter.count() > 0 ):
		return counter[0].pocket_mobileno +'|'+ counter[0].pocket_photo+'|'+counter[0].pocket_fcm_token
	else:
		cnt = Cashbox.objects.filter(Q(cashbox_customer_id__exact = customer_id )).select_related()
		if(cnt.count() > 0):
			return cnt[0].cashbox_mobileno+'|'+cnt[0].cashbox_photo+'|'+cnt[0].cashbox_fcm_token
		else:
			return 0
	




#=======================================cashbox notifactions================================================
#@csrf_exempt
#def cashboxNotification(request):
def cashboxNotification(fcm_token,sender_full_name , msg, msg_title, trxid, sender_num, receiver_num, amount, photo, service_fee, transaction_type, req_type, log_view_type, date_time):

	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")



	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )

	#withdraw , deposit
	data_message = {
		"msg_title": msg_title ,  
		"trxid": trxid ,  
		"name": sender_full_name ,
		"sender": sender_num , 
		"receiver": receiver_num ,  
		"amount": str(amount)+"TK." ,  
		"msg": msg ,
		"photo": photo ,    
		"service_fee": str(service_fee)+"TK." ,  
		"transaction_type": transaction_type ,
		"type": req_type ,  
		"log_view_type": log_view_type,
		"date": date_time  
	}
	print("cashbox fcm calll: %s "% data_message )
	results = push_service.single_device_data_message(registration_id=fcm_token , data_message=data_message)
	#return JsonResponse({'msg':'Sent'})
	print("fcm result: %s"% results)
	return results





#================ cashbox pin verification api ==========================================
@csrf_exempt
def cashboxPinVerification(request):

	if request.method == 'POST':
		data = json.loads(request.body)
		param = getDecryptedValue(data['data'] , BKEY)	
		r = json.loads(param)
		cashbox_mobileno = r['drawer_mobileno']
		cashbox_deviceid = r['drawer_deviceid']
		cashbox_simid_one = r['drawer_simid_one']
		cashbox_simid_two = r['drawer_simid_two']
		cashbox_verification_code = r['drawer_verification_code']

		counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = cashbox_mobileno) & Q(cashbox_deviceid__exact = cashbox_deviceid) & Q(cashbox_simid_one__exact = cashbox_simid_one) & Q(cashbox_pin_verification_code__exact=cashbox_verification_code) ).select_related()
	

		if counter.count() > 0 : 
			customer_id = counter[0].cashbox_customer_id
			Cashbox.objects.filter(Q(cashbox_customer_id__exact = customer_id)).update(cashbox_agree_flag='1',cashbox_pin_verified_flag='1')
			return JsonResponse({'success':'true' , 'msg': 'PIN Verified Successfully.' })
		elif(not cashbox_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not cashbox_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not cashbox_simid_one):
			return JsonResponse({'error': 'SIM ID One Required.'})		
		else:		
			return JsonResponse({'error': 'Wrong pin verification.'})
	else:
		return JsonResponse({'error':'check your request method'})







#==================cashbox transaction pin verification==================================
@csrf_exempt
def cashboxTransactionPinVerification(request):

	if request.method == 'POST':
		data = json.loads(request.body)
		param = getDecryptedValue(data['data'] , BKEY)		
		r = json.loads(param)
		cashbox_mobileno = r['drawer_mobileno']
		cashbox_deviceid = r['drawer_deviceid']
		cashbox_simid_one = r['drawer_simid_one']
		cashbox_simid_two = r['drawer_simid_two']
		cashbox_transaction_pin = r['drawer_transaction_pin']
		cashbox_confirm_transaction_pin = r['drawer_confirm_transaction_pin']
        
		counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = cashbox_mobileno) & Q(cashbox_deviceid__exact = cashbox_deviceid) & Q(cashbox_simid_one__exact = cashbox_simid_one ) ).select_related()

	
		if counter.count() <= 0 :
			return JsonResponse({'error': 'Mobile Number Not Exist.' })
		elif(not cashbox_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not cashbox_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not cashbox_simid_one):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not cashbox_transaction_pin):
			return JsonResponse({'error': 'Transaction PIN Required.'})
		elif(not cashbox_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction Confirm PIN Required.'})
		elif(cashbox_transaction_pin != cashbox_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction and  Confirm PIN Not Matched.'})
		else:             
			customer_id = counter[0].cashbox_customer_id
			transaction_pass = dbEncrypt(cashbox_transaction_pin)
			Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_transaction_pass = transaction_pass)

			return JsonResponse({'success':'true', 'msg': 'Your Transaction PIN updated successfullly.'  })

	else:
		return JsonResponse({'error':'check your request method'})









#================================= Cashbox or Drawer Registration =====================================
@csrf_exempt
def cashboxSignUp(request):
	if request.method  == 'POST':
		r = json.loads(request.body)

		cashbox_first_name = r["cashbox_fname"]
		cashbox_last_name = r["cashbox_lname"]		
		cashbox_mobileno = r["cashbox_mobileno"]
		cashbox_dob = r["cashbox_dob"]	
		cashbox_deviceid = r["cashbox_deviceid"]
		cashbox_simid_one = r["cashbox_simid_one"]
		cashbox_simid_two = r["cashbox_simid_two"]
		cashbox_app_pass = r["cashbox_app_pass"]
		cashbox_app_retype_pass = r["cashbox_app_retype_pass"]		
		cashbox_fcm_token = r["cashbox_fcm_token"]
		cashbox_lat = r["cashbox_lat"]
		cashbox_lon = r["cashbox_lon"]
		cashbox_verified = 1
		drawer_type = r["cashbox_drawer_type"]
	               	
		accessToken = '{0:05}'.format(random.randint(2,100000))
		salt = uuid.uuid4().hex
		cashbox_access_token =  hashlib.sha256(salt.encode() + accessToken.encode()).hexdigest()
	
		#'{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		pocket_authorization_key =  '{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		nomineedobs = "1111-11-11"		


		counter = Cashbox.objects.filter(cashbox_mobileno__exact=cashbox_mobileno).select_related()
		if(counter.count() >0):
			return JsonResponse({'error':'Mobile number already exist'})		
		elif(not cashbox_first_name):
			return JsonResponse({'error':'First Name cannot empty'})
		elif(not cashbox_last_name):
			return JsonResponse({'error':'Last Name cannot empty'})
		elif(not cashbox_mobileno):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not cashbox_simid_one):
			return JsonResponse({'error':'SIM ID one cannot be empty'})
		elif(not cashbox_deviceid):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not cashbox_dob):
			return JsonResponse({'error':'Date of Birth cannot be empty'})
		#elif(cashbox_dob.find("1111-11-11") == -1):
		#	return JsonResponse({'error':'Date of Birth cannot be empty'})
		elif(not cashbox_app_pass):
			return JsonResponse({'error':'App password cannot be empty'})
		elif(not cashbox_app_retype_pass):
			return JsonResponse({'error':'App confirm password cannot be empty'})
		elif(cashbox_app_pass != cashbox_app_retype_pass):
			return JsonResponse({'error':'App password and confirm password not matched!'})
		else:

			customerIDNum = '{0:06}'.format(random.randint(1,10000000))+'101'+cashbox_mobileno+'{0:06}'.format(random.randint(1,1000000))
			customer_pin = '{0:06}'.format(random.randint(1,1000000)) + 'UUCAS'+cashbox_mobileno+ '{0:06}'.format(random.randint(1,1000000))
			cashbox_verification_pin = '{0:06}'.format(random.randint(1,1000000))

			salt_val = uuid.uuid4().hex
			qrCodeNo = hashlib.sha256( salt_val.encode() + cashbox_verification_pin.encode() ).hexdigest()

			if(counter.count() ==0):
				c = Cashbox()
				c.cashbox_customer_id = customerIDNum
				c.cashbox_id = customer_pin
				c.cashbox_fname = cashbox_first_name
				c.cashbox_mname = ''
				c.cashbox_lname = cashbox_last_name
				c.cashbox_mobileno = cashbox_mobileno
				c.cashbox_dob = cashbox_dob
				c.cashbox_access_token = cashbox_access_token
				c.cashbox_deviceid = cashbox_deviceid
				c.cashbox_simid_one = cashbox_simid_one
				c.cashbox_simid_two = cashbox_simid_two
				c.cashbox_qrcode = '0'+qrCodeNo+'.png'
				c.cashbox_app_pass = dbEncrypt(cashbox_app_pass)			
				c.cashbox_fcm_token = cashbox_fcm_token
				c.cashbox_pin_verification_code =  cashbox_verification_pin
				c.cashbox_pin_verified_flag = '0'
				c.cashbox_agree_flag = '0'
				c.cashbox_verified = 1
				c.customer_type_id = '102'
				c.cashbox_lat = cashbox_lat
				c.cashbox_lon = cashbox_lon
				c.save()


				#qr code generation
				salt = uuid.uuid4().hex
				qr_content = "https://www.uumoo.biz/?" + qrCodeNo  
				qr = qrcode.QRCode(
					version=6,
					error_correction=qrcode.constants.ERROR_CORRECT_H,
					box_size=10,
					border=4,
				)
				qr.add_data(qr_content)
				qr.make(fit=True)
				img = qr.make_image(fill_color="#000000", back_color="#ffffff")
				img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png');
				im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png')
				im = im.convert("RGBA")
				logo = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/uumoo_logo.png') 
				box = (250,250,350,350) #tested middle position and black background logo
				#box = (200,200,270,270) (270,270,380,380) (135,135,235,235)
				im.crop(box)
				region = logo
				region = region.resize((box[2] - box[0], box[3] - box[1]))
				im.paste(region,box)
				im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0'+ qrCodeNo  +'.png')
				im.show()		


				#accounts blank entry
				ca_counter = CustomerAccounting.objects.filter(Q(userid__exact = c.cashbox_customer_id )).select_related()
				if( ca_counter.count() <= 0 ):
					ca = CustomerAccounting()				
					ca.userid = c.cashbox_customer_id
					ca.user_type_id = '102'
					ca.customeraccount_transaction_type = ''
					ca.customeraccount_debit = 0.00
					ca.customeraccount_credit = 0.00
					ca.comission_amount = 0.00
					ca.commission_slave_amount = 0.00
					ca.service_fee_amount = 0.00
					ca.customer_referral_withdrawl_amount = 0.00
					ca.customer_balance = 0.00
					ca.business_id = ''
					ca.auxilary_accounting_userid = ''
					ca.referral_id = ''
					ca.save()







				bb = Business()
				bb.app_mobile_number = cashbox_mobileno
				bb.business_name = ''
				bb.business_type = drawer_type
				bb.business_category = ''
				bb.business_nid = ''
				bb.business_nid_image = ''
				bb.business_tradelicense = ''
				bb.business_tradelicense_image = ''
				bb.business_tin = ''
				bb.business_tin_image = ''
				bb.business_mobileno = ''
				bb.business_verified = ''
				bb.business_approx_monthly_transaction_amount = 0.00
				bb.business_max_balance_amount = 0.00
				bb.business_logo = ''
				#bb.business_address_id = ''
				bb.business_registration_timestamp = datetime.datetime.now()
				bb.save()





				#business id save into cashbox table
				Cashbox.objects.filter(Q(cashbox_customer_id__exact = c.cashbox_customer_id )).update(cashbox_business_id=bb.business_id)
				return JsonResponse({'success':'true','verification_pin': cashbox_verification_pin,'msg':'Cashbox registration complete.'})
			else:
				return JsonResponse({'error':'Mobile Number Alreasy Exist'})

	else:
		return JsonResponse({'error':'Please check your request method'})




#================================== Pocket FCM Token Update ================================================
@csrf_exempt
def fcmTokenUpdate(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno = r["mobileno"] 	
		user_type = r["user_type"]
		deviceid = r["deviceid"]
		simid_one = r["simid_one"]
		simid_two = r["simid_two"]
		fcm_token = r["fcm_token"]

		if(not mobileno):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User type cannot be empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SIM ID one cannot be empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			if(user_id == 0):
				return JsonResponse({'error':'Mobile is not registered'})
			else:
				print("%s %s %s"% (mobileno , user_type, fcm_token)) 
				if(user_type=='101'):
					print("one")
					Pocket.objects.filter(customer_id__exact = user_id).update(pocket_fcm_token=fcm_token)
					return JsonResponse({'success':'true' , 'msg':'Fcm tokend updated'})
				elif(user_type=='102'):
					print("two")
					Cashbox.objects.filter(cashbox_customer_id__exact = user_id).update(cashbox_fcm_token=fcm_token)
					return JsonResponse({'success':'true' , 'msg':'Fcm tokend updated'})
				else:
					return JsonResponse({'error':'Invalid User type'})
	else:
		return JsonResponse({'error':'Please check your request method'})





#============================== Pocket Common Notification(For Messages) ==================================================
def sendCommonNotification(fcm_token , title , msg_body ,msg_type , msg_date,  custom_date, msg_time , mobile_no , profile_pic ):
	
	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")
	

	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
	#geo = bussiness logo
	if(msg_type == 'sys'):
		data_message = {
			"msg_title": title ,
			"msg": msg_body , 
			"type": msg_type  ,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no": mobile_no ,   #"01300000000" ,
			"time": msg_time 		
		}	
	elif(msg_type == 'geo'):
		data_message = {
			"msg_title": title ,
			"msg": msg_body ,
			"type": msg_type  ,
			"img": profile_pic,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no":mobile_no ,  #"01300000000" ,
			"time": msg_time
		}
	else:
		data_message = {
			"msg_title": title ,
			"msg": msg_body ,
			"type": msg_type  ,
			"date": msg_date ,
			"custom_date": custom_date,
			"mobile_no": mobile_no , #"01300000000" ,
			"time": msg_time
		}


	result = push_service.single_device_data_message(registration_id=fcm_token , data_message=data_message)
	#result = push_service.notify_single_device(registration_id=fcm_token , message_title=title , message_body=msg_body)
	return result





#================================== Pocket Transaction Notification ===============================================
def sendTransactionNotification(flag,fcm_token,title,trxid,sender,receiver,amount,service_fee,transaction_type,transaction_date):
	#push_service = FCMNotification(api_key="AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F")


	push_service = FCMNotification(api_key = str(DRAWER_FCM_KEY) )
                      
	if(flag == 1):
		#sender
		data_message = {
			"msg_title": title ,
			"trxid": trxid ,
			"msg": "To:"+ receiver +", Amount:"+amount ,
			"sender": sender ,
			"receiver": receiver ,
			"amount": amount ,
			"service_fee": service_fee ,
			"transaction_type": transaction_type ,
			"type":"trn" ,
			"date": transaction_date
		}
	else:
		#receiver
		data_message = {		
			"msg_title": title ,
			"trxid": trxid ,
			"msg": "From:"+ sender +", Amount:"+amount ,
			"sender": sender ,
			"receiver": receiver ,
			"amount": amount ,
			"service_fee": service_fee ,
			"transaction_type": transaction_type ,
			"type":"trn" , #trn=Transaction
			"date": transaction_date
		}


        #for single data notification
	result = push_service.single_device_data_message(registration_id=fcm_token , data_message=data_message)

	#registration_ids = []
	#registration_ids.append( sender_fcm_token )
	#registration_ids.append( receiver_fcm_token )

        #["<device registration_id 1>", "<device registration_id 2>", ...]

	#result = push_service.multiple_devices_data_message(registration_ids=registration_ids, data_message=data_message)

	return result











#============================ Cashbox Profile Image,NID,Nominee Image ===============================================
@csrf_exempt
def imageFileseInformationUpdateCashbox(request):
	if request.method == 'POST':
		#r = json.loads(request.body)
		customer_mobile = request.POST.get('customer_mobile')	
		image_type = request.POST.get('image_type')
		pocket_image = request.FILES['avatar'].name  
		extension = pocket_image.split(".")[-1]
	

		if(not customer_mobile):
			return JsonResponse({'error':'Mobile Number must not be Empty.'})	
		elif(not image_type):
			return JsonResponse({'error':'Image Type must not be Empty.'})
		elif(not pocket_image):
			return JsonResponse({'error':'Image must not be Empty.'})
		else:
			token_generate = '{0:06}'.format(random.randint(1,10000000))			
			salt = uuid.uuid4().hex
			img =  hashlib.sha256(salt.encode() + token_generate.encode()).hexdigest()+'.'+extension
			handle_uploaded_file(request.FILES['avatar'] , img)

			counter = Cashbox.objects.filter(cashbox_mobileno__exact = customer_mobile ).select_related()

			if(counter.count() > 0):
				customer_id = counter[0].cashbox_customer_id
				if(image_type == 'profile_image'):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_photo=img)
					return JsonResponse({'success':'true', 'msg':'Profile Image uploaded', 'filename': img})
				elif(image_type == 'nid'):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nid_image = img)
					return JsonResponse({'success':'true' ,'msg':'NID Image uploaded' , 'filename': img })
				elif(image_type == 'vid'):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_vodetid_image =img)
					return JsonResponse({'success':'true', 'msg':'Voter ID Image uploaded', 'filename': img })
				elif(image_type == 'nominee_nid'):
					Cashbox.objects.filter(cashbox_customer_id__exact = customer_id).update(cashbox_nominee_vid_image = img)
					return JsonResponse({'success':'true', 'msg':'Nominee NID Image uploaded', 'filename': img })
				else:
					return JsonResponse({'error':'Image type error'})

				
				return JsonResponse({'status':'true','filename': img , 'msg': 'Information updated successfully.'})
			else:
				return JsonResponse({'error':'Mobile Number Not Exist'})

	else:
		return JsonResponse({'error':'check your request method'})







#============================ Cashbox Business Profile Image Upload =====================================
@csrf_exempt
def cashboxBusinessProfileImageUpload(request):
	if request.method == 'POST':
		customer_mobile = request.POST.get('customer_mobile')	
		image_type = request.POST.get('image_type')
		cashbox_image = request.FILES['avatar'].name 
		extension = cashbox_image.split(".")[-1]

		print("Image Type: %s %s %s"%(customer_mobile, image_type , extension))

		if(not customer_mobile):
			return JsonResponse({'error':'Mobile Number cannot be empty'})
		elif(not image_type):
			return JsonResponse({'error':'Image Type cannot be empty'})
		elif(not cashbox_image):
			return JsonResponse({'error':'Image cannot be empty'})
		else:	
			token_generate = '{0:06}'.format(random.randint(1,10000000))
			salt = uuid.uuid4().hex
			img =  hashlib.sha256(salt.encode() + token_generate.encode()).hexdigest()+'.'+extension
			handle_uploaded_file(request.FILES['avatar'] , img)

			counter = Cashbox.objects.filter(cashbox_mobileno__exact = customer_mobile ).select_related()
			bcounter = Business.objects.filter(app_mobile_number__exact = customer_mobile).select_related()
			if( counter.count() > 0 ):
				customer_id = counter[0].cashbox_customer_id
				if(bcounter.count() >0):
					businessID = bcounter[0].business_id
				else:
					businessID = 0





				if(counter[0].cashbox_customer_id==None):
					if(businessID != 0):
						Cashbox.objects.filter(Q(cashbox_customer_id__exact=customer_id)).update(cashbox_business_id=bcounter[0].business_id)






				print("%s %s"% (businessID,customer_id ))
				
				if(businessID  == ''):
					return JsonResponse({'error':'Business Information Empty'})
				else:
					
					if(image_type == 'business_profile'):			
						Business.objects.filter(app_mobile_number__exact = customer_mobile).update(business_logo= img)
						return JsonResponse({'success':'true', 'filename':img , 'msg':'Business Information Image Uploaded'})	
					elif(image_type == 'tin'):
						Business.objects.filter(app_mobile_number__exact = customer_mobile).update(business_tin_image =img)			
						return JsonResponse({'success':'true', 'filename':img , 'msg':'Business Information Image Uploaded'})
					elif(image_type == 'trade'):
						Business.objects.filter(app_mobile_number__exact = customer_mobile).update(business_tradelicense_image =img)				
						return JsonResponse({'success':'true', 'filename': img , 'msg':'Business Information Image Uploaded'})
					else:
						return JsonResponse({'error':'Wrong Image Type'})
					
			else:
				return JsonResponse({'error':'Mobile Number Not Exist'})

	else:
		return JsonResponse({'error':'Please check your request method'})






#=====================decryption helper==========================
def getDecryptedValue(enc,key):
	d = AESCipher(key)
	r = d.decrypt(enc)
	return str(r)


#===================encryption helper ===========================
def getEncryptedValue(msg,key):
	d = AESCipher(key)
	c = d.encrypt(msg)
	return str(c)







#================================= Cashbox Login ===========================================================
@csrf_exempt
def cashboxLogin(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		params = getDecryptedValue(r['data'], BKEY)
		param = json.loads(params)
		mobile_number = param['mobile_number']
		deviceid = param['deviceid']
		simid    = param['simid']    
		app_pass = param['app_pass']
		fcm_token = param['fcm_token']
		#print("Values: %s %s %s %s" % (mobile_number,deviceid,simid, app_pass))





		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not simid):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not app_pass):
			return JsonResponse({'error':'App Password cannot be empty'})
		else:
			
			if(fcm_token is not None and fcm_token != ''):
				Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile_number) & Q(cashbox_simid_one__exact=simid) & Q(cashbox_deviceid__exact=deviceid)).update(cashbox_fcm_token=fcm_token)


			print(dbEncrypt(app_pass) )
			print("Mobile:%s , SIM ID:%s"% (mobile_number,simid))
			counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile_number) & Q(cashbox_simid_one__exact=simid) & Q(cashbox_app_pass__exact = dbEncrypt(app_pass) )  ).select_related()
			print("Test: %s" % ( counter.count()  ))
			if(counter.count() > 0):
				
				deviceCounter = Cashbox.objects.filter(Q(cashbox_deviceid__exact=deviceid)).select_related()
				print("Customer ID: %s"% (counter[0].cashbox_customer_id))
				#deviceid replace
				if(deviceCounter.count() == 0 ):
					Cashbox.objects.filter(Q(cashbox_customer_id__exact = counter[0].cashbox_customer_id )).update(cashbox_deviceid=deviceid)
					warning_msg = "You are Logged in by Device"
				else:
					warning_msg = ""


				

				bb = Business.objects.filter(Q(app_mobile_number__exact=mobile_number)).select_related()
				if(bb.count()>0):
					drawerType = bb[0].business_type
					print("drawer-type: %s"% drawerType)
					business_name = bb[0].business_name
					business_type = bb[0].business_type
					business_category = bb[0].business_category
					business_nid = bb[0].business_nid
					business_tradelicense = bb[0].business_tradelicense
					business_tin = bb[0].business_tin
					business_mobileno = bb[0].business_mobileno
					business_email = bb[0].business_email
					business_logo = bb[0].business_logo
					payment_limit = bb[0].business_approx_monthly_transaction_amount
					business_address_id = bb[0].business_address_id
					addr_counter = DrawerAddress.objects.filter(Q(address_id__exact= business_address_id)).select_related()
					if(addr_counter.count() > 0):
						address = addr_counter[0].reg_address
						division = addr_counter[0].division
						thana = addr_counter[0].thana
						district = addr_counter[0].district
						postcode = addr_counter[0].postcode
						lat = addr_counter[0].latitude
						lag = addr_counter[0].longitude
					else:
						address = ''
						division = ''
						district = ''
						thana = ''
						postcode = ''
						lat = ''
						lag = ''
				else:
					drawerType = 'Individual'
					business_name = ''
					business_type = ''
					business_category = ''
					business_nid = ''
					business_tradelicense = ''
					business_tin = ''
					business_mobileno = ''
					business_email = ''
					business_logo = ''
					payment_limit = ''
					address = ''
					division = ''
					district = ''
					thana = ''
					postcode = ''
					lat = ''
					lag = ''




				pre_add = PocketAddress.objects.filter(Q(address_id__exact=counter[0].cashbox_present_address_id)).select_related()
				if(pre_add.count() > 0):
					pre_address = pre_add[0].reg_address
					pre_division = pre_add[0].division
					pre_thana = pre_add[0].thana
					pre_district = pre_add[0].district 
					pre_postcode = pre_add[0].postcode
					pre_lat = pre_add[0].latitude
					pre_lon = pre_add[0].longitude
				else:
					pre_address = ''
					pre_division = ''
					pre_thana =''
					pre_district = ''
					pre_postcode =''
					pre_lat = ''
					pre_lon = ''

				
				par_add = PocketAddress.objects.filter(Q(address_id__exact=counter[0].cashbox_permanent_address_id)).select_related()
				if(par_add.count() > 0):
					par_address = par_add[0].reg_address
					par_division = par_add[0].division
					par_thana = par_add[0].thana
					par_district = par_add[0].district
					par_postcode = par_add[0].postcode
					par_lat = par_add[0].latitude
					par_lon = par_add[0].longitude
				else:
					par_address = ''
					par_division = ''
					par_thana = ''
					par_district = ''
					par_postcode = ''
					par_lat = ''
					par_lon = ''
				



				if(counter[0].cashbox_pin_verified_flag == '0'):
					return JsonResponse({'success':'true','pin_verified_flag':'0'})
				elif(counter[0].cashbox_agree_flag == '0'):
					return JsonResponse({'success':'true','agree_verified_flag':'0'})
				elif(counter[0].cashbox_transaction_pass == ''):
					return JsonResponse({'success':'true','trans_verified_flag':'0'})
				else:				
					#credential ok
					resp = '{"user_type":"'+ str(counter[0].customer_type_id)+'" ,"mobile_number":"'+ str(counter[0].cashbox_mobileno) +'" ,"device_id":"'+ str(counter[0].cashbox_deviceid) +'" ,"sim_id":"'+ str(counter[0].cashbox_simid_one) +'" ,"sim_id_two":"'+ str(counter[0].cashbox_simid_two)+'" ,"access_token":"'+ str(counter[0].cashbox_access_token) +'","authorization_key":"'+ str(counter[0].cashbox_authorization_key)+'","fname":"'+ str(counter[0].cashbox_fname)+'" ,"mname":"'+ str(counter[0].cashbox_mname)+'","lname":"'+ str(counter[0].cashbox_lname) +'","dob":"'+ str(counter[0].cashbox_dob) +'","gender":"'+ str(counter[0].cashbox_gender)+'","vid":"'+ str(counter[0].cashbox_vodetid) +'","nid":"'+ str(counter[0].cashbox_nationalD) +'","email":"'+ str(counter[0].cashbox_email)+'","alternate_mobile":"'+ str(counter[0].cashbox_alternate_mobileno) +'","photo":"'+ str(counter[0].cashbox_photo)+'","nid_image":"","qrcode":"'+ str(counter[0].cashbox_qrcode)+'","nominee_fname":"'+ str(counter[0].cashbox_nominee_fname) +'" ,"nominee_mname":"'+ str(counter[0].cashbox_nominee_mname)+'" ,"nominee_lname":"'+ str(counter[0].cashbox_nominee_lname) +'","nominee_contactno":"'+ str(counter[0].cashbox_nominee_contactno)+'","nominee_voterid":"'+ str(counter[0].cashbox_vodetid) +'","nominee_nid":"'+ str(counter[0].cashbox_nationalD) +'" ,"nominee_relation":"'+ str(counter[0].cashbox_nominee_relation)+'" ,"nominee_voterid_image":"" ,"permanent_address":{"address":"'+ str(par_address)+'" , "division":"'+str(par_division)+'","thana":"'+ str(par_thana)+'","postcode":"'+ str(par_postcode)+ '","district":"'+ str(par_district) +'"} ,"present_address": {"address":"'+ str(pre_address)+'" ,"division":"'+ str(pre_division) +'","district":"'+str(pre_district)+'" ,"thana":"'+str(pre_thana)+'","postcode":"'+ str(pre_postcode) +'" } , "drawer_type":"'+ str(drawerType) +'", "business_name":"'+ str(business_name) +'","business_photo":"'+ str(business_logo) +'","business_payment_limit":"'+ str(payment_limit) +'","business_category":"'+ str(business_category) +'","business_tradelicense":"'+ str(business_tradelicense)+'","business_tin":"'+ str(business_tin)+'","business_mobileno":"'+ str(business_mobileno)+'","business_email":"'+ str(business_email)+'","business_address":{"address":"'+ str(address) +'","division":"'+str(division)+'","district":"'+str(district)+'","thana":"'+ str(thana) +'", "postcode":"'+ str(postcode) +'"}}'
						

					#print(resp)
					payload_msg = getEncryptedValue(resp , BKEY)

					print(payload_msg)
					return JsonResponse({'success':'true','msg':'Login Successfully.','warning':warning_msg, 'payload': payload_msg  })					
			else:
				return JsonResponse({'error':'Invalid Password '})		
		
	else:
		return JsonResponse({'error':'Please check your request method'})







#=========================  part 01 ===============================================

def customerNameDetailInfo(userid):
	c = Pocket.objects.filter( customer_id__exact=userid).select_related()
	if(c.count() > 0):
		return c[0].pocket_fname+' '+c[0].pocket_lname+'|'+c[0].pocket_photo+'|'+c[0].pocket_mobileno+'|'+c[0].pocket_fcm_token	
	else:
		cc = Cashbox.objects.filter(cashbox_customer_id__exact=userid).select_related()
		if(cc.count() > 0 ):
			bc = Business.objects.filter(Q(business_id__exact = cc[0].cashbox_business_id )).select_related()
			if(bc.count() == 0 ):
				fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				photo = cc[0].cashbox_photo
				mobile = cc[0].cashbox_mobileno
			else:
				mobile = cc[0].cashbox_mobileno				
				if(bc[0].business_name == ''):
					fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				else:
					fname = fname = bc[0].business_name


				if(bc[0].business_logo == ''):
					photo = cc[0].cashbox_photo	
				else:
					photo = bc[0].business_logo


			return fname +'|'+ photo +'|'+ mobile +'|'+ cc[0].cashbox_fcm_token
			#return cc[0].cashbox_fname+' '+cc[0].cashbox_lname+'|'+cc[0].cashbox_photo+'|'+cc[0].cashbox_mobileno+'|'+cc[0].cashbox_fcm_token	
		else:
			return 0





def customerNameDetail(userid):	
	c = Pocket.objects.filter( customer_id__exact=userid).select_related()
	if(c.count() > 0 ):
		return c[0].pocket_fname+' '+c[0].pocket_lname+'|'+c[0].pocket_photo+'|'+c[0].pocket_mobileno
	else:
		cc = Cashbox.objects.filter(cashbox_customer_id__exact=userid).select_related()
		if(cc.count() >0):
			bc = Business.objects.filter(Q(business_id__exact = cc[0].cashbox_business_id )).select_related()
			if(bc.count() == 0 ):
				fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				photo = cc[0].cashbox_photo
				mobile = cc[0].cashbox_mobileno
			else:
				mobile = cc[0].cashbox_mobileno
				if(bc[0].business_name == ''):
					fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				else:
					fname = bc[0].business_name


				if(bc[0].business_logo == ''):
					photo = cc[0].cashbox_photo
				else:
					photo = bc[0].business_logo



			return fname +'|'+ photo+'|'+mobile
			#return cc[0].cashbox_fname+' '+cc[0].cashbox_lname+'|'+cc[0].cashbox_photo+'|'+cc[0].cashbox_mobileno
		else: 
			return 0

	
	

#==============================================================================================================



#======================= Pocket Withdraw or Cashout Log Data ==========================================
#cashout log data code = 10004
@csrf_exempt
def cashoutLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		# calendar.monthrange(today.strftime('%Y') , today.strftime('%m'))	
				
		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10004'):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom flag cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)
			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')

			if(is_custom == 'no'):
				if(search_type=='*'):
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type,today))
					#for today 
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today)).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					#for week
					print("%s %s %s"%(search_type,today , weekdate))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					#for month
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)
					
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					#for all
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			else:
				#for custom
				start,end = search_type.split(',')			
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()





			if(sending_logs.count()>0):			
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone =''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p==0):
						name = ''
						photo =''
						phone = ''
					else:
						name,photo,phone = p.split('|')


					print("%s %s %s %s"% ((sl.transaction_timestamp).strftime("%d-%m-%Y"),sl.sender_userid , sl.receiver_userid , name))	

					sending_log = {
						'trxid':sl.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount': str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)


				return JsonResponse({'success':'true','data':sending_data  }) 
			else:
				return JsonResponse({'error':'No Cashout Data Found'})
	else:
		return JsonResponse({'error':'Please Check your request method'})







#=========================== Pocket Payment Log =============================================
#pay log data code = 10002
@csrf_exempt
def payLogData(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']

		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(log_type != '10002'):
			return JsonResponse({'error':'Invalid Log Type'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)	


			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)

			last = calendar.mdays[today.month]
			#last_date = today - datetime.timedelta(days=calendar.mdays[ int(search_type)+1]) # today.month])
			first_date = today.strftime('%Y-%m-01')


			if(is_custom == 'no'):
				if(search_type=='*'):
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"%(search_type , today))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__contains=today )).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					print("%s %s  %s"%(search_type , weekdate,today))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__gte=weekdate)  ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):				
					
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1)					
					print("block# %s %s %s"% (search_type, month_first_date , month_last_date))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[month_first_date,month_last_date]) ).order_by('-transaction_timestamp').select_related()
				else:
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			else:
				start,end = search_type.split(',')
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) & Q(transaction_timestamp__range=[start,end]) ).order_by('-transaction_timestamp').select_related()

					



			#sending_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & Q(sender_userid__exact=user_id) ).order_by('-transaction_timestamp').select_related()
			if(sending_logs.count()>0):
				sending_data = []
				for sl in sending_logs:
					p = customerNameDetail(sl.receiver_userid)
					ss = customerNameDetail(sl.sender_userid)
					
					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone = ''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|')

					if(p ==0):
						name = ''
						photo = ''
						phone = ''
					else:
						name,photo,phone = p.split('|')

					print("%s %s %s"% (sl.sender_userid , sl.receiver_userid , name))					

					sending_log = {
						'trxid': sl.customer_transaction_id ,
						'name': name ,
						'photo': photo ,
						'phone': phone ,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': sl.reference_txt ,
						'amount':str(decimal.Decimal(sl.transaction_amount))+'TK' ,
						'transaction_date': (sl.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (sl.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)
				return JsonResponse({'success':'true', 'data': sending_data })
			else:
				return JsonResponse({'error':'No Pay Data Found'})

	else:
		return JsonResponse({'error':'Please check your request method'})






#=================== Pocket Transfer Log ============================================
#transfer log data code = 10001
@csrf_exempt
def transferLogData(request):
	if request.method  == 'POST':
		r = json.loads(request.body)
		mobile_number  = r['mobile_number']
		user_type = r['user_type']
		device_id = r['device_id']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		log_type = r['log_type']
		search_type = r['search_type']
		is_custom = r['is_custom']
		
		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not log_type):
			return JsonResponse({'error':'Log Type cannot be empty'})
		elif(log_type != '10001'):
			return JsonResponse({'error':'Invalid Log Type'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type cannot be empty'})
		elif(not is_custom):
			return JsonResponse({'error':'Is Custom cannot be empty'})
		else:
			user_id = getCustomerInfo(mobile_number , user_type)	

			today = date.today()
			weekdate =  today - datetime.timedelta(days=7)
			last = calendar.mdays[today.month]
			first_date = today.strftime('%Y-%m-01')


			if(is_custom == 'no'):
				if(search_type=='*'):
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) ) ).order_by('-transaction_timestamp').select_related()
				elif(search_type=='today'):
					print("%s %s"% (search_type,today))	
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) ) & Q(transaction_timestamp__contains=today )).order_by('-transaction_timestamp').select_related()
				elif(search_type=='week'):
					print("block# %s %s %s"% (search_type, weekdate,today))
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id)) & Q(transaction_timestamp__gte=weekdate) ).order_by('-transaction_timestamp').select_related()	
				elif(int(search_type)>=0 or int(search_type)<=11):
					
					month_first = str(today.year)+"-"+str(int(search_type)+1)+"-01"
					month_first_date = datetime.datetime.strptime(month_first , "%Y-%m-%d").date()
					last_num = calendar.mdays[ int(search_type)+1]
					month_last_date =  month_first_date + datetime.timedelta(days=int(last_num)-1 )
					print("block# %s %s %s"% (search_type, month_first_date , month_last_date))				
					
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id)) & Q(transaction_timestamp__range=[month_first_date,month_last_date])  ).order_by('-transaction_timestamp').select_related()
				else:
					sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id)) ).order_by('-transaction_timestamp').select_related()
			else:
				start,end = search_type.split(',')	
				print("Custom: %s %s"% (start , end))
				sending_logs = CustomerTransaction.objects.filter( Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id)) & Q(transaction_timestamp__range=[start,end])  ).order_by('-transaction_timestamp').select_related()





			#sending_logs = CustomerTransaction.objects.filter(Q(transaction_type__exact=log_type) & (Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id)) ).order_by('-transaction_timestamp').select_related()


			if(sending_logs.count()>0):
				sending_data = []
				for l in sending_logs:
					#print("%s %s", (l.sender_userid , l.receiver_userid))
					#if(l.receiver_userid != user_id):
					p = customerNameDetail(l.receiver_userid)
					ss = customerNameDetail(l.sender_userid)

					if(ss ==0):
						ss_name = ''
						ss_photo = ''
						ss_phone = ''
					else:
						ss_name,ss_photo,ss_phone = ss.split('|') 

					if(p == 0):
						name = ''
						photo = ''
						phone=''
					else:
						name,photo,phone= p.split('|')
				
					print("%s %s %s"% (l.sender_userid , l.receiver_userid , name))									
					sending_log = {
						'trxid':l.customer_transaction_id ,
						'name': name ,
						'photo':  photo ,
						'phone': phone ,
						'ss_name': ss_name ,
						'ss_photo': ss_photo ,
						'ss_phone': ss_phone ,
						'reference': l.reference_txt ,
						'amount': str(decimal.Decimal(l.transaction_amount))+'TK' ,
						'transaction_date': (l.transaction_timestamp).strftime("%d/%m/%Y"),
						'transaction_time': (l.transaction_timestamp).strftime("%I:%M %p")
					}
					sending_data.append(sending_log)
				return JsonResponse({'success':'true', 'data': sending_data  })
			else:
				return JsonResponse({'error':'No Transfer data found'})

	else:
		return JsonResponse({'error':'Please check your request method'})





#=========================== Pocket Forgot Both ==========================================
@csrf_exempt
def forgotBoth(request):
	if request.method == 'POST':
		params = json.loads(request.body)
		param_data = getDecryptedValue(params['data'] , BKEY)
		r = json.loads(param_data)
		mobile_number  = r['mobile_number']
		sim_id_one = r['sim_id_one']
		sim_id_two = r['sim_id_two']
		user_type = r['user_type']
		device_id = r['device_id']		
		dob = r['dob']
		nominee_name = r['nominee_name']
		flag = r['flag']
		nid_vid = r['nid_vid']
		last_transaction_amount = r['last_transaction_amount']

		user_id = getCustomerInfo(mobile_number , user_type)
		
		if(not mobile_number):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not sim_id_one):
			return JsonResponse({'error':'SIM ID cannot be empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type cannot be empty'})
		elif(not device_id):
			return JsonResponse({'error':'Device ID cannot be empty'})
		elif(not dob):
			return JsonResponse({'error':'Date of Birth cannot be empty'})
		elif(not nominee_name):
			return JsonResponse({'error':'Nominee Name cannot be empty'})
		elif(not flag):
			return JsonResponse({'error':'Flag cannot be empty'})
		elif(not nid_vid):
			return JsonResponse({'error':'NID or Voter ID cannot be empty'})		
		elif(not last_transaction_amount):
			return JsonResponse({'error':'Last Transaction amount cannot be empty'})
		elif(user_id == 0):
			return JsonResponse({'error':'Mobile number not found'})
		else:
			if(user_type=='101'):			
				pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()								
				if(pocket_counter.count() == 0):
					return JsonResponse({'error':'Mobile number not found'})
				else:				
					pocket_user_id = pocket_counter[0].customer_id
					check_dob = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & Q(pocket_dob__exact=dob)).select_related()
					check_nominee_name = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & Q(pocket_nominee_lname__exact=nominee_name)).select_related()
					check_nid_vid = Pocket.objects.filter(Q(customer_id__exact=pocket_user_id) & (Q(pocket_nid__exact=nid_vid) | Q(pocket_voterlD__exact=nid_vid)) ).select_related()


			

					transaction = CustomerTransaction.objects.filter(Q(transaction_amount__exact=last_transaction_amount) & (Q(sender_userid__exact=pocket_user_id) | Q(receiver_userid__exact=pocket_user_id))).order_by('-transaction_timestamp').select_related()					
					print ("id:%s " % pocket_user_id)
					if(check_dob.count() ==0):
						return JsonResponse({'error':'Date of birthd not matched'})
					elif(check_nominee_name.count() ==0 ):
						return JsonResponse({'error':'Nominee name not matched'})
					elif(check_nid_vid.count() == 0):
						return JsonResponse({'error':'NID/VID not matched'})	
					elif(transaction.count() == 0):
						return JsonResponse({'error':'No trasaction found'})
					else:						
						return JsonResponse({'success':'true','msg':'All answer is matched'})

			elif(user_type=='102'):
				cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one)).select_related()				
				if(cashbox_counter.count() == 0):
					return JsonResponse({'error':'Mobile number not found' })				
				else:
					cashbox_user_id = cashbox_counter[0].cashbox_customer_id
					transaction = CustomerTransaction.objects.filter(Q(transaction_amount__exact=last_transaction_amount) & (Q(sender_userid__exact=cashbox_user_id) | Q(receiver_userid__exact=cashbox_user_id))).order_by('-transaction_timestamp').select_related()


					checkdob = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_dob__exact=dob)).select_related()

					checknomineeinfo = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & Q(cashbox_nominee_lname__exact=nominee_name)).select_related()

					checknidvid = Cashbox.objects.filter(Q(cashbox_customer_id__exact=cashbox_user_id) & (Q(cashbox_nationalD__exact=nid_vid) | Q(cashbox_vodetid__exact=nid_vid))).select_related()

					if(checkdob.count() ==0 ):
						return JsonResponse({'error':'Date of birth not matched'})
					elif(checknomineeinfo.count() == 0):
						return JsonResponse({'error':'Nominee name not matched'})
					elif(checknidvid.count() == 0):
						return JsonResponse({'error':'NID/VID not matched'})
					elif(transaction.count() == 0):
						return JsonResponse({'error':'No Transaction found'})
					else:
						return JsonResponse({'success':'true', 'msg':'All answer is matched' })
			else:
				return JsonResponse({'error':'Invalid User Type'});
			
	else:
		return JsonResponse({'error':'Please check your request method'})






@csrf_exempt
def forgotTransactionPin(request):
	if request.method  == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21802C') ).select_related()		
		broken_key = hooker[0].broken_key
		try:
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'] , broken_key )
			r = json.loads(param_data)
			mobile_number  = r['mobile_number']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			user_type = r['user_type']
			device_id = r['device_id']
			app_pass = r['app_pass']
			app_password = dbEncrypt(app_pass)		

			user_id = getCustomerInfo(mobile_number , user_type)

			if(user_id==0):
				return JsonResponse({'error':'Invalid User'})
			elif(not mobile_number):
				return JsonResponse({'error':'Mobile Number cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'SIM ID 1 cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'Usert Type cannot be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device Id cannot be empty'})
			elif(not app_pass):
				return JsonResponse({'error':'App Password cannot be empty'})
			else:
			
				if(user_type=='101'):
					pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_app_pass = app_password)).select_related()								
				
					if(pocket_counter.count() ==0):
						return JsonResponse({'error':'Mobile Number is not found'})
					else:
						return JsonResponse({'success':'true','msg':'App Password is correct'})

				elif(user_type=='102'):
					cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_app_pass__exact= app_password )).select_related()				

					if(cashbox_counter.count() == 0):
						return JsonResponse({'error':'Mobile number is not found'})
					else:
						return JsonResponse({'success':'true','msg':'App Password is correct'})

				else:
					return JsonResponse({'error':'Invalid User Type'})
		except:
			return JsonResponse({'error':'Invalid parameters'})
	else:
		return JsonResponse({'error':'Please Check your request method'})





#=========================== Pocket Forgot Transaction PIN ===================================
@csrf_exempt
def forgotTransactionPINUpdate(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21802C') ).select_related()
		broken_key = hooker[0].broken_key

		try:
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'] , broken_key )
			r = json.loads(param_data)
			mobile_number  = r['mobile_number']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			user_type = r['user_type']
			device_id = r['device_id']
			new_transaction_pin = r['new_transaction_pin']		
			confirm_new_transaction_pin = r['confirm_new_transaction_pin']

			user_id = getCustomerInfo(mobile_number , user_type)

			if(not mobile_number):
				return JsonResponse({'error':'Mobile number cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'SIM ID cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'User Type cannot be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device ID cannot be empty'})
			elif(not new_transaction_pin):
				return JsonResponse({'error':'New Transaction PIN cannot be empty'})
			elif(not confirm_new_transaction_pin):
				return JsonResponse({'error':'Confirm New Transaction PIN cannot be empty'})
			elif(new_transaction_pin != confirm_new_transaction_pin):
				return JsonResponse({'error':'New Transaction PIN and Confirm New Transaction PIN cannot matched'})
			else:

				if(user_type=='101'):
					pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number)& Q(pocket_deviceid__exact=device_id) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()

					if(pocket_counter.count()== 0):
						return JsonResponse({'error':'Mobile not found'})
					else:
						transaction_pin = dbEncryptTwo(new_transaction_pin)
						Pocket.objects.filter(customer_id__exact = user_id).update(pocket_transaction_pass = transaction_pin )
						return JsonResponse({'success':'true' , 'msg':'Transaction PIN is updated'}) 	

				elif(user_type=='102'):
					cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_transaction_pass__exact = transaction_pin)).select_related()
					if(cashbox_counter.count() == 0):
						return JsonResponse({'error':'Mobile Number not found'})
					else:
						transaction_pin = dbEncryptTwo(new_transaction_pin)
						Cashbox.objects.filter(cashbox_customer_id__exact = user_id).update(cashbox_transaction_pass = transaction_pin ) 					
						return JsonResponse({'success':'true','msg':'Transaction PIN is updated'})
				else:
					return JsonResponse({'error':'Invalid User Type'})			
		


		except:
			return JsonResponse({'error':'Invalid Parameter'})	
	else:
		return JsonResponse({'error':'Please Check you request method'})






#============================ Pocket Forgot Password =============================================
@csrf_exempt
def forgotPassword(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21801B') ).select_related()		
		broken_key = hooker[0].broken_key
		try:
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'],  broken_key )
			r = json.loads(param_data)
			mobile_number  = r['mobile_number']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			user_type = r['user_type']
			device_id = r['device_id']
			transaction_pin = r['transaction_pin']
			transaction_pass = dbEncryptTwo(transaction_pin)		

			print("%s %s %s %s %s %s %s"%(mobile_number , sim_id_one , sim_id_two , user_type , device_id , transaction_pin , transaction_pass))
			user_id = getCustomerInfo(mobile_number , user_type)
			if(user_id == 0):
				return JsonResponse({'error':'Invalid User'})
			elif(not mobile_number):
				return JsonResponse({'error':'Mobile Number cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'Sim ID 1 cannot be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device Id cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'User Type cannot be empty'})
			elif(not transaction_pin):
				return JsonResponse({'error':'Transaction PIN cannot be empty'})
			else:
				if(user_type=='101'):
					pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & Q(pocket_deviceid__exact=device_id)  & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_transaction_pass__exact = transaction_pass)).select_related()				
					if(pocket_counter.count() == 0):
						return JsonResponse({'error':'Invalid Transaction PIN'})
					else:
						return JsonResponse({'success':'true', 'msg':'Transaction PIN is correct.'})

				elif(user_type == '102'):			
					cashbox_counter = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one) & Q(cashbox_transaction_pass__exact=transaction_pass)).select_related()
					if(cashbox_counter.count() == 0):
						return JsonResponse({'error':'Invalid Transaction PIN' })
					else:	
						return JsonResponse({'success':'true' , 'msg':'Transaction PIN is correct' })
					
				else:
					return JsonResponse({'error':'Invalid User Type' })
		except:
			return JsonResponse({'error':'Parameter Missing'})
	else:
		return JsonResponse({'error': 'Please Check Your Request Method'})





#=========================== Pocket Forgot Password ===================================
@csrf_exempt
def forgotPasswordUpdate(request):
	hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21800A') ).select_related()		
	broken_key = hooker[0].broken_key

	if request.method == 'POST':
		try:		
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'], broken_key )
			r = json.loads(param_data)
			mobile_number  = r['mobile_number']
			sim_id_one = r['sim_id_one']
			sim_id_two = r['sim_id_two']
			user_type = r['user_type']
			device_id = r['device_id']
			new_app_pass = r['new_app_pass']		
			confirm_new_app_pass = r['confirm_new_app_pass']
	
			user_id = getCustomerInfo(mobile_number , user_type)
			if(user_id == 0):
				return JsonResponse({'error':'Invalid User'})
			elif(not mobile_number):
				return JsonResponse({'error':'Mobile Number cannot be empty'})
			elif(not sim_id_one):
				return JsonResponse({'error':'Sim ID 1 cannot be empty'})
			elif(not user_type):
				return JsonResponse({'error':'User Type cannot be empty'})
			elif(not device_id):
				return JsonResponse({'error':'Device ID cannot be empty'})
			elif(not new_app_pass):
				return JsonResponse({'error':'New App Password cannot be empty'})
			elif(not confirm_new_app_pass):
				return JsonResponse({'error':'Confirm New App Password cannot be empty'})
			elif(new_app_pass != confirm_new_app_pass ):
				return JsonResponse({'error':'New App Password and Confirm New App Password not matched'})
			else:
				if(user_type=='101'):
					pocket_counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number)& Q(pocket_deviceid__exact=device_id) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) ).select_related()
					if(pocket_counter.count() == 0):
						return JsonResponse({'error':'Mobile Number not found'})
					else:
						#app pass update
						app_pass = dbEncrypt(new_app_pass)
						Pocket.objects.filter(customer_id__exact = user_id).update(pocket_app_pass = app_pass )
						return JsonResponse({'success':'true', 'msg':'Appp Password is updated.'})
				elif(user_type=='102'):
					cashbox_counter = Cashbox.objects.filter( Q(cashbox_mobileno__exact=mobile_number) & Q(cashbox_deviceid__exact=device_id) & Q(cashbox_simid__exact=sim_id_one)).select_related()
					if(cashbox_counter.count() == 0):
						return JsonResponse({'error':'Mobile Number is not found' })
					else:
						app_pass = dbEncrypt(new_app_pass)
						Cashbox.objects.filter(cashbox_customer_id__exact = user_id).update(cashbox_app_pass = app_pass ) 
						return JsonResponse({'success':'true' , 'msg':'App Password is  updated' })
				else:
					return JsonResponse({'error':'Invalid User Type'})

		except:
			return JsonResponse({'error':'Parameter Missing'})
	else:
		return JsonResponse({'error':'Please check your request method'})






#============================= Pocket QR Code PDF Version ===========================================
@csrf_exempt
def qrCodePdfVersion(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']


		print("%s %s"% (mobileno , user_type))

		if(not mobileno):
			return JsonResponse({'error':'Mobile Number Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Type Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			if(user_id ==0):
				return JsonResponse({'error':'Invalid Mobile Number'})
			else:
				if(user_type=='101'):
					pc = Pocket.objects.filter(Q(pocket_mobileno__exact = mobileno)  ).select_related()
					qr_Code = pc[0].pocket_qrcode
					qr_Code = qr_Code.replace(".png","")
					
					#pdf = FPDF()
					pdf = FPDF('P', 'mm', 'A4')
					# compression is not yet supported in py3k version
					pdf.compress = False
					pdf.add_page()

					# Unicode is not yet supported in the py3k version; use windows-1252 standard font
					pdf.set_font('Arial', '', 16)  
					pdf.ln(10)

					#pdf.write( 'hello world %s áó' % sys.version)
					pdf.write(10, 'Name : '+pc[0].pocket_fname+' '+ pc[0].pocket_mname +' '+pc[0].pocket_lname )
					pdf.ln(8)
					pdf.write(10, 'Mobile Number: '+ pc[0].pocket_mobileno)
					pdf.image("/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/"+ qr_Code+".png" , 0, 50)
					pdf.output('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+qr_Code+'.pdf', 'F')
					
					qr_pdf_url = "http://ec2-54-144-212-196.compute-1.amazonaws.com:80/profilepic/"+qr_Code+".pdf"

					return JsonResponse({'success':'true' , 'qr_pdf_url': qr_pdf_url })
				elif(user_type=='102'):
					cb = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobileno)  ).select_related()
					qr_code = cb[0].cashbox_qrcode
					qr_code = qr_code.replace(".png","")

					#pdf = FPDF()
					pdf = FPDF('P', 'mm', 'A4')
					# compression is not yet supported in py3k version
					pdf.compress = False
					pdf.add_page()
					# Unicode is not yet supported in the py3k version; use windows-1252 standard font
					pdf.set_font('Arial', '', 16)
					pdf.ln(10)

					pdf.write(10 , 'Name: '+ cb[0].cashbox_fname+' '+cb[0].cashbox_mname+' '+cb[0].cashbox_lname)
					pdf.ln(8)
					prf.write(10 , 'Mobile Number:'+ cb[0].cashbox_mobileno)

					#pdf.write(5, 'hello world %s Ã' % sys.version)
					pdf.image("/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/"+ qr_code+".png" , 0, 50)
					pdf.output('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+qr_code+'.pdf', 'F')

					
					qr_pdf_url = "http://ec2-54-144-212-196.compute-1.amazonaws.com:80/profilepic/"+qr_code+".pdf"

					return JsonResponse({'success':'true' , 'qr_pdf_url': qr_pdf_url })
				else:
					return JsonResponse({'error':'Please Check User Type'})		

	else:
		return JsonResponse({'error':'Please Check Your Request Method'})





#======= no need ============================================
@csrf_exempt
def transactionList(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']
		deviceid  = r['deviceid']
		simid_one = r['simid_one']
		simid_two = r['simid_two']
		transaction_pin = r['transaction_pin']
		transaction_type = r['transaction_type']
		search_type = r['search_type']

		if(not mobileno):
			return JsonResponse({'error': 'Mobile Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Tyepe Cannot be Empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid Cannot be Empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SimID One Cannot be Empty'})
		elif(not transaction_pin):
			return JsonResponse({'error':'Transaction PIN Cannot be Empty'})
		elif(not search_type):
			return JsonResponse({'error':'Search Type Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			if (user_id ==0):
				return JsonResponse({'error':'Please Check your Mobile Number'})
			else:
				request_date = date.today()
				if(search_type == '7'):
					one_week_date = date.today() - datetime.timedelta(days=7)
					transaction_list = CustomerTransaction.objects.filter(Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=(one_week_date,request_date)) ).select_related()
					return JsonResponse({'success':'true'})
				elif(search_type == '30'):
					one_month_date  = date.today() - datetime.timedelta(days=30)
					transaction_list = CustomerTransaction.objects.filter(Q(sender_userid__exact=user_id) | Q(receiver_userid__exact=user_id) & Q(transaction_timestamp__range=(one_week_date,request_date))).select_related()
					return JsonResponse({'status':'true' })
				else:
					return JsonResponse({'error':'Please Check Your Search Type'})
		
	else:
		return JsonResponse({'error':'Please Check Your Request'})






#======================== Pocket Balance ======================================
@csrf_exempt
def customerCurrentBalance(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']
		user_type = r['user_type']
		deviceid  = r['deviceid']
		simid_one     = r['simid_one']      
		simid_two = r['simid_two']
		transaction_pin = r['transaction_pin']

		if( not mobileno):
			return JsonResponse({'error': 'Mobile Cannot be Empty'})
		elif(not user_type):
			return JsonResponse({'error':'User Tyepe Cannot be Empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid Cannot be Empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SimID One Cannot be Empty'})
		#elif(not transaction_pin):
		#	return JsonResponse({'error': 'Transaction PIN Cannot be Empty'})
		else:
			user_id = getCustomerInfo(mobileno , user_type)
			user_det = CustomerAccounting.objects.filter( Q(userid__exact = user_id) ).select_related()
			customer_balance = decimal.Decimal(user_det[0].customer_balance)			

			return JsonResponse({'success':'true','current_balance': str(customer_balance)   })
	else:
		return JsonResponse({'error':'Please Check your request'})	







#========================= customer and merchant id =========================================================
def getCustomerInfo(mobile , customer_type):
	if (customer_type == '101'):
		pc = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile)  ).select_related()
		if ( pc.count() > 0):
			return pc[0].customer_id
		else:
			return 0
	else:
		ch = Cashbox.objects.filter(Q(cashbox_mobileno__exact = mobile)  ).select_related()
		if ( ch.count() > 0):
			return ch[0].cashbox_customer_id
		else:
			return 0
	




#=================== Pocket Cashout or Withdraw =====================================================
@csrf_exempt
def cashOutRequest(request):
	if request.method == 'POST' :

		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21806G') ).select_related()
		broken_key = hooker[0].broken_key
		
		try:

			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'] , broken_key )
			r = json.loads(param_data)
			sender_mobile = r['sender_mobile']
			sender_type = r['sender_type']
			receiver_mobile = r['receiver_mobile']
			receiver_type = r['receiver_type']
			cashout_amount = r['cashout_amount']
			transaction_pass = r['transaction_pass']
			withdraw_reference = r['withdraw_reference']


			if ( not sender_mobile):
				return JsonResponse({'error': 'Sender Mobile Cannot be Empty'})
			elif ( not receiver_mobile):
				return JsonResponse({'error': 'Receiver Mobile Cannot be Empty'})
			elif ( not cashout_amount):
				return JsonResponse({'error': 'Withdraw Amount Cannot be Empty'})
			elif ( receiver_type == '101'):
				return JsonResponse({'error': 'Pocket ot Pocket Withdraw Not Allow'})
			elif ( decimal.Decimal(cashout_amount) < 100 ):
				return JsonResponse({'error': 'Minimum Withdraw limit 100TK'})
			elif ( not transaction_pass ):
				return JsonResponse({'error': 'Transaction Password Cannot be Empty' })
			else:

				sender_info = getCustomerInfo(sender_mobile , sender_type)
				receiver_info = getCustomerInfo(receiver_mobile , receiver_type)
				print("%s , %s"% (sender_info,receiver_info))


				pocketCounter = Pocket.objects.filter(Q(customer_id__exact=sender_info) & Q(pocket_transaction_pass__exact=dbEncryptTwo(transaction_pass))).select_related()	

				if(sender_info == 0):
					return JsonResponse({'error':'Sender Mobile Not Found'})
				elif(pocketCounter.count() ==0 ):
					return JsonResponse({'error':'Wrong Transaction Password'})
				else:			
					if(receiver_info == 0):
						return JsonResponse({'error':'Receiver Mobile Not Found'})
					else:
						service_fee = (decimal.Decimal(cashout_amount)*1)/100;
						total = decimal.Decimal(cashout_amount)+decimal.Decimal(service_fee)

						print("fee:%s , total: %s"%(service_fee, total))
					
						cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)

						if(decimal.Decimal(sender_balance) > decimal.Decimal(total) ):
							ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
							receiver_balance_total = decimal.Decimal(ras[0].customer_balance) 
							blockCounter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=receiver_info) & Q(topup_transaction_status__exact='pending')).select_related()	
							if(blockCounter.count() > 0 ):						
								block_amount = blockCounter[0].topup_transaction_amount
							else:
								block_amount = 0



							receiver_balance = decimal.Decimal(receiver_balance_total) - decimal.Decimal(block_amount)
							sender_current_balance = decimal.Decimal(sender_balance)-decimal.Decimal(total)
							receiver_current_balance = decimal.Decimal(receiver_balance_total)+decimal.Decimal(total)
							print("%s , %s , %s , %s , %s"% (str(decimal.Decimal(total)), sender_balance , receiver_balance_total , sender_current_balance,receiver_current_balance ) )

							if(decimal.Decimal(cashout_amount) > decimal.Decimal(receiver_balance) ):
								return JsonResponse({'error':'Withdraw not occured.Drawer insufficient balance'})
							else:
								trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))


								ts = time.gmtime()
								currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)

								#Customer Transaction portion
								cts = CustomerTransaction()
								cts.customer_transaction_id = trx
								cts.sender_userid = sender_info
								cts.receiver_userid = receiver_info
								cts.reference_txt  = withdraw_reference
								cts.transaction_amount = decimal.Decimal(cashout_amount)
								cts.transaction_type = '10004'
								cts.transaction_message = 'TRXID: '+ str(trx) + ' Withdraw Amount:'+ str(decimal.Decimal(total)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
								cts.cashbox_business_id = ''
								cts.auxilary_userid = ''
								cts.referral_id = ''
								cts.transaction_status = ''
								cts.sender_longitute = ''
								cts.sender_latitude = ''
								cts.receiver_longitute = ''
								cts.receiver_latitude = ''				
								cts.transaction_timestamp = datetime.datetime.now()
								cts.save()						


								CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).update( customer_balance = sender_current_balance )
								CustomerAccounting.objects.filter( Q(userid__exact = receiver_info) ).update( customer_balance = receiver_current_balance )



								#notification
								sender_cust_detail = customerInfo(sender_info)
								receiver_cust_detail = customerInfo(receiver_info)
								if(sender_cust_detail == 0):
									sender_num = ''
									sender_photo = ''
								else:
									sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')


								if(receiver_cust_detail == 0):
									receiver_num = ''
									receiver_photo = ''
								else:
									receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')



								service_fee = service_fee
								#notification for merchant
								msg_title = "Withdraw Request."
								msg = str(cashout_amount) +"TK Withdraw request from:"+ sender_num 
								drawer_fcm_token = receiver_fcm_token
								sender_number = sender_num
								receiver_number = receiver_num
								photo = sender_photo
								transaction_type = "withdraw"
								log_view_type = "drawer"
								req_type = "req"											
								print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
								senderDet = customerNameDetailInfo(sender_info)
								sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
								cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, str(cashout_amount) , photo, service_fee,transaction_type, req_type,log_view_type,currentTime)


								#notification for customer
								receiverDet = customerNameDetailInfo(receiver_info)
								receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
								fcm_title = "Withdraw Request."

								sendTransactionNotification(1,sender_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(cashout_amount))+"TK" ,str(service_fee)+"TK","withdraw", currentTime  )

			

								return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee))+'','cashout_amount': str(decimal.Decimal(total))+'TK' , 'msg':'Withdraw Complete'})
						else:
							return JsonResponse({'error': 'Insufficient Balance.Current Balance:'+str(decimal.Decimal(sender_balance))+'TK'  })


		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})
	else:
		return JsonResponse({'error':'Please check your request method'})
			









#======================= TopUp or Deposit Request ==============================================
@csrf_exempt
def balanceTopUpWalletRequestToMerchant(request):
	if request.method  == 'POST' :
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21807H') ).select_related()
		broken_key = hooker[0].broken_key


		

		try:
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'] , broken_key )
			r = json.loads(param_data)

			sender_customer_mobile   = r['sender_mobile'].strip()
			receiver_customer_mobile = r['receiver_mobile'].strip()
			sender_customer_type     = r['sender_customer_type'].strip()
			receiver_customer_type   = r['receiver_customer_type'].strip()
			topup_amount             = r['topup_amount'].strip()
			deposit_reference        = r['deposit_reference'].strip()

			if(not sender_customer_mobile ):
				return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
			elif(not receiver_customer_mobile):
				return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
			elif(not sender_customer_type):
				return JsonResponse({'error' : 'Sender Customer Type not Empty'})
			elif(not receiver_customer_type):
				return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
			elif(not topup_amount):
				return JsonResponse({'error' : 'Deposit Amount not Empty'})
			else:
				sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
				receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )
				if ( sender_info != 0):
					if ( receiver_info != 0):
						cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)

						ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
						receiver_balance = decimal.Decimal(ras[0].customer_balance) 



						blockCounter = CustomerTopUpTransaction.objects.filter(Q(topup_receiver_userid__exact=receiver_info) & Q(topup_transaction_status__exact='pending')).select_related()	
						if (blockCounter.count() > 0):
							block_amount = blockCounter[0].topup_transaction_amount
						else:
							block_amount = 0



					
						counterPocket = Pocket.objects.filter(Q(customer_id__exact = receiver_info )).select_related()	
						drawer_balance = decimal.Decimal(receiver_balance )- decimal.Decimal(block_amount)									
						if (  decimal.Decimal(topup_amount) > decimal.Decimal( drawer_balance) ):
							return JsonResponse({'error': 'Insufficient Drawer Balance.'  })
						elif( counterPocket.count() > 0 ):
							return JsonResponse({'error' : 'Pocket to Pocket Deposit Request Not Allow' })
						elif ( decimal.Decimal(sender_balance) >  decimal.Decimal(topup_amount) ):
							sbal = decimal.Decimal(sender_balance) - decimal.Decimal(topup_amount)
							rbal = decimal.Decimal(receiver_balance) + decimal.Decimal(topup_amount)

							trx = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))

							service_fee = 0.00						
							ts = time.gmtime()
							currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)

							ct = CustomerTopUpTransaction()
							ct.topup_transaction_id = trx
							ct.topup_sender_userid   = sender_info
							ct.topup_receiver_userid = receiver_info
							ct.topup_reference_txt  = deposit_reference
							ct.topup_transaction_amount = decimal.Decimal(topup_amount)
							ct.topup_transaction_type = '10003'
							ct.topup_transaction_message = 'TRXID:'+trx+' Deposit Request Send.'
							ct.topup_cashbox_business_id =   ''
							ct.topup_auxilary_userid = ''  
							ct.topup_referral_id =  ''
							ct.topup_transaction_status = 'pending'
							ct.topup_transaction_initiator_request_timestamp =   currentTime   #datetime.datetime.now() 	
							ct.topup_transaction_completrion_request_timestamp = currentTime   #datetime.datetime.now()
							ct.save()			           


							#notification
							sender_cust_detail = customerInfo(sender_info)
							receiver_cust_detail = customerInfo(receiver_info)
							if(sender_cust_detail == 0):
								sender_num = ''
								sender_photo = ''
							else:
								sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')



							if(receiver_cust_detail == 0):
								receiver_num = ''
								receiver_photo = ''
							else:
								receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')



							#notification for merchant
							msg_title = "Deposit Request"
							msg = str(topup_amount) +"TK Deposit request from:"+ sender_num 
							drawer_fcm_token = receiver_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "deposit"
							log_view_type = "drawer"
							req_type = "req"											
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo(sender_info)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, topup_amount, photo, service_fee,transaction_type, req_type,log_view_type,currentTime)


							#notification for customer						
							receiverDet = customerNameDetailInfo(receiver_info)
							receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
							fcm_title = "Deposit Request Sent."
						
							sendTransactionNotification(1,sender_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(topup_amount))+"TK" ,str(service_fee)+"TK","deposit", currentTime  )


							return JsonResponse({ 'success' : 'true' , 'msg' : 'TRXID: '+ str(trx) +' Topup Request Send .' , 'service_fee': str(service_fee) })													
						else:
							return JsonResponse({'error': 'Block---4'})

					else:
						return JsonResponse({'error': 'Receiver Mobile Not Found'})
				else:
					return JsonResponse({'error': 'Sender Mobile Not Found'})

		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})
		
	else:
		return JsonResponse({'error': 'Check your request method'})






#==================== Pocket Payment ==================================================================
@csrf_exempt
def balancePay(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21805F') ).select_related()
		broken_key = hooker[0].broken_key


		try:
			params = json.loads(request.body)
			param_data = getDecryptedValue(params['data'] , broken_key )
			r = json.loads(param_data)
			sender_customer_mobile   = r['sender_mobile'].strip()
			receiver_customer_mobile = r['receiver_mobile'].strip()
			sender_customer_type     = r['sender_customer_type'].strip()
			receiver_customer_type   = r['receiver_customer_type'].strip()
			pay_amount               = r['pay_amount'].strip()
			transaction_pass         = r['transaction_pass'].strip()
			payment_reference        = r['payment_reference'].strip()

			if ( not sender_customer_mobile ):
				return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
			elif ( not receiver_customer_mobile ):
				return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
			elif ( not sender_customer_type ):
				return JsonResponse({'error' : 'Sender Customer Type not Empty'})
			elif ( not receiver_customer_type ):
				return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
			elif ( not pay_amount ):
				return JsonResponse({'error' : 'Pay Amount not Empty'})
			elif ( not transaction_pass ):
				return JsonResponse({'error' : 'Transaction Amount not Empty'})
			elif ( sender_customer_type == '102'):
				return JsonResponse({'error' : 'Sender Cannot be Merchant'})
			elif ( receiver_customer_type == '101' ):
				return JsonResponse({'error' : 'Pocket to Pocket Payment Not Allow'})
			else:
				sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
				receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )

				pocketCounter = Pocket.objects.filter(Q(customer_id__exact=sender_info) & Q(pocket_transaction_pass__exact=dbEncryptTwo(transaction_pass))).select_related()		
				print("%s %s"% (sender_info , dbEncryptTwo(transaction_pass)))
				if(sender_info==0):
					return JsonResponse({'error': 'Sender Mobile Number Not Found'})
				elif(pocketCounter.count() ==0):
					return JsonResponse({'error':'Wrong Transaction Password'})
				else:
					if(receiver_info==0):
						return JsonResponse({'error':'Receiver Mobile Number Not Found'})
					else:
						if(decimal.Decimal(pay_amount)<= 50):
							service_fee = 0
						elif(decimal.Decimal(pay_amount)>50 and decimal.Decimal(pay_amount)<=1000):
							service_fee = 2
						else:
							service_fee = 5



						cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_info) ).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)
					
						total = decimal.Decimal(pay_amount)+decimal.Decimal(service_fee)

						if(decimal.Decimal(sender_balance) < decimal.Decimal(total) ):
							return JsonResponse({'error':'Insufficient Balance.Current Balance:'+str(decimal.Decimal(sender_balance))+'TK' })
						else:
							ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_info ) ).select_related()
							receiver_balance = decimal.Decimal(ras[0].customer_balance) 

							sender_current_balance = decimal.Decimal(sender_balance)-decimal.Decimal(total)
							receiver_current_balance = decimal.Decimal(receiver_balance)+decimal.Decimal(total)					
							print("%s, %s , %s ,%s, %s"% (service_fee,sender_balance,total,sender_current_balance,receiver_current_balance) )
							trx =  ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))



							cts = CustomerTransaction()
							cts.customer_transaction_id = trx
							cts.sender_userid = sender_info
							cts.receiver_userid = receiver_info
							cts.reference_txt  = payment_reference
							cts.transaction_amount = decimal.Decimal(pay_amount)
							cts.transaction_type = '10002'
							cts.transaction_message = 'TRXID: '+ str(trx) + ' Pay amount:'+ str(decimal.Decimal(pay_amount)) +'TK. Current Balance:' + str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+str(decimal.Decimal(service_fee))+'TK'
							cts.cashbox_business_id = ''
							cts.auxilary_userid = ''
							cts.referral_id = ''
							cts.transaction_status = ''
							cts.sender_longitute = ''
							cts.sender_latitude = ''
							cts.receiver_longitute = ''
							cts.receiver_latitude = ''
							cts.transaction_timestamp = datetime.datetime.now()
							cts.save()


							CustomerAccounting.objects.filter( Q(userid__exact = sender_info)).update( customer_balance = sender_current_balance )
							CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)).update( customer_balance = receiver_current_balance )


							#notification start
							ts = time.gmtime()
							currentTime  = time.strftime("%Y-%m-%d %H:%M" ,ts)

							#notification
							sender_cust_detail = customerInfo(sender_info)
							receiver_cust_detail = customerInfo(receiver_info)
							if(sender_cust_detail == 0):
								sender_num = ''
								sender_photo = ''
							else:
								sender_num , sender_photo, sender_fcm_token = sender_cust_detail.split('|')



							if(receiver_cust_detail == 0):
								receiver_num = ''
								receiver_photo = ''
							else:
								receiver_num , receiver_photo, receiver_fcm_token  = receiver_cust_detail.split('|')


							#notification for merchant
							msg_title = "Payment Successfully"
							msg = str(pay_amount) +"TK Payment from:"+ sender_num 
							drawer_fcm_token = receiver_fcm_token
							sender_number = sender_num
							receiver_number = receiver_num
							photo = sender_photo
							transaction_type = "payment"
							log_view_type = "drawer"
							req_type = "trn"											
							print("drawer fcm:%s %s %s  %s %s"% (sender_fcm_token , receiver_fcm_token , sender_number,receiver_number,photo ))
							senderDet = customerNameDetailInfo( sender_info )
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token_id = senderDet.split('|')
							cashboxNotification(receiver_fcm_token , sender_full_name , msg, msg_title, trx , sender_number, receiver_number, str(pay_amount) , photo, str(service_fee),transaction_type, req_type,log_view_type,currentTime)


							#notification for customer
							receiverDet = customerNameDetailInfo(receiver_info)
							receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token_id =receiverDet.split('|')
							fcm_title = str(pay_amount) +"TK Payment To:"+ receiver_num


							sendTransactionNotification(1,sender_fcm_token_id,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(pay_amount))+"TK" ,str(service_fee)+"TK","payment", currentTime  )


						
							return JsonResponse({'success':'true','service_fee':str(decimal.Decimal(service_fee)),'msg':'TRXID:'+str(trx)+'.Pay Amount:'+ str(decimal.Decimal(total))+'TK.Current Balance:'+ str(decimal.Decimal(sender_current_balance))+'TK.Service Fee:'+ str(decimal.Decimal(service_fee))+'TK.'  })

		except:								
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameter'})
	else:
		return JsonResponse({'error':'Check your request method'})






def checkIsNumber(mobileno):
	counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobileno )).select_related()
	if(counter.count() <= 0):
		cnt = Cashbox.objects.filter(Q(cashbox_mobileno__exact=mobileno)).select_related()
		if(cnt.count() <= 0):
			return 0
		else:
			return str(cnt[0].cashbox_customer_id) +'|'+ str(cnt[0].customer_type_id)
	else:
		return str(counter[0].customer_id) +'|'+ str(counter[0].pocket_customer_type)	





#==================== Balance Transfer from contact list =========================================
@csrf_exempt
def balanceTransferFromContactListCheckNumber(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobileno  = r['mobile_number']	
		deviceid  = r['deviceid']
		simid_one = r['simid_one']      
		simid_two = r['simid_two']

		
		if(mobileno.find("-") != -1):
			mobileno = mobileno.replace("-","")
			if(mobileno.startswith("+880")):
				mobileno = "0"+ (mobileno.replace("+880","")).strip()
			elif(mobileno.startswith("880")):
				mobileno = "0"+ (mobileno.replace("880","")).strip()
			else:
				mobileno = mobileno
		else:
			if(mobileno.startswith("+880")):
				mobileno = "0"+ (mobileno.replace("+880","")).strip()
			elif(mobileno.startswith("880")):
				mobileno = "0"+ (mobileno.replace("880","")).strip()
			else:
				mobileno = mobileno


			
		print("mobileno: %s"% mobileno)
		
		if(not mobileno):
			return JsonResponse({'error':'Mobile number cannot be empty'})
		elif(not deviceid):
			return JsonResponse({'error':'Deviceid cannot be empty'})
		elif(not simid_one):
			return JsonResponse({'error':'SIM ID one cannot be empty'})
		else:			
			user_id = checkIsNumber(mobileno)
			print("UserID: %s"% user_id)
			if(user_id == 0):
				return JsonResponse({'error':'Not uummo registered user'})
			else:
				userID,user_type = user_id.split('|')
				print("%s %s"%(userID,user_type))

				if(user_type == '101'):
					p = Pocket.objects.filter(Q(customer_id__exact = userID )).select_related()
					mobile = p[0].pocket_mobileno
					fname = p[0].pocket_fname
					lname = p[0].pocket_lname
					mname = p[0].pocket_mname
					photo = p[0].pocket_photo
					userType = p[0].pocket_customer_type					
					qrcode = p[0].pocket_qrcode
				else:
					cs = Cashbox.objects.filter(Q(cashbox_customer_id__exact=userID)).select_related()
					mobile = cs[0].cashbox_mobileno
					fname = cs[0].cashbox_fname
					lname = cs[0].cashbox_lname
					mname = cs[0].cashbox_mname
					photo = cs[0].cashbox_photo
					userType = cs[0].customer_type_id
					qrcode = cs[0].cashbox_qrcode

				
				qrcode = qrcode.replace(".png","")						
				qrcode = qrcode[1:]
				return JsonResponse({'success':'true','customer_mobile': mobile , 'first_name': fname , 'last_name': lname , 'middle_name': mname , 'profile_image': photo , 'user_type': userType , 'msg': 'https://www.uumoo.biz/?'+ qrcode   })
	else:
		return JsonResponse({'error':'Please check your request method'})





#==============================pocket balnace transfer ======================================
@csrf_exempt
def balanceTransfer(request):
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21808I') ).select_related()
		broken_key = hooker[0].broken_key


		try:
			params = json.loads(request.body)		
			param_data = getDecryptedValue(params['data'] , BKEY)
			r = json.loads(param_data)
			sender_customer_mobile   = r['sender_mobile'].strip()
			receiver_customer_mobile = r['receiver_mobile'].strip()
			sender_customer_type     = r['sender_customer_type'].strip()
			receiver_customer_type   = r['receiver_customer_type'].strip()
			transfer_amount          = r['transfer_amount'].strip()
			transaction_pass         = r['transaction_pass'].strip()
			transfer_reference       = r['transfer_reference'].strip()

			if ( not sender_customer_mobile ):
				return JsonResponse({'error' : 'Sender Mobile Number not Empty'})
			elif ( not receiver_customer_mobile ):
				return JsonResponse({'error' : 'Receiver Mobile Number not Empty'})
			elif ( not sender_customer_type ):
				return JsonResponse({'error' : 'Sender Customer Type not Empty'})
			elif ( not receiver_customer_type ):
				return JsonResponse({'error' : 'Receiver Customer Type not Empty'})
			elif ( not transfer_amount ):
				return JsonResponse({'error' : 'Transfer Amount not Empty'})
			elif ( not transaction_pass ):
				return JsonResponse({'error' : 'Transaction Amount not Empty'})
			elif ( sender_customer_type == '102'):
				return JsonResponse({'error': 'Sender Mobile Cannot be Drawer'})
			elif ( receiver_customer_type == '102' ):
				return JsonResponse({'error':'Pocket cannot Transfer to Drawer'})
			else:
				sender_info = getCustomerInfo( sender_customer_mobile , sender_customer_type )
				receiver_info = getCustomerInfo( receiver_customer_mobile, receiver_customer_type )



				if ( sender_info != 0 ):
					if( receiver_info != 0 ):
						receiver_id = receiver_info
						sender_id = sender_info
						cas = CustomerAccounting.objects.filter( Q(userid__exact = sender_id) ).select_related()
						sender_balance = decimal.Decimal(cas[0].customer_balance)
						ras = CustomerAccounting.objects.filter( Q(userid__exact = receiver_id ) ).select_related()
						receiver_balance = decimal.Decimal(ras[0].customer_balance) 


						pocketCounter = Pocket.objects.filter(Q(customer_id__exact=sender_info) & Q(pocket_transaction_pass__exact=dbEncryptTwo(transaction_pass))).select_related()
					
						if(pocketCounter.count() == 0):
							return JsonResponse({'error':'Wrong Transaction Password'})
						elif ( decimal.Decimal(sender_balance) <  decimal.Decimal(transfer_amount) ):
							return JsonResponse({'error': 'Insufficient Balance.Your Current Balance:'+ str(cas[0].customer_balance)  })
						elif ( decimal.Decimal(sender_balance) >  decimal.Decimal(transfer_amount) ):
						
							service_fee = (decimal.Decimal(transfer_amount)* decimal.Decimal("0.5"))/100							
							sbal = decimal.Decimal(sender_balance) - (decimal.Decimal(transfer_amount)+decimal.Decimal(service_fee))
							CustomerAccounting.objects.filter( Q(userid__exact = sender_id) ).update( customer_balance = sbal )
						
							rbal = decimal.Decimal(receiver_balance) + decimal.Decimal(transfer_amount)
							CustomerAccounting.objects.filter( Q(userid__exact = receiver_info)  ).update(customer_balance = rbal )
							trx = ''.join(random.choice('0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz') for i in range(19))

							#Customer Transaction portion
							cts = CustomerTransaction()
							cts.customer_transaction_id = trx
							cts.sender_userid = sender_id
							cts.receiver_userid = receiver_id
							cts.reference_txt = transfer_reference
							cts.transaction_amount = decimal.Decimal(transfer_amount)
							cts.transaction_type = '10001'
							cts.transaction_message = 'TRXID: '+ str(trx) + ' Balance transfer amount: '+ str(transfer_amount) + 'TK. Current Balance: '+str(sbal)+'TK'
							cts.cashbox_business_id = ''
							cts.auxilary_userid = ''
							cts.referral_id = ''
							cts.transaction_status = ''
							cts.sender_longitute = ''
							cts.sender_latitude = ''
							cts.receiver_longitute = ''
							cts.receiver_latitude = ''
							cts.transaction_timestamp = datetime.datetime.now()
							cts.save()




							senderDet = customerNameDetailInfo(sender_id)
							sender_full_name,sender_photo,sender_mobile,sender_fcm_token= senderDet.split('|')
							receiverDet = customerNameDetailInfo(receiver_id)
							receiver_full_name,receiver_photo,receiver_mobile,receiver_fcm_token=receiverDet.split('|')
							fcm_title = "Transfer Successfull."
						
							print("%s %s %s %s"% (sender_fcm_token,receiver_fcm_token,sender_mobile,receiver_mobile))
							#sender
							sendTransactionNotification(1,sender_fcm_token,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(transfer_amount))+"TK" ,str(service_fee)+"TK","transfer",datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))

							#receiver
							sendTransactionNotification(0,receiver_fcm_token,fcm_title,trx ,sender_mobile,receiver_mobile,str(decimal.Decimal(transfer_amount))+"TK" ,str(service_fee)+"TK","transfer",datetime.datetime.now().strftime("%Y-%m-%d %H:%M"))



							return JsonResponse({ 'success' : 'true' ,'service_fee':str(decimal.Decimal(service_fee)) , 'msg' :'TRXID: '+  str(trx) +' Balance ' + str(transfer_amount) + 'BDT Transfered. Current Balance:'+ str(sbal) + 'TK' })	
						else:
							return JsonResponse({'error': 'Block---4'})
					else:
						return JsonResponse({'error': 'Receiver Mobile Number Not Exist'})
				else:
					return JsonResponse({'error': 'Sender Mobile Number Not Exist'})	
		except:
			changeAesKey()
			#return JsonResponse({'error':'Invalid Parameters'})
	else:
		return JsonResponse({'error':'Please check your request method'})

					


#========================pocket Nominee information for show into form===========================
@csrf_exempt
def pocketNomineeInfo(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		mobile_number = r['mobile_number'].strip()
		sim_id_one = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		device_id = r['device_id'].strip()
		
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = mobile_number) & (Q(pocket_simid__exact= sim_id_one) | Q(pocket_simid_two__exact = sim_id_two)) & Q(pocket_deviceid__exact= device_id)).select_related()
		if ( counter.count() > 0 ):

			#if ( "1111-11-11" not in (counter[0].nomineedobs)): dob = None
			#else:	dob = counter[0].nomineedobs


			return JsonResponse({'status':'true' , 'first_name': counter[0].pocket_nominee_fname , 'middle_name': counter[0].pocket_nominee_mname , 'last_name': counter[0].pocket_nominee_lname , 'dob': counter[0].nomineedobs , 'address': counter[0].pocket_nominee_address , 'contactno': counter[0].pocket_nominee_contactno , 'voter_id': counter[0].pocket_nominee_voterid , 'nid': counter[0].pocket_nominee_nid , 'relation': counter[0].pocket_nominee_relation , 'voter_or_nid_image': counter[0].pocket_nominee_voterid_scancopy  })
		else:
			return JsonResponse({'error': 'Mobile Number is Not Exist'})
	else:
		return JsonResponse({'error': 'Check your request method'})		



@csrf_exempt
def checkHeaderInfo1(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		data = r['data'].strip()
		key = 'faa9409d07cfce16592816bcba1545df'
		a = AESCipher(key)
		c = a.encrypt(data)

		#d = AESCipher(key)
		#dt = d.decrypt(c)
		
		return JsonResponse({'enc': str(c) })
	else:
		return JsonResponse({'error':'Please check your request method'})
		


@csrf_exempt
def checkHeaderInfo2(request):
	if request.method == 'POST':
		r = json.loads(request.body)
		data = r['data'].strip()
		key = 'faa9409d07cfce16592816bcba1545df'
		d = AESCipher(key)
		dt = d.decrypt(data)
		print(str(dt))		
		return JsonResponse({'dec': str(dt) })
	else:
		return JsonResponse({'error':'Please check your request method'})










#dummy cashbox entry
def dummyCashbox():
	ca = CustomerAccounting()
	ca.userid = '59136810201718556677598419'
	ca.user_type_id = '102'
	ca.customeraccount_transaction_type = 'pay'
	ca.customeraccount_debit = '0.00'
	ca.customeraccount_credit = '0.00'
	ca.comission_amount = '0.00'   #(ex:per transaction fee)   
	ca.commission_slave_amount = '0.00'
	ca.service_fee_amount = '0.00'
	ca.customer_referral_withdrawl_amount =  '0.00'
	ca.customer_balance = '0.00'
	ca.business_id = '2'
	ca.auxilary_accounting_userid = ''  #(comma separated value)
	ca.referral_id = ''  #(comma separated value)
	ca.save()

"""
	customerIDNum = '{0:06}'.format(random.randint(1,10000000))+'102'+'01718556677'+'{0:06}'.format(random.randint(1,1000000))
	customer_pin = '{0:06}'.format(random.randint(1,1000000)) + 'UUCAS'+'01718556677'+ '{0:06}'.format(random.randint(1,1000000))
	pocket_verification_pin = '{0:06}'.format(random.randint(1,1000000))
	salt_val = uuid.uuid4().hex
	qrCodeNo = hashlib.sha256( salt_val.encode() + pocket_verification_pin.encode() ).hexdigest()

	c = Cashbox()
	c.cashbox_customer_id = customerIDNum
	c.cashbox_id = customer_pin
	c.cashbox_fname = "ABC"
	c.cashbox_mname = "Modern"
	c.cashbox_lname = "House"
	c.cashbox_mobileno = "01718556677"
	c.cashbox_dob = '1987-08-05'
	c.cashbox_gender = 'Male'
	c.cashbox_nationalD = '62343805967-0837948384'
	c.cashbox_vodetid = '56636726777848389849'
	c.cashbox_photo = ''
	c.cashbox_access_token = 'hfska73872'
	c.cashbox_authorization_key = 'B3823694I9L878CYBDYB8FDQ2ZZLL2AQQEIF1T9'
	c.cashbox_deviceid = 'i89kh7373h8j893397f90'
	c.cashbox_simid = 'uy77389fh7338fh8890'
	c.cashbox_verified = '1'
	c.cashbox_app_pass = '3456789'
	c.cashbox_transaction_pass = '12344567'
	c.cashbox_qrcode = '0AK7ITWMR2HX8I2CJFGP2.png'
	c.cashbox_status_ative_frozen = '0'
	c.cashbox_business_id = '1'
	c.cashbox_refferedby_customer_id = ''
	c.customer_type_id = '102'
	c.cashbox_nominee_fname = 'abul'
	c.cashbox_nominee_mname = 'hasan'
	c.cashbox_nominee_lname = 'hasan'
	c.cashbox_nominee_address = 'Badda,Dhaka'
	c.cashbox_nominee_contactno = '01723456788'
	c.cashbox_nominee_relation = 'Cousin'
	c.cashbox_registration_timestamp = '2018-08-09 10:25:37.947243+00'
	c.save()
"""






#==========================sample qr===================================================
@csrf_exempt
def sampleQR(request):
	#dummyCashbox()
	
	qr = qrcode.QRCode(
		    version=6,
		    error_correction=qrcode.constants.ERROR_CORRECT_H,
		    box_size=10,
		    border=4,
	)
	qr.add_data('https://www.uumoo.biz/?AK7ITWMR2HX8I2CJFGP2')
	#1a36251df6f379cfcd198964122f1b6fec06950cc4c54bb9179b3955e583b145')   #AK7ITWMR2HX8I2CJFGP2')
	qr.make(fit=True)
	img = qr.make_image(fill_color="#000000", back_color="#ffffff")
	img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/test.png');
	im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/test.png')
	im = im.convert("RGBA")
	logo = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/uumoo_logo.png')
	box =(200,200,300,300)   #(250,250,350,350) #(200,200,270,270)   # (265,265,300,300)#(135,135,235,235)
	im.crop(box)
	region = logo
	region = region.resize((box[2] - box[0], box[3] - box[1]))
	im.paste(region,box)
	#im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/01a36251df6f379cfcd198964122f1b6fec06950cc4c54bb9179b3955e583b145.png')
	im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0AK7ITWMR2HX8I2CJFGP2.png');
	im.show()
	

	return JsonResponse({'msg': 'Sample QR'})



def iv():
	return chr(0) * 32



def dbDecrypt(enc_pass):
	con = psycopg2.connect(
		host = str(HOSTNAME), 
		database = str(DATABASE) ,
		user = str(USERNAME), 
		password= str(PASSWORD), 
		port = str(PORTNO) 
	)
	ukey = 'uumoo'
	curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	curr.execute("select convert_from(decrypt('"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
	two = curr.fetchone()
	data = str(two[0])
	curr.close()
	con.close()		
	return data
	

def dbDecryptTwo(enc_pass):
	con = psycopg2.connect(
		host = str(HOSTNAME), 
		database = str(DATABASE) ,
		user = str(USERNAME), 
		password= str(PASSWORD), 
		port = str(PORTNO) 
	)
	ukey = 'uumoo2'
	curr = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	curr.execute("select convert_from(decrypt('\\x"+ enc_pass +"','"+ ukey +"','bf'),'utf-8');")
	two = curr.fetchone()
	data = str(two[0])
	curr.close()
	con.close()
	return data




def dbEncrypt(user_str):
	con = psycopg2.connect(
		host = str(HOSTNAME) ,
		database = str(DATABASE), 
		user = str(USERNAME), 
		password= str(PASSWORD), 
		port = str(PORTNO) 
	)

	cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	pass_txt = user_str #request.GET.get("pass") #'12345'
	ukey = 'uumoo'
	rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
	one = cur.fetchone()	
	enc_pass = b"".join( one['pt'])
	cur.close()
	con.close()	
	print ("Enc: \\x%s" % str( binascii.b2a_hex( enc_pass), 'utf-8') )
	return "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8') 



def dbEncryptTwo(user_str):
	con = psycopg2.connect(
		host = str(HOSTNAME) , 
		database = str(DATABASE),
		user = str(USERNAME), 
		password= str(PASSWORD), 
		port = str(PORTNO)
	)

	cur = con.cursor(cursor_factory=psycopg2.extras.DictCursor)
	pass_txt = user_str 
	ukey = 'uumoo2'
	rw = cur.execute("select encrypt('"+pass_txt+"','"+ukey+"','bf') as pt;")
	one = cur.fetchone()
	enc_pass =  b"".join( one['pt'])
	cur.close()	
	con.close()	
	return "\\x"+ str( binascii.b2a_hex( enc_pass), 'utf-8')




#pocket nominee profileupdate ui
def pocketNomineeProfileUI(request):
	context = {}
	return render(request , "api/nominee_profile.html" , context)




#==================== Pocket Nominee Profile ==================================
@csrf_exempt
def pocketNomineeProfile(request):
	#dd = request.POST
	if request.method == 'POST':
		r = json.loads(request.body)

		pocket_mobileno = r["pocket_mobileno"].strip()
		pocket_nominee_fname = r["pocket_nominee_fname"].strip()
		pocket_nominee_mname = r["pocket_nominee_mname"].strip()
		pocket_nominee_lname = r["pocket_nominee_lname"].strip()
		nomineedobs = r["nomineedob"].strip()
		pocket_nominee_contactno = r["pocket_nominee_contactno"].strip()
		pocket_nominee_relation = r["pocket_nominee_relation"].strip()
		pocket_nominee_nid_voter = r["pocket_nominee_nid_voter"].strip()
		pocket_nominee_nid_or_voter = r["pocket_nominee_nid_or_voter"].strip()
		pocket_nominee_address = r['pocket_nominee_address'].strip()		

	
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()
	
	

	
		pocket_nominee_nid = None
		pocket_nominee_voterlD = None
		if  pocket_nominee_nid_or_voter == 'NID':
			pocket_nominee_nid = pocket_nominee_nid_voter
			pocket_nominee_voterlD = ""
		else:
			pocket_nominee_voterlD = pocket_nominee_nid_voter
			pocket_nominee_nid = ""






		if (not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			if ( counter.count() > 0 ):
				customer_id = counter[0].customer_id
	

				
				if (pocket_nominee_fname is not None and pocket_nominee_fname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_fname = pocket_nominee_fname )
				if (pocket_nominee_mname is not None and pocket_nominee_mname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_mname = pocket_nominee_mname )
				if (pocket_nominee_lname is not None and pocket_nominee_lname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_lname = pocket_nominee_lname )
				if (nomineedobs is not None and nomineedobs != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(nomineedobs = nomineedobs )
				if (pocket_nominee_relation is not None and pocket_nominee_relation != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_relation= pocket_nominee_relation)
				if (pocket_nominee_nid is not None and pocket_nominee_nid !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_nid = pocket_nominee_nid )
				if (pocket_nominee_voterlD is not None and pocket_nominee_voterlD !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_voterid = pocket_nominee_voterlD)
				if (pocket_nominee_contactno is not None and pocket_nominee_contactno != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_contactno=pocket_nominee_contactno)
				if (pocket_nominee_address is not None and pocket_nominee_address != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_address=pocket_nominee_address)
				
	


				msg_body = "Nominee Information  update complete"
				title = "Profile Update"
				fcm_token = counter[0].pocket_fcm_token
				msg_date = date.today().strftime('%A, %d %B %Y')
				custom_date = date.today().strftime('%Y-%m-%d')
				msg_time = time.strftime('%I:%M %p')
				print("%s"% fcm_token )
				if(fcm_token != ""):
					sendCommonNotification(fcm_token , title , msg_body , "sys" , msg_date ,custom_date , msg_time ,"01300000000" , "" )
				



				return JsonResponse({'status':'true' ,'msg':'Nominee information updated.' })
			else:
				return JsonResponse({'msg':'Please check Mobileno,DeviceID,SIMID'})			
	else:
		return JsonResponse({'error':'check your request method'})



#==============Pocket qr-code scan and ================================================
@csrf_exempt
def qrCodeScan(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobile = r['mobile_number'].strip()
		sim_id_one  = r['sim_id_one'].strip()
		sim_id_two = r['sim_id_two'].strip()
		sender_user_type = r['sender_user_type'].strip()
		device_id = r['device_id'].strip()
		qr_token = '0' + r['qr_token'].strip() +'.png'

	
		counter = Pocket.objects.filter(Q(pocket_qrcode__exact = qr_token )).select_related()
		if (counter.count() > 0 ):
			return JsonResponse({'success':'true' , 'customer_mobile': counter[0].pocket_mobileno , 'first_name': counter[0].pocket_fname , 'last_name': counter[0].pocket_lname , 'middle_name': counter[0].pocket_mname , 'profile_image': counter[0].pocket_photo , 'user_type': counter[0].pocket_customer_type  })
		
		else:
			cash = Cashbox.objects.filter(Q(cashbox_qrcode__exact = qr_token )).select_related()
			if ( cash.count() > 0 ):
				mob = cash[0].cashbox_mobileno
				pro_img = cash[0].cashbox_photo
				bcount = Business.objects.filter(Q(app_mobile_number__exact=mob)).select_related()
				if(bcount.count() > 0):
					if(bcount[0].business_name == ''):
						fname = cash[0].cashbox_fname
						lname = cash[0].cashbox_lname
						mname = cash[0].cashbox_mname
					else:
						fname = bcount[0].business_name
						lname = ''
						mname = ''


					if(bcount[0].business_logo == ''):
						logo = cash[0].cashbox_photo
					else:
						logo = bcount[0].business_logo
				else:
					fname = cash[0].cashbox_fname
					lname = cash[0].cashbox_lname
					mname = cash[0].cashbox_mname
					logo = cash[0].cashbox_photo



				return JsonResponse({'success':'true' , 'customer_mobile': cash[0].cashbox_mobileno , 'first_name': fname , 'last_name': lname , 'middle_name': mname , 'profile_image': logo , 'user_type': cash[0].customer_type_id   })
				
			else:
				return JsonResponse({'url':'https://www.uumoo.biz/'})  
	else:
		return JsonResponse({'error':'Check your request method.'})
		



#========================== Pocket Registration ==========================================
@csrf_exempt
def pocketSignUp(request):
	if request.method == 'POST':
		r = json.loads(request.body)

		first_name = r["pocket_fname"]
		last_name = r["pocket_lname"]		
		pocket_mobileno = r["pocket_mobileno"]
		pocket_dob = r["pocket_dob"]	
		pocket_deviceid = r["pocket_deviceid"]
		pocket_simid = r["pocket_simid"]
		pocket_simid_two = r["pocket_simid_two"]
		pocket_app_pass = r["pocket_app_pass"]
		pocket_app_retype_pass = r["pocket_app_retype_pass"]		
		pocket_fcm_token = r["pocket_fcm_token"]
		pocket_verified = 1
	               	
		accessToken = '{0:05}'.format(random.randint(2,100000))
		salt = uuid.uuid4().hex
		pocket_access_token =  hashlib.sha256(salt.encode() + accessToken.encode()).hexdigest()
		
		#'{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		pocket_authorization_key =  '{0:05}'.format(random.randint(2,100000))+''+'{0:05}'.format(random.randint(2,100000))
		nomineedobs = "1111-11-11"

		counter = Pocket.objects.filter(pocket_mobileno__exact = pocket_mobileno).count()

		if counter >= 1:
			return JsonResponse({'error':'Mobile Number already exist.'})	
		elif (not pocket_app_pass):
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif len(pocket_app_pass) < 6:
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif len(pocket_app_pass) > 32:
			return JsonResponse({'error':'Password minimum 6 digit.'})
		elif (last_name == None):
			return JsonResponse({'error':'Last Name Required.'})
		elif (pocket_mobileno == None):
			return JsonResponse({'error':'Mobile Number Reqired.'})
		elif (pocket_deviceid == None):
			return JsonResponse({'error':'Device ID Reqired.'})
		elif (pocket_simid == None):
			return JsonResponse({'error':'SIM ID Required.' })
		elif (pocket_dob == None):
			return JsonResponse({'error':'Date of Birth Required.'})
		elif (pocket_app_retype_pass == None):
			return JsonResponse({'error':'Retype Password Required.'})
		elif (pocket_app_retype_pass != pocket_app_pass):
			return JsonResponse({'error':'Retype and Password Not Matched.'})
		else:
			customerIDNum = '{0:06}'.format(random.randint(1,10000000))+'101'+pocket_mobileno+'{0:06}'.format(random.randint(1,1000000))
			customer_pin = '{0:06}'.format(random.randint(1,1000000)) + 'UUPOC'+pocket_mobileno+ '{0:06}'.format(random.randint(1,1000000))
			pocket_verification_pin = '{0:06}'.format(random.randint(1,1000000))



			salt_val = uuid.uuid4().hex
			qrCodeNo = hashlib.sha256( salt_val.encode() + pocket_verification_pin.encode() ).hexdigest()

			#print customerIDNum
			p = Pocket()
			p.customer_id = customerIDNum			
			p.pocket_id = customer_pin
			p.pocket_fname = first_name 
			p.pocket_lname = last_name
			p.pocket_mobileno = pocket_mobileno
			p.pocket_dob = pocket_dob
			p.pocket_access_token = pocket_access_token
			p.pocket_authorization_key = pocket_authorization_key
			p.pocket_deviceid = pocket_deviceid
			p.pocket_simid = pocket_simid
			p.pocket_simid_two = pocket_simid_two
			p.pocket_app_pass = dbEncrypt( pocket_app_pass)
			p.pocket_verified = '1'
			p.pocket_verification_pin = pocket_verification_pin
			p.pocket_pin_verified_flag = '0'
			p.pocket_agree_flag = '0'
			p.nomineedobs = nomineedobs
			p.pocket_qrcode = '0' + qrCodeNo +'.png' 
			p.pocket_customer_type = '101'
			p.pocket_fcm_token = pocket_fcm_token
			p.save()


			

			#qr_content = p.pocket_fname+' '+p.pocket_lname+ ' '+p.pocket_mobileno+' '+p.customer_id		
			salt = uuid.uuid4().hex
			qr_content = "https://www.uumoo.biz/?" + qrCodeNo  
			qr = qrcode.QRCode(
				version=6,
				error_correction=qrcode.constants.ERROR_CORRECT_H,
				box_size=10,
				border=4,
			)
			qr.add_data(qr_content)
			qr.make(fit=True)
			img = qr.make_image(fill_color="#000000", back_color="#ffffff")
			img.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png');
			im = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'+ qrCodeNo +'.png')
			im = im.convert("RGBA")
			logo = Image.open('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/uumoo_logo.png') 
			box = (250,250,350,350) #tested middle position and black background logo
			#box = (200,200,270,270) (270,270,380,380) (135,135,235,235)
			im.crop(box)
			region = logo
			region = region.resize((box[2] - box[0], box[3] - box[1]))
			im.paste(region,box)
			im.save('/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/0'+ qrCodeNo  +'.png')
			im.show()

			#q = pyqrcode.create(str(qr_content));
			#q.png('profilepic/'+ str(pocket_verification_pin) +'.png' , scale=10)
			#print('QR Code Generated')
			#img.save('/home/uumoogrid/uumoogrid/profilepic/'+ str(pocket_verification_pin) +'.png')
				
		
			ca_counter = CustomerAccounting.objects.filter(Q(userid__exact = p.customer_id )  ).select_related()
			if ( ca_counter.count() <= 0 ):
				ca = CustomerAccounting()	
				ca.userid = p.customer_id
				ca.user_type_id = '101'
				ca.customeraccount_transaction_type = ''
				ca.customeraccount_debit = 0.00
				ca.customeraccount_credit = 0.00
				ca.comission_amount = 0.00
				ca.commission_slave_amount = 0.00
				ca.service_fee_amount = 0.00
				ca.customer_referral_withdrawl_amount = 0.00
				ca.customer_balance = 2000.00
				ca.business_id = ''
				ca.auxilary_accounting_userid = ''
				ca.referral_id = ''
				ca.save()




			#sms sending
			#sms_url = "http://api.zaman-it.com/api/sendsms/plain?user=01720208362&password=d01720208362&sender=Friend&SMSText=Your Pocket Verification Code:" + str(pocket_verification_pin) +"&GSM=88"+ str(pocket_mobileno) +""
			#resp = requests.get(sms_url)
			#print resp.content

 
		return JsonResponse({'success':'true','msg':'Pocket Registraion Finished.','verification_pin': pocket_verification_pin })
	else:
		return JsonResponse({'error':'check your request method'})
		







#pin verification api test
@csrf_exempt
def pocketPinVerification(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)
	
		pocket_mobileno = r['pocket_mobileno']
		pocket_deviceid = r['pocket_deviceid']
		pocket_simid = r['pocket_simid']
		pocket_verification_code = r['pocket_verification_code']

		#cond1 = Q(pocket_mobileno__contains = pocket_mobileno)
		#cond2 = Q(pocket_deviceid__contains = pocket_deviceid)
		#cond3 = Q(pocket_simid__contains = pocket_simid)
		#cond4 = Q(pocket_verification_pin__contains = pocket_verification_code)	
		#counter = Pocket.objects.filter(cond1 & cond2 & cond3 & cond4).count()

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_deviceid__exact = pocket_deviceid) & Q(pocket_simid__exact = pocket_simid) & Q(pocket_verification_pin__exact = pocket_verification_code)).select_related()
	

		if counter.count() > 0 : 
			customerID = counter[0].customer_id
			Pocket.objects.filter(Q(customer_id__exact=customerID)).update(pocket_pin_verified_flag='1',pocket_agree_flag='1')
			return JsonResponse({'success':'true' , 'msg': 'PIN Verified Successfully.' })
		elif(not pocket_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not pocket_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not pocket_simid):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not pocket_verification_code):
			return JsonResponse({'error': 'Verification PIN Required.'})
		else:		
			return JsonResponse({'error': 'Wrong pin verification.'})
	else:
		return JsonResponse({'error':'check your request method'})



	
#transaction pin verification api test
@csrf_exempt
def pocketTransactionPinVerification(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobileno = r['pocket_mobileno']
		pocket_deviceid = r['pocket_deviceid']
		pocket_simid = r['pocket_simid']
		pocket_transaction_pin = r['pocket_transaction_pin']
		pocket_confirm_transaction_pin = r['pocket_confirm_transaction_pin']
        
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_deviceid__exact = pocket_deviceid) & Q(pocket_simid__exact = pocket_simid) ).select_related()

	
		if counter.count() <= 0 :
			return JsonResponse({'error': 'Mobile Number Not Exist.' })
		elif(not pocket_mobileno):
			return JsonResponse({'error': 'Mobile Number Required.'})
		elif(not pocket_deviceid):
			return JsonResponse({'error': 'Device ID Required.'})
		elif(not pocket_simid):
			return JsonResponse({'error': 'SIM ID Required.'})
		elif(not pocket_transaction_pin):
			return JsonResponse({'error': 'Transaction PIN Required.'})
		elif(not pocket_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction Confirm PIN Required.'})
		elif(pocket_transaction_pin != pocket_confirm_transaction_pin):
			return JsonResponse({'error': 'Transaction and  Confirm PIN Not Matched.'})
		else:             
			customer_id = counter[0].customer_id
			transaction_pass = dbEncryptTwo(pocket_transaction_pin)
			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_transaction_pass = transaction_pass)

			return JsonResponse({'success':'true', 'msg': 'Your Transaction PIN updated successfullly.'  })

	else:
		return JsonResponse({'error':'check your request method'})




#pocket login api test
@csrf_exempt
def pocketLogin(request):
	context = {}
	if request.method == 'POST':
		hooker = APIAesDetails.objects.filter(Q(admin_id__exact = '111') & Q(aescode__exact ='21800A') ).select_related()		
		broken_key = hooker[0].broken_key
		#print(broken_key)


		try:
			params = json.loads(request.body)
			#param_data = getDecryptedValue(params['data'], BKEY)
			param_data = getDecryptedValue(params['data'], broken_key )
			r = json.loads(param_data)
			pocket_mobileno = r['pocket_mobileno']
			pocket_deviceid = r['pocket_deviceid']
			pocket_simid    = r['pocket_simid']      
			pocket_app_pass = dbEncrypt(r['pocket_app_pass'])
			pocket_fcm_token = r['pocket_fcm_token']
			pocket_status_active_frozen = '0'
		

			counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) & Q(pocket_simid__exact = pocket_simid) & Q(pocket_app_pass__exact = pocket_app_pass)  ).select_related()


			if(pocket_fcm_token is not None and pocket_fcm_token != ""):
				Pocket.objects.filter(Q(pocket_mobileno__exact=pocket_mobileno)).update(pocket_fcm_token=pocket_fcm_token)




			if(not pocket_mobileno):
				return JsonResponse({'error': 'Mobile Number Required.'})
			elif(not pocket_deviceid):
				return JsonResponse({'error': 'Device ID Required.'})
			elif(not pocket_simid):
				return JsonResponse({'error': 'SIM ID Required.'})
			elif(not pocket_app_pass):
				return JsonResponse({'error': 'Password Required.'})
			else:
				if(counter.count() <= 0):
					return JsonResponse({'error': 'Please check your username and password.' })
				else:
					if(counter[0].pocket_status_active_frozen == '0'):
						return JsonResponse({'error': 'Account freeze' })
					else: 	

						permanent_info = PocketAddress.objects.filter(Q(address_id__exact = counter[0].pocket_permanent_address_id) ).select_related()
						if(permanent_info.count() >0):
							permanent_address = '{"id":"'+ str(permanent_info[0].address_id) +'","address":"'+ str(permanent_info[0].reg_address) +'" ,"division":"'+ str(permanent_info[0].division) +'","thana":"'+ str(permanent_info[0].thana) +'" ,"district":"'+ str(permanent_info[0].district) +'","postcode":"'+ str(permanent_info[0].postcode) +'" , "longitude":"'+ str( permanent_info[0].longitude) +'", "latitude":"'+ str(permanent_info[0].latitude) +'" }'
							#permanent_address = {'id': permanent_info[0].address_id ,	'address': permanent_info[0].reg_address ,'division': permanent_info[0].division ,'thana': permanent_info[0].thana ,'district': permanent_info[0].district ,'postcode': permanent_info[0].postcode , 'longitude': permanent_info[0].longitude , 'latitude': permanent_info[0].latitude }
						else:
							permanent_address = '{"id": "" ,"address": "" ,"division": "" , "thana": "" ,"district": "" , "postcode": "" , "longitude": "" , "latitude": "" }'
							#permanent_address = {'id': '' ,'address': '' ,'division': '' , 'thana': '' ,'district': '' , 'postcode': '' , 'longitude': '' , 'latitude': '' }



						present_info = PocketAddress.objects.filter(Q(address_id__exact = counter[0].pocket_present_address_id)).select_related()
						if(present_info.count() > 0 ):
							present_address = '{"id":"'+ str(present_info[0].address_id) +'" ,"address":"'+ str(present_info[0].reg_address) +'" ,"division":"'+ str(present_info[0].division) + '","thana":"'+ str( present_info[0].thana) +'" ,"district":"'+ str(present_info[0].district) +'" , "postcode":"'+ str(present_info[0].postcode) +'" , "longitude":"'+ str( present_info[0].longitude) +'" ,"latitude":"'+ str(present_info[0].latitude) +'"  }'
							#present_address = {'id': present_info[0].address_id ,'address': present_info[0].reg_address ,'division': present_info[0].division ,'thana': present_info[0].thana ,'district': present_info[0].district , 'postcode': present_info[0].postcode , 'longitude': present_info[0].longitude , 'latitude': present_info[0].latitude  }
						else:
							present_address = '{"id": "" ,"address": "", "division": "", "thana": "" , "district": "","postcode": "" ,"longitude": "" ,"latitude": "" }'
							#present_address = {'id': '' ,'address': '', 'division': '', 'thana': '' , 'district': '' ,'postcode': '' ,'longitude': '' ,'latitude': '' }




						if(counter[0].pocket_pin_verified_flag == '0'):
							return JsonResponse({'success':'true','pin_verified_flag':'0'})
						elif(counter[0].pocket_agree_flag == '0'):
							return JsonResponse({'success':'true','agree_verified_flag':'0'})
						elif(counter[0].pocket_transaction_pass == ''):
							return JsonResponse({'success':'true','trans_verified_flag':'0'})
						else:
							deviceCounter = Pocket.objects.filter(Q(pocket_deviceid__exact=pocket_deviceid)).select_related()
							if(deviceCounter.count() == 0 ):
								Pocket.objects.filter(Q(customer_id__exact=counter[0].customer_id)).update(pocket_deviceid=pocket_deviceid)
								warning_msg = "You are Logged in by Device"
							else:
								warning_msg = ""



							resp = '{"user_type":"'+ str(counter[0].pocket_customer_type) +'" ,"mobile_number" :"'+ str(counter[0].pocket_mobileno) +'" ,"device_id" :"'+ str(counter[0].pocket_deviceid) +'", "sim_id" :"'+ str(counter[0].pocket_simid) +'" ,"sim_id_two":"'+ str(counter[0].pocket_simid_two) +'" , "access_token" :"'+ str(counter[0].pocket_access_token) +'" , "authorization_key" :"'+ str(counter[0].pocket_authorization_key) +'","fname":"'+ str(counter[0].pocket_fname) +'" ,"mname":"'+ str(counter[0].pocket_mname) +'" ,"lname":"'+ str(counter[0].pocket_lname) +'", "dob":"'+ str(counter[0].pocket_dob) + '" ,"gender":"'+ str(counter[0].pocket_gender) +'" ,"vid":"'+ str(counter[0].pocket_voterlD) +'" ,"nid":"'+ str(counter[0].pocket_nid) + '" ,"email":"'+ str(counter[0].pocket_email) +'","alternate_mobile":"'+ str(counter[0].pocket_alternate_mobileno) +'","photo":"'+ str(counter[0].pocket_photo) + '","nid_image":"'+ str(counter[0].pocket_nid_image) +'" ,"qrcode":"'+ str(counter[0].pocket_qrcode) +'" ,"nominee_fname":"'+ str(counter[0].pocket_nominee_fname) +'" ,"nominee_mname":"'+ str(counter[0].pocket_nominee_mname) +'" ,"nominee_lname":"'+ str(counter[0].pocket_nominee_lname) +'", "nominee_contactno":"'+ str(counter[0].pocket_nominee_contactno) +'" ,"nominee_voterid":"'+ str(counter[0].pocket_nominee_voterid) +'","nominee_nid":"'+ str(counter[0].pocket_nominee_nid) +'","nominee_relation":"'+ str(counter[0].pocket_nominee_relation) +'","nominee_voterid_image":"'+ str(counter[0].pocket_nominee_voterid_scancopy) +'","permanent_address": '+ permanent_address +', "present_address":'+ present_address +'}'

							payload_msg = getEncryptedValue(resp , broken_key )
							print(resp)
							print(payload_msg)

							return JsonResponse({'success':'true', 'msg': 'Welcome to UUMOO Pocket.','warning':warning_msg, 'payload': payload_msg })



		except:
			changeAesKey()
			#return JsonResponse({'error':'Encryption Failed'})			



	else:
		return JsonResponse({'error':'check your request method'})





#illegal login attempt consequently 3 times wrong password
@csrf_exempt
def threeTimeWrongPassword(request):

	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_mobileno = r['pocket_mobile']

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

		if (counter.count() > 0 ):		
			Pocket.objects.filter(customer_id__exact = counter[0].customer_id ).update(pocket_status_active_frozen  = '1' )
			return JsonResponse({'success':'true' , 'msg':'Acount deactivated successfully.'})
		else:
			return JsonResponse({'error':'Mobile Number not Exist'})
	else:
		return JsonResponse({'error':'Please Check your request method'})




#for file upload function

def handle_uploaded_file(file, filename):
    if not os.path.exists('profilepic/'):
        os.mkdir('profilepic/')
 
    with open('profilepic/' + filename, 'wb+') as destination:
        for chunk in file.chunks():
            destination.write(chunk)



#common file upload method
@csrf_exempt
def imageFilesUpload(request):
	#r = requests.post(url, files=files)
	file = request.FILES #['file']

	url = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'#'http://httpbin.org/post'
	files = {'file': one(file , 'rb')}
	values = {'author': 'John Smith'}
	requests.post(url, files=files, data=values)
	#filename = file.name

	#handle_uploaded_file(file, filename):


@csrf_exempt
def imageFileseInformationUpdate(request):
	if request.method == 'POST':
		#r = json.loads(request.body)
		customer_mobile = request.POST.get('customer_mobile')	
		image_type = request.POST.get('image_type')
		pocket_image = request.FILES['avatar'].name  #r['pocket_image')
		extension = pocket_image.split(".")[-1]
	

		if(not customer_mobile):
			return JsonResponse({'error':'Mobile Number must not be Empty.'})	
		elif(not image_type):
			return JsonResponse({'error':'Image Type must not be Empty.'})
		elif(not pocket_image):
			return JsonResponse({'error':'Image must not be Empty.'})
		else:
			token_generate = '{0:06}'.format(random.randint(1,10000000))			
			salt = uuid.uuid4().hex
			img =  hashlib.sha256(salt.encode() + token_generate.encode()).hexdigest()+'.'+extension
			handle_uploaded_file(request.FILES['avatar'] , img)

			counter = Pocket.objects.filter(pocket_mobileno__exact = customer_mobile ).select_related()

			if(counter.count() > 0):
				customer_id = counter[0].customer_id
				if(image_type == 'profile_image'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_photo=img)
				elif(image_type == 'nid'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nid_image=img)
				elif(image_type == 'nominee_nid'):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nominee_voterid_scancopy=img)
				else:
					return JsonResponse({'error':'Image type error'})


				
				return JsonResponse({'status':'true','filename': img , 'msg': 'Information updated successfully.'})
			else:
				return JsonResponse({'error':'Mobile Number Not Exist'})

	else:
		return JsonResponse({'error':'check your request method'})


#pocket profile api test
@csrf_exempt
def pocketBasicProfile(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)
		pocket_fname = r['pocket_fname']
		pocket_mname = r['pocket_mname']
		pocket_lname = r['pocket_lname']
		pocket_gender = r['pocket_gender']
		pocket_alternate_mobileno = r['pocket_alternate_mobileno']

		pocket_email = r['pocket_email']
		pocket_mobileno = r['pocket_mobileno']
	        
		pocket_nid_voter = r['pocket_nid_voter']
		pocket_nid_or_voter = r['pocket_nid_or_voter']
	
		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

		#profile_photo = '{0}_{1}'.format("100x100" , request.FILES['pocket_photo'])
		#handle_uploaded_file( File(profile_photo) , "100x100_"+ str(request.FILES['pocket_photo']) )
	
		pocket_nid = None
		pocket_voterlD = None 
		if pocket_nid_or_voter == 'NID':
			pocket_nid =  pocket_nid_voter
			pocket_voterlD = ""
		else:
			pocket_voterlD = pocket_nid_voter
			poket_nid = ""

		counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno)  ).select_related()

	        #profile_photo = '{0}_{1}'.format("100x100" , request.FILES['pocket_photo'])
	        #handle_uploaded_file( File(profile_photo) , "100x100_"+ str(request.FILES['pocket_photo']) )

		if(not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			if(counter.count() > 0 ):
				customer_id = counter[0].customer_id
				if(pocket_fname is not None and pocket_fname != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_fname = pocket_fname )
				if(pocket_mname is not  None and pocket_mname !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_mname = pocket_mname )
				if(pocket_lname is not None and pocket_lname!=''):	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_lname = pocket_lname )
				if(pocket_gender is not None and pocket_gender!=''):	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_gender = pocket_gender )
				if(pocket_nid is not None and pocket_nid !=''): 	
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_nid = pocket_nid )
				if(pocket_voterlD is not None and pocket_voterlD !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_voterlD = pocket_voterlD )
				if(pocket_alternate_mobileno is not None and pocket_alternate_mobileno != ''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_alternate_mobileno = pocket_alternate_mobileno )
				if(pocket_email is not None and pocket_email !=''):
					Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_email = pocket_email )
		    	

				#if (request.FILES['pocket_photo'] is not None and request.FILES['pocket_photo'] != ''):	
				#handle_uploaded_file( request.FILES['pocket_photo'] , request.FILES['pocket_photo'].name )
				#image1 = Image.open("/home/ec2-user/uumoogrid/uumoogrid/profilepic/"+ request.FILES['pocket_photo'].name)
				#new_image1 = image1.resize((100, 100))
				#new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/100x100_'+request.FILES['pocket_photo'].name)
                        	#new_image1 = image1.resize((40, 40))
                        	#new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/40x40_'+request.FILES['pocket_photo'].name)
				#new_image1 = image1.resize((25, 25))                        
		                #new_image1.save('/home/ec2-user/uumoogrid/uumoogrid/profilepic/25x25_'+request.FILES['pocket_photo'].name)
				#Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_photo = request.FILES['pocket_photo'].name )

				msg_body = "Profile update complete"
				title = "Profile Update"		
				fcm_token = counter[0].pocket_fcm_token
				msg_date = date.today().strftime('%A, %d %B %Y')
				custom_date = date.today().strftime('%Y-%m-%d')
				msg_time = time.strftime('%I:%M %p')
				print("%s"% fcm_token )
				if(fcm_token != ""):	
					sendCommonNotification(fcm_token , title , msg_body , "sys" , msg_date ,custom_date , msg_time,"01300000000" , "" )




				return JsonResponse({'msg':'profile update complete'})
			else:
				return JsonResponse({'msg':'Please check Mobileno,DeviceID,SIMID'})
	else:
		return JsonResponse({'error':'check your request method'})




#pocket address profile update api test
@csrf_exempt
def pocketAddressProfile(request):
	context = {}
	if request.method == 'POST':
		r = json.loads(request.body)

		permanent_address = r["permanent_address"]
		permanent_division = r["permanent_division"]
		permanent_district = r["permanent_district"]
		permanent_thana = r["permanent_thana"]
		permanent_postcode = r["permanent_postcode"]

		pocket_lat = r["pocket_lat"]
		pocket_lon = r["pocket_lon"]

		present_address = r["present_address"]
		present_division = r["present_division"]
		present_district = r["present_district"]
		present_thana  = r["present_thana"]
		present_postcode = r["present_postcode"]

		pocket_mobileno = r['pocket_mobileno']

		if(not pocket_mobileno):
			return JsonResponse({'msg':'Mobileno Required'})
		else:
			counter = Pocket.objects.filter(Q(pocket_mobileno__exact = pocket_mobileno) ).select_related()
			customer_id = counter[0].customer_id
			per_id = counter[0].pocket_permanent_address_id			
			pre_id = counter[0].pocket_present_address_id


		if(per_id == None):
			#print('block-001')
			pa = PocketAddress()
			pa.reg_address = permanent_address
			pa.division = permanent_division
			pa.thana =  permanent_thana
			pa.district = permanent_district
			pa.postcode = permanent_postcode
			pa.longitude = ''
			pa.latitude = ''
			pa.save()

			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_permanent_address_id = pa.address_id )      
		else:
			if(permanent_address is not None and permanent_address !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(reg_address = permanent_address)
			if(permanent_division is not None and permanent_division !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(division = permanent_division)
			if(permanent_thana is not None and permanent_thana !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(thana = permanent_thana)
			if(permanent_district is not None and permanent_district !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(district = permanent_district)
			if(permanent_postcode is not None and permanent_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = per_id).update(postcode = permanent_postcode)

		#present address
		if(pre_id == None):

			paa = PocketAddress()
			paa.reg_address = present_address
			paa.division = present_division
			paa.thana =  present_thana
			paa.district = present_district
			paa.postcode = present_postcode
			paa.longitude = pocket_lon
			paa.latitude = pocket_lat
			paa.save()

			Pocket.objects.filter(customer_id__exact = customer_id).update(pocket_present_address_id = paa.address_id )      

		else:
			if(present_address is not None and present_address !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(reg_address = present_address)
			if(present_division is not None and present_division !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(division = present_division)
			if(present_thana is not None and present_thana !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(thana = present_thana)
			if(present_district is not None and present_district !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(district = present_district)
			if(present_postcode is not None and present_postcode !=''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(postcode = present_postcode)
			if(pocket_lat is not None and pocket_lat != ''):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(latitude=pocket_lat)
			if(pocket_lon is not None and pocket_lon !='' ):
				PocketAddress.objects.filter(address_id__exact = pre_id).update(longitude=pocket_lon)





		msg_body = "Profile address update complete"
		title = "Profile Update"
		fcm_token = counter[0].pocket_fcm_token
		msg_date = date.today().strftime('%A, %d %B %Y')
		custom_date = date.today().strftime('%Y-%m-%d')
		msg_time = time.strftime('%I:%M %p')
		print("%s"% fcm_token )
		if(fcm_token != ""):
			sendCommonNotification(fcm_token , title , msg_body , "sys" , msg_date ,custom_date , msg_time , "01300000000" , "" )



		
		return JsonResponse({'msg':'profile address updated.' }) 
	else:
		return JsonResponse({'error':'check your request method'})





#pocket registration api ui test
def pocketSignUpUI(request):
	context = {}
	return render(request , 'api/pocket_reg.html' , context)

#pin verification api test ui
def pocketPinVerificationUI(request):
	context = {}
	return render(request , 'api/pin_verification.html' , context)

#pocket transaction pin verification ui test
def pocketTransactionPINVerificationUI(request):
	context = {}
	return render(request , 'api/transaction_pin_verification.html' , context)

#pocket login ui test
def pocketLoginUI(request):
	context = {}
	return render(request , 'api/pocket_login.html' , context )

#pocket profile ui test
def pocketBasicProfileUI(request):
	context = {}
	return render(request , 'api/pocket_profile.html', context )
 
#pocket address profile ui test
def pocketAddressProfileUI(request):
	context = {}
	return render(request , 'api/pocket_address_profile.html' , context )



