# Generated by Django 2.1 on 2018-09-13 16:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0010_auto_20180913_1539'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_lat',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_lon',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
    ]
