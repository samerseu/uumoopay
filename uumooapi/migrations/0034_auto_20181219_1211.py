# Generated by Django 2.1 on 2018-12-19 12:11

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0033_customertransaction_service_fee_amount'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicefee',
            name='other_amount',
            field=models.DecimalField(decimal_places=2, default='0.00', max_digits=10),
        ),
        migrations.AddField(
            model_name='servicefee',
            name='vat_amount',
            field=models.DecimalField(decimal_places=2, default='0.00', max_digits=10),
        ),
    ]
