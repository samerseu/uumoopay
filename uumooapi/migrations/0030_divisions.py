# Generated by Django 2.1 on 2018-11-25 12:44

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0029_apiaesdetails_broken_key'),
    ]

    operations = [
        migrations.CreateModel(
            name='Divisions',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('division_name', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('division_bn_name', models.CharField(blank=True, default='', max_length=550, null=True)),
            ],
        ),
    ]
