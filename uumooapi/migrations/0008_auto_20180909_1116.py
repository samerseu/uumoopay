# Generated by Django 2.1 on 2018-09-09 11:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0007_cashbox_cashbox_fcm_token'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_nid_image',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_nominee_dob',
            field=models.DateField(default='1111-11-11'),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_nominee_vid_image',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_permanent_address_id',
            field=models.BigIntegerField(blank=True, null=True),
        ),
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_present_address_id',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
