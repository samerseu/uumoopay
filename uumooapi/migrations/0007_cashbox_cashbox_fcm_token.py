# Generated by Django 2.1 on 2018-09-05 17:27

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0006_auto_20180905_1547'),
    ]

    operations = [
        migrations.AddField(
            model_name='cashbox',
            name='cashbox_fcm_token',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
    ]
