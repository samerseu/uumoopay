# Generated by Django 2.1 on 2018-10-07 11:35

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0021_cashbox_cashbox_email'),
    ]

    operations = [
        migrations.AddField(
            model_name='pocket',
            name='pocket_agree_flag',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
        migrations.AddField(
            model_name='pocket',
            name='pocket_pin_verified_flag',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
    ]
