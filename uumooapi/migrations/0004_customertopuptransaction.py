# -*- coding: utf-8 -*-
# Generated by Django 1.11.14 on 2018-08-13 10:58
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0003_customeraccounting_transaction_status'),
    ]

    operations = [
        migrations.CreateModel(
            name='CustomerTopUpTransaction',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('topup_transaction_id', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_sender_userid', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_receiver_userid', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_transaction_amount', models.DecimalField(decimal_places=2, default='0.00', max_digits=10)),
                ('topup_transaction_type', models.CharField(blank=True, default='topup', max_length=550, null=True)),
                ('topup_transaction_message', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_cashbox_business_id', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_auxilary_userid', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_referral_id', models.CharField(blank=True, default='', max_length=550, null=True)),
                ('topup_transaction_status', models.CharField(blank=True, default='pending', max_length=550, null=True)),
                ('topup_transaction_initiator_request_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
                ('topup_transaction_completrion_request_timestamp', models.DateTimeField(default=django.utils.timezone.now)),
            ],
        ),
    ]
