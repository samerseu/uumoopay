# Generated by Django 2.1 on 2018-10-22 12:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0023_auto_20181011_1623'),
    ]

    operations = [
        migrations.AddField(
            model_name='servicefee',
            name='transaction_type_id',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
