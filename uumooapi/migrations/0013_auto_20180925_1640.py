# Generated by Django 2.1 on 2018-09-25 16:40

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('uumooapi', '0012_auto_20180916_1358'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='business',
            name='business_addrss_id',
        ),
        migrations.AddField(
            model_name='business',
            name='app_mobile_number',
            field=models.CharField(blank=True, default='', max_length=550, null=True),
        ),
        migrations.AddField(
            model_name='business',
            name='business_address_id',
            field=models.BigIntegerField(blank=True, null=True),
        ),
    ]
