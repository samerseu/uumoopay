from __future__ import unicode_literals

from django.db import models
from django.utils.timezone import now
# Create your models here.
import datetime


class Divisions(models.Model):
	division_name = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	division_bn_name= models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	def __unicode__(self):
		return u"%s" % self.division_bn_name

	
class Districts(models.Model):
	division_id = models.BigIntegerField(null=True ,blank=True)
	district_name = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	district_bn_name = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	def __unicode__(self):
		return u"%s" % self.district_bn_name



class Thanas(models.Model):
	district_id = models.BigIntegerField(null=True ,blank=True)
	thana_name = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	thana_bn_name = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	def __unicode__(self):
		return u"%s" % self.thana_bn_name
	




#customer_id use for mapping and pocket_id like as wallet id
#pocket_photo for profile picture
#pocket_verified for account active or deactive tracking
#pocket_status_activefrozen for user give greater than 3 times wrong password
#pocket information
class Pocket(models.Model):	
	customer_id  = models.CharField(max_length=550,default='******',primary_key=True)
	pocket_id    = models.CharField(max_length=550,default='******')          
	pocket_fname = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	pocket_mname = models.CharField(max_length=550 ,default=''   ,null=True ,blank=True)
	pocket_lname = models.CharField(max_length=550)
	pocket_father_name = models.CharField(max_length=550 , default='', null=True , blank=True)
	pocket_mobileno = models.CharField(max_length = 550)
	pocket_alternate_mobileno = models.CharField(max_length = 550,default=''   ,null=True ,blank=True)
	pocket_email = models.CharField(max_length = 550,default=''   ,null=True ,blank=True)
	pocket_dob 		= models.DateField()
	pocket_gender 	= models.CharField(max_length = 550,default=''   ,null=True ,blank=True)
	pocket_voterlD 	= models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	pocket_nid 		= models.CharField(max_length = 550 ,default=''  ,null=True ,blank=True)
	pocket_photo 	= models.CharField(max_length=550  ,default=''   ,null=True ,blank=True)	
	pocket_nid_image = models.CharField(max_length=550 ,default='' ,null=True ,blank=True)
	pocket_permanent_address_id = models.BigIntegerField(null=True ,blank=True)
	pocket_present_address_id = models.BigIntegerField(null=True ,blank=True)
	pocket_access_token = models.CharField(max_length = 550 , default=''  ,null=True ,blank=True)
	pocket_authorization_key = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	pocket_deviceid = models.CharField(max_length = 550)
	pocket_simid    = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	pocket_simid_two    = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	pocket_verified = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	pocket_pin_verified_flag = models.CharField(max_length=550, default='', null=True,blank=True)
	pocket_agree_flag = models.CharField(max_length=550, default='', null=True, blank=True)
	pocket_app_pass = models.CharField(max_length = 550)
	pocket_transaction_pass = models.CharField(max_length = 550 , default='' ,null=True ,blank=True)
	pocket_verification_pin = models.CharField(max_length = 550 , default='' ,null=True ,blank=True)
	pocket_qrcode = models.CharField(max_length = 550 , default=''   ,null=True ,blank=True)
	pocket_status_active_frozen = models.CharField(max_length = 550 ,default='' , null=True ,blank=True)
	pocket_reffered_by_customer_id = models.CharField(max_length = 550,default=''   ,null=True ,blank=True)
	pocket_nominee_fname = models.CharField(max_length = 550,default='' ,null=True ,blank=True )
	pocket_nominee_mname = models.CharField(max_length = 550,default='' ,null=True ,blank=True )
	pocket_nominee_lname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	nomineedobs   = models.DateField(default='1111-11-11')
	pocket_nominee_address = models.CharField(max_length = 550,default='',null=True,blank=True)
	pocket_nominee_contactno  = models.CharField(max_length = 550,default=''   ,null=True ,blank=True)
	pocket_nominee_voterid    = models.CharField(max_length = 550,default='' ,null=True ,blank=True )
	pocket_nominee_nid      = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	pocket_nominee_relation = models.CharField(max_length = 550,default='' ,null=True ,blank=True )
	pocket_nominee_voterid_scancopy = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	pocket_daily_transfer_limit = models.DecimalField(max_digits = 10 , decimal_places = 2 , default = '0.00')
	pocket_daily_receive_limit = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	pocket_monthly_transfer_limit = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	pocket_monthly_receive_limit = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	pocket_monthly_max_balance = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	pocket_customer_type = models.CharField(max_length=550,default='' ,null=True ,blank=True)
	pocket_fcm_token = models.CharField(max_length=550,default='',null=True,blank=True)
	pocket_registration_timestamp = models.DateTimeField(default = now )


#pocket address information
class PocketAddress(models.Model):
	address_id  = models.BigAutoField(primary_key=True)
	#customer_pocket_id = models.ForeignKey(Pocket , related_name = 'customer_pocketid')
	reg_address = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	division = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)	
	thana = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	district = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	postcode = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	longitude = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	latitude = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)	


class DrawerAddress(models.Model):
	address_id  = models.BigAutoField(primary_key=True)
	reg_address = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	division = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	thana = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	district = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	postcode = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	longitude = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	latitude = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)



#merchase user information 
class Cashbox(models.Model):
	cashbox_customer_id = models.CharField(max_length = 550,default='******',primary_key=True)	
	cashbox_id = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)	
	cashbox_fname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_mname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_lname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_father_name = models.CharField(max_length=550 , default='' , null=True, blank=True)
	cashbox_mobileno = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_email = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_alternate_mobileno = models.CharField(max_length=550 , default='' , null=True, blank=True)
	cashbox_dob = models.DateField()
	cashbox_gender = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nationalD = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_vodetid = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_vodetid_image = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nid_image = models.CharField(max_length=550 ,default='' ,null=True ,blank=True)
	cashbox_photo = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_access_token = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_authorization_key = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_deviceid = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_simid_one = models.CharField(max_length =550,default='', null=True ,blank=True)
	cashbox_simid_two = models.CharField(max_length=550,default='', null=True, blank=True)
	cashbox_verified = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_app_pass = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_transaction_pass = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_pin_verification_code = models.CharField(max_length=550 , default='' , null=True , blank=True) 
	cashbox_pin_verified_flag = models.CharField(max_length=550 , default='' , null=True , blank=True)
	cashbox_agree_flag = models.CharField(max_length=550 , default='' , null=True , blank=True)
	cashbox_qrcode = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_status_ative_frozen = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_business_id = models.BigIntegerField(null=True ,blank=True)
	cashbox_refferedby_customer_id = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True)
	cashbox_present_address_id = models.BigIntegerField(null=True , blank =True)
	cashbox_permanent_address_id = models.BigIntegerField(null=True , blank =True)
	customer_type_id = models.BigIntegerField(null=True ,blank=True)
	cashbox_nominee_dob = models.DateField(default="1111-11-11")
	cashbox_nominee_fname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_mname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_lname = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_address = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_contactno = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_relation = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_vid =  models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_nid = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	cashbox_nominee_vid_image = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	cashbox_fcm_token = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True)
	cashbox_lat = models.CharField(max_length=550 , default='', null=True, blank=True)
	cashbox_lon = models.CharField(max_length=550, default='', null=True, blank=True)
	cashbox_registration_timestamp = models.DateTimeField(default = now , blank = False)



#business information
class Business(models.Model):
	business_id = models.BigAutoField(primary_key=True)	
	app_mobile_number = models.CharField(max_length = 550 ,default='',null=True,blank=True)
	business_name = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_type = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_category = models.CharField(max_length = 550,default='', null=True,blank=True)#ex:export,import etc
	business_nid = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_nid_image = models.CharField(max_length = 550 , default = '' , null=True , blank=True)
	business_tradelicense = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_tradelicense_image  = models.CharField(max_length = 550 , default ='' , null=True, blank=True)
	business_tin = models.CharField(max_length=550 , default='' , null=True , blank=True)
	business_tin_image = models.CharField(max_length=550 , default='' , null=True, blank=True)	
	business_mobileno = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_email = models.CharField(max_length = 550,default='',null=True,blank=True)
	business_verified = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_approx_monthly_transaction_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	business_max_balance_amount = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	business_logo = models.CharField(max_length = 550,default='' ,null=True ,blank=True)
	business_address_id = models.BigIntegerField(null=True , blank=True)
	business_registration_timestamp = models.DateTimeField(default = now , blank = False)




class BusinessLogic(models.Model):
	business_logic_id = models.BigAutoField(primary_key=True)
	business_logic_title = models.CharField(max_length = 550)
	business_action_item_id = models.BigIntegerField(null=True ,blank=True)
	services_id = models.BigIntegerField(null=True ,blank=True)
	business_logic_created_by_admin_id = models.CharField(max_length=550 ,default=''  ,null=True ,blank=True)
	business_logic_creation_timestamp = models.DateTimeField(default = now , blank = False)



class SystemModules(models.Model):
	system_moduele_id  = models.BigAutoField(primary_key=True)
	system_moduele_name  = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	system_moduele_status  = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)



class CustomerModuleGroup(models.Model)	:
	customer_module_group_id = models.BigAutoField(primary_key=True)
	customer_module_id = models.BigIntegerField(null=True ,blank=True)
	customer_module_permission = models.BigIntegerField(null=True ,blank=True)
	customer_module_group_description = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	admin_user_id = models.CharField(max_length=550 , default=''  ,null=True ,blank=True)
	created_datetime = models.DateTimeField(default = now , blank = False)



class CustomerModulePermission(models.Model):
	customer_module_id = models.BigIntegerField()
	customer_module_name = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	customer_module_description = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	

#customer type
class CustomerType(models.Model):
	customer_type_id = models.BigAutoField(primary_key=True)
	customer_module_id = models.BigIntegerField(null=True ,blank=True)	
	customer_module_group_id = models.BigIntegerField(null=True ,blank=True)
	admin_user_id = models.CharField(max_length=550 ,default=''  ,null=True ,blank=True)
	customer_type_description = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	created_datetime = models.DateTimeField(default = now , blank = False)


#transaction type
class TransactionType(models.Model):
	transaction_type_id = models.BigIntegerField(primary_key=True)
	transaction_type_title = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	transaction_type_status = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	transaction_type_createdby_user_id = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)
	transaction_type_creation_timestamp = models.DateTimeField(default = now , blank = False)


#serice fees
class ServiceFee(models.Model):
	service_id = models.BigIntegerField(primary_key = True  )
	transaction_type_id = models.BigIntegerField(null=True ,blank=True)
	service_title = models.CharField(max_length = 550,default='',null=True ,blank=True)
	minimum_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	maximum_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	amount_range = models.CharField(max_length = 550,default='',null=True ,blank=True)
	service_fee_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	service_status = models.CharField(max_length = 550,default=''  ,null=True ,blank=True)	
	vat_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	other_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	service_created_by_used_id = models.CharField(max_length=550 ,default=''  ,null=True ,blank=True)
	service_creation_timestamp = models.DateTimeField(default = now , blank = False)


#comission rates
class Comission(models.Model):
	commission_id = models.BigIntegerField(primary_key = True  )
	commission_title = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	amount_range_min = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	amount_range_max = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	commission_fees = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	commission_status= models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	commission_created_by_used_id = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	commission_creation_timestamp = models.DateTimeField(default = now , blank = False)


#pocket and cashbox transaction
class CustomerTransaction(models.Model)	:
	customer_transaction_id =  models.CharField(max_length=550  ,default='' ,primary_key=True )
	sender_userid =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	receiver_userid = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	reference_txt = models.CharField(max_length=550 , default='' , null=True , blank=True)
	transaction_amount = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	service_fee_amount = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	transaction_type	=  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	transaction_message = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	cashbox_business_id =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )  #(contain name)
	auxilary_userid =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	referral_id =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	transaction_status = models.CharField(max_length=550 , default='',null=True, blank=True)
	sender_longitute = models.CharField(max_length=550,default='',null=True,blank=True)
	sender_latitude = models.CharField(max_length=550,default='',null=True,blank=True)
	receiver_longitute = models.CharField(max_length=550,default='',null=True,blank=True)
	receiver_latitude = models.CharField(max_length=550,default='',null=True,blank=True)
	transaction_timestamp = models.DateTimeField(default = now , blank = False)


#pocket and cashbox accounting
class CustomerAccounting(models.Model):
	userid = models.CharField(max_length=550  ,default=''  ,primary_key=True  )
	user_type_id = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	customeraccount_transaction_type = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	customeraccount_debit = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	customeraccount_credit = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	comission_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')  #(ex:per transaction fee)   
	commission_slave_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	service_fee_amount = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	customer_referral_withdrawl_amount =models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	customer_balance = models.DecimalField(max_digits = 10, decimal_places = 2, default = '0.00')
	business_id = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	auxilary_accounting_userid = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )  #(comma separated value)
	referral_id = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )  #(comma separated value)
	transaction_status =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True ) #(pending,paid)


#pocket and cashbox topup transaction log
class CustomerTopUpTransaction(models.Model):
	topup_transaction_id =  models.CharField(max_length=550  ,default='***********'  ,primary_key = True )
	topup_sender_userid =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_receiver_userid = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_reference_txt = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_transaction_amount = models.DecimalField(max_digits = 10 , decimal_places = 2, default = '0.00')
	topup_transaction_type	=  models.CharField(max_length=550  ,default='topup'  ,null=True ,blank=True )
	topup_transaction_message = models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_cashbox_business_id =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )  #(contain name)
	topup_auxilary_userid =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_referral_id =  models.CharField(max_length=550  ,default=''  ,null=True ,blank=True )
	topup_transaction_status = models.CharField(max_length=550 , default='pending',null=True, blank=True)
	topup_transaction_initiator_request_timestamp = models.DateTimeField(default = now , blank = False)	
	topup_transaction_completrion_request_timestamp = models.DateTimeField(default = now , blank = False)



#=================transaction messages==============================================
class TransactionMessage(models.Model):
	msg_id = models.CharField(max_length=550,default='',null=True,blank=True)
	transaction_type_id = models.CharField(max_length=550,default='',null=True,blank=True)
	receiver_type = models.CharField(max_length=550,default='',null=True,blank=True)#sender,receiver,pocket_all
	msg_body = models.CharField(max_length=550,default='',null=True,blank=True)
	

#================all aes encryption details ========================================
class APIAesDetails(models.Model):	
	aescode = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	broken_key = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	aes_title = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	app_name = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	admin_id = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	det_date = models.DateTimeField(default = now , blank = False)





#================== Security Question ======================================================================
class SecurityQuestion(models.Model):
	security_question_id = models.BigAutoField(primary_key=True )
	security_question = models.CharField(max_length = 600 , default = '' , null=True , blank = True)
	admin_id = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	det_date = models.DateTimeField(default = now , blank = False)


#======================== Sercurity Answer ================================================================
class SecurityAnswer(models.Model):
	answer_id = models.BigAutoField(primary_key=True )
	question_id = models.BigIntegerField(null=True ,blank=True)
	security_answer = models.CharField(max_length = 600  ,default=''  ,null=True , blank=True)
	admin_id = models.CharField(max_length=550  ,default=''  ,null=True , blank=True)
	answer_date = models.DateTimeField(default = now , blank = False)
	







