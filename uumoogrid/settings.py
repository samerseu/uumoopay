import os
import zlib


BKEY = 'hsedalgnabiytrewq321@ylilrabrud#'#'hsedalgnabiytrewq321@ylilrabrud#'
#'hsedalgnabiytrewq321' # qwertyi'

#======== AES Key for Pocket =======================
POCKET_LOGIN_KEY = ''
POCKET_APP_PASS_KEY = ''
POCKET_TRAN_PASS_KEY = ''
POCKET_PAYMENT_KEY = ''
POCKET_DEPOSIT_KEY = ''
POCKET_WITHDRAW_KEY = ''
POCKET_TRANSFER_KEY = ''

#======= AES Key for ==============================















#HOSTNAME = 'ec2-54-144-212-196.compute-1.amazonaws.com' 
HOSTNAME = 'ec2-54-237-160-80.compute-1.amazonaws.com'
DATABASE = 'uumoopay' #'uumoogrid_dev'
USERNAME = 'power_user'
PASSWORD = 'PsnT@uumoo2018'
PORTNO = '5432'



#APPEND_SLASH = False
PROJECT_ROOT = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid'             #'/home/ec2-user/uumoogrid/uumoogrid'
MEDIA_ROOT = '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/profilepic/'   # '/home/ec2-user/uumoogrid/uumoogrid/profilepic/'

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)
BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
#print BASE_DIR

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.10/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = 'w5iczjlg!yp8&*gr0c2^=_s_jjoki6nyc408)4)u+efbirvle+'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'corsheaders',
    'uumooapi' ,
    'uumooadmin' ,
    'fcm_django',
]

MIDDLEWARE = [
    'django.middleware.security.SecurityMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
]

ROOT_URLCONF = 'uumoogrid.urls'



TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': ['/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui'	 ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
		#'django.core.context_processors.csrf',
		#'django.core.context_processors.i18n',
		#'django.core.context_processors.media',
		#'django.core.context_processors.static',
                #'django.template.context_processors.debug',
                #'django.template.context_processors.request',
                'django.contrib.auth.context_processors.auth',
                #'django.contrib.messages.context_processors.messages',

            ],
        },
    },
]

WSGI_APPLICATION = 'uumoogrid.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.10/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.sqlite3',
        #'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'uumoopay',#'uumoogrid_dev' ,
        'USER':'power_user' ,
        'PASSWORD': 'PsnT@uumoo2018' ,
	'HOST':'ec2-54-237-160-80.compute-1.amazonaws.com',
        #'HOST': 'ec2-54-144-212-196.compute-1.amazonaws.com',
        'PORT': '5432' 
    }
}


# Password validation
# https://docs.djangoproject.com/en/1.10/ref/settings/#auth-password-validators

AUTH_PASSWORD_VALIDATORS = [
    {
        'NAME': 'django.contrib.auth.password_validation.UserAttributeSimilarityValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.MinimumLengthValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.CommonPasswordValidator',
    },
    {
        'NAME': 'django.contrib.auth.password_validation.NumericPasswordValidator',
    },
]


# Internationalization
# https://docs.djangoproject.com/en/1.10/topics/i18n/

LANGUAGE_CODE = 'en-us'
#TIME_ZONE = 'UTC'
TIME_ZONE = 'Asia/Dhaka'
USE_I18N = True
USE_L10N = True
USE_TZ = False #True


#======================== FCM KEYS ==================================
DRAWER_FCM_KEY = "AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F"


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.10/howto/static-files/

STATIC_URL = 'assets/' #'ui/webadmin/'
#STATIC_ROOT = os.path.join(PROJECT_ROOT , "ui/webadmin")



STATICFILES_DIRS = (
  #os.path.join(PROJECT_ROOT, 'ui/'),
  os.path.join(BASE_DIR, 'assets'),	
)


MEDIA_URL = 'profilepic/'
CORS_ALLOW_METHODS = (
    'DELETE',
    'GET',
    'OPTIONS',
    'PATCH',
    'POST',
    'PUT',
)
CORS_ORIGIN_ALLOW_ALL = True
CORS_ORIGIN_WHITELIST = (
	#'ec2-54-144-212-196.compute-1.amazonaws.com:80'
	'ec2-54-237-160-80.compute-1.amazonaws.com:80'
)


FCM_DJANGO_SETTINGS = {
        "FCM_SERVER_KEY": "AAAAKwrMdtQ:APA91bEfLZbLiouRb12vPqNIgtxJjmTwFHOhDWDy9UVcE4zGWwjm27j468PsVjoHBlOWgu2hXMRrMvk4JBXClh9sGUrdRiLvySC3PqObHSGaYyBz22WG9XFRhHrlzelxyzzF7WstAt6F"
}
