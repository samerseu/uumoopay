
from django.conf.urls import url , include
from uumooapi import views


urlpatterns = [
    #url(r'' , views.pocketSignUp) ,

    #UI for api rest	
    url(r'v1/pocket-registration/$' , 		views.pocketSignUpUI), #pocket reg ui
    url(r'v1/pin-verification/$' , 		views.pocketPinVerificationUI), #pin verification ui
    url(r'v1/transaction-pin-verification/$' , views.pocketTransactionPINVerificationUI), #transaction pin verification ui
    url(r'v1/pocket-login/$'		 , views.pocketLoginUI) , #pocket login ui
    url(r'v1/pocket-basic-profile/$' 	, views.pocketBasicProfileUI) , #pocket basic profile  ui
    url(r'v1/pocket-address-profile/$'  , views.pocketAddressProfileUI) , #pocket address profile  ui
    url(r'v1/pocket-nominee-profile/$'  , views.pocketNomineeProfileUI) , #pocket nominee profile  ui



    #api test responses	
    url(r'v2/pocket-registration/$' , 		views.pocketSignUp) , #pocket reg api
    url(r'v2/pin-verification/$' , 		views.pocketPinVerification) , #pin verification api
    url(r'v2/transaction-pin-verification/$' ,  views.pocketTransactionPinVerification) , #transaction pin verification api    
    url(r'v2/pocket-login/$' 			, views.pocketLogin) , #pocket login api
    url(r'v2/pocket-basic-profile/$' , 		  views.pocketBasicProfile), #pocket profile update api
    url(r'v2/pocket-address-profile/$' , 	  views.pocketAddressProfile), #pocket address profile update api
    url(r'v2/pocket-nominee-profile/$' ,            views.pocketNomineeProfile), #pocket nominee profile update api
    url(r'v2/pocket-nominee-profile-information/$' , views.pocketNomineeInfo), #pocket nominee profile information api

    url(r'v2/balance-transfer/$', 		views.balanceTransfer), #balance transfer api
    url(r'v2/pocket-image-upload/$', 		views.imageFileseInformationUpdate) , #pocket image upload information update api	
    url(r'v2/pocket-qr-code-scan/$' , 		views.qrCodeScan) , #pocket qr code scan api
    url(r'v2/pocket-account-deactivate/$' , 	views.threeTimeWrongPassword) , #pocket account deactivate (3 times wrong password type) api
    

    #transaction part	
    url(r'v2/balance-transfer/$', 		views.balanceTransfer), #balance transfer api  	
    url(r'v2/balance-pay/$',                    views.balancePay), #balance pay api
    url(r'v2/balance-topup-pocket-request-to-cashbox/$', views.balanceTopUpWalletRequestToMerchant), 
    #balance topup wallet request to merchant api


    url(r'v2/balance-cashout/$' ,		 views.cashOutRequest ),
    url(r'v2/balance-query/$',  views.customerCurrentBalance ),

    url(r'v2/transaction-list/$' , views.transactionList),
    url(r'v2/qr-code-pdf-version/$', views.qrCodePdfVersion),


    #forgot app password	
    url(r'v2/forgot-password/$' ,       views.forgotPassword) ,
    url(r'v2/forgot-password-update/$', views.forgotPasswordUpdate) , 


    #forgot transaction password
    url(r'v2/forgot-transaction-pin/$', views.forgotTransactionPin ),
    url(r'v2/forgot-transaction-pin-update/$' , views.forgotTransactionPINUpdate) ,


    #forgot both
    url(r'v2/forgot-both/$', views.forgotBoth ),


    #for test
    url(r'v2/sample-qr/$', views.sampleQR), #test for qr with logo checking,	
    url(r'v2/check-header/$', views.testAes ) ,#checkHeaderInfo) ,#test for header checking,
  

    #======================== test =============================================
    url(r'v2/enc/$' , views.dbEncryptTest),
    url(r'v2/dec/$' , views.dbDecryptTest),

   
]

