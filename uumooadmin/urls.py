from django.conf import settings
from django.conf.urls import url , include 
from uumooadmin import views
from django.conf.urls.static import static
from django.contrib.staticfiles.urls import staticfiles_urlpatterns

urlpatterns = [
    	
    url(r'test/$' , views.testPage ),
    url(r'login/$', views.loginAdmin ) , #admin web login,
    url(r'login-auth/$', views.loginAction ),
    url(r'home/$' , views.loginHomeAdmin ) ,	      
    url(r'logout/$', views.logoutAction ) , 
    url(r'users/$', views.usersListAdmin ) ,
    url(r'pocket-users-list/$', views.pocketUsersListAdmin ) ,
    url(r'pocket-users-list/<str:customerid>/$', views.pocketUsersListAdmin ) ,
    url(r'drawer-users-list/$', views.drawerUsersListAdmin ) , 
    url(r'api-encryption-keys/$', views.keyListAdmin ),
    url(r'user-profile/(?P<userid>\w+)/$', views.userProfileAdmin ) ,
    url(r'pocket-user-profile/$', views.pocketUserProfileviewAdmin ) , 
    url(r'drawer-user-profile/(?P<userid>\w+)/$', views.drawerUserProfileviewAdmin ) ,
    url(r'service-fees/$' , views.serviceFeeListAdmin ) , 
    url(r'service-fee-edit/$', views.serviceFeeEditAdmin ) , 

    url(r'aes-key-edit/$', views.aesApiKeyEditAdmin ) ,
    url(r'transaction-type-list/$' , views.transactionTypesListAdmin ) ,
    url(r'security-questions-list/$', views.securityQuestionListAdmin ) ,
    url(r'add-security-question/$', views.securityQuestionAddAdmin ) ,
]


urlpatterns += staticfiles_urlpatterns()



#if settings.DEBUG: # will be 'True' in developmen
 #   urlpatterns += static(settings.STATIC_URL, document_root=settings.PROJECT_ROOT)







