# -*- coding: utf-8 -*-
from __future__ import unicode_literals



import json
import requests

import urllib.request
from urllib.parse import parse_qs
from django.contrib import messages
from django.db.models import Q ,Subquery , Sum , Prefetch
from uumooapi.models import * 
from django.utils import *
from django.shortcuts import render,redirect
from django.conf import settings
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from uumoogrid.settings import MEDIA_URL ,HOSTNAME,DATABASE,USERNAME,PASSWORD,PORTNO,BKEY , DRAWER_FCM_KEY



#======================================================================================================


def securityQuestionListAdmin(request):
	if "mobile" in request.session:
		questions = SecurityQuestion.objects.all()
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'questions':questions }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/question_list.html' , context)
		
	else:
		return redirect("/grid/login")


def securityQuestionAddAdmin(request):
	if "mobile" in request.session:
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/question_entry.html' , context)
	else:
		return redirect("/grid/login")



def transactionTypesListAdmin(request):
	if "mobile" in request.session:
		transaction_types = TransactionType.objects.all()

		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'transaction_types':transaction_types }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/transaction_type_list.html' , context )
	else:
		return redirect('/grid/login/')





def aesApiKeyEditAdmin(request):
	if "mobile" in request.session:
		try:
			all_params = request.get_full_path()
			first_part , second_part  = all_params.split('?')
			params = parse_qs(second_part)
			keyid = params['keyid']
			keyid  = ''.join(keyid )
		except:
			keyid = None




		aes_keys = APIAesDetails.objects.filter(Q(id__exact = keyid)).select_related()

		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL  ,'aescode': aes_keys[0].aescode , 'broken_key': aes_keys[0].broken_key ,'aes_title': aes_keys[0].aes_title ,'app_name': aes_keys[0].app_name  }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/aes_key_edit.html' , context )
	else:
		return redirect('/grid/login/')





def serviceFeeEditAdmin(request):
	if "mobile" in request.session:
		try:
			all_params = request.get_full_path()
			first_part , second_part  = all_params.split('?')
			params = parse_qs(second_part)
			serviceid = params['serviceid']
			serviceid  = ''.join(serviceid )
		except:
			serviceid = None


		print("service id: "+ serviceid )
		service_fees = ServiceFee.objects.filter(Q(service_id__exact = serviceid )).select_related()
				
	
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL  , 'service_id': str(service_fees[0].service_id ),'service_type_id':service_fees[0].transaction_type_id, 'service_title': service_fees[0].service_title, 'service_fee_amount': service_fees[0].service_fee_amount , 'max_amount': service_fees[0].maximum_amount ,'min_amount': service_fees[0].minimum_amount , 'vat_amount': service_fees[0].vat_amount  , 'other_amount': service_fees[0].other_amount   }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/service_fee_edit.html' , context )	
	else:
		return redirect('/grid/login')




def drawerUsersListAdmin(request):
	if "mobile" in request.session:
		#callurl = "http://"+ HOSTNAME + "/api/web/drawer-list/"
		#r = requests.get(callurl , data=request.GET)
		#data = r.json()
		#drawers = data['drawers']

		try:
			all_params = request.get_full_path()
			first_part , second_part  = all_params.split('?')
			params = parse_qs(second_part)
			customerid = params['customerid']
			customerid = ''.join(customerid)
		except:
			customerid = None



		pagination_string = ""
		current_page = request.GET.get('page' ,1)
		limit = 5 * int(current_page)
		offset = int(limit) - 5
		business_list = Business.objects.all().values('business_id')
		print(business_list)

		if not customerid:						
			contact_list = Cashbox.objects.raw('select 1 as sid, uc.cashbox_customer_id,uc.*,ub.* from uumooapi_cashbox uc inner join uumooapi_business ub  on (uc.cashbox_business_id = ub.business_id) order by uc.cashbox_customer_id')

			print(contact_list)
			
			total_contacts = 6
			print("block 1 - %s"%(customerid))
		else:
			print("block 2 - %s"%(customerid))
			contact_list = Cashbox.objects.raw('select 1 as sid , uc.cashbox_customer_id,uc.*,ub.* from uumooapi_cashbox uc inner join uumooapi_business ub  on (uc.cashbox_business_id = ub.business_id) order by uc.cashbox_customer_id')
			total_contacts = 10



		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'drawers': contact_list }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/drawer_users_list.html' , context )
	else:
		return redirect('/grid/login/')








#==============================================================================================================================

def pocketUsersListAdmin(request , customerid = None):
	if "mobile" in request.session:

		#callurl = "http://"+ HOSTNAME + "/api/web/pocket-list/"
		#r = requests.get(callurl , data=request.GET)
		#data = r.json()
		#contacts = data['pockets']

		try:
			all_params = request.get_full_path()
			first_part , second_part  = all_params.split('?')
			params = parse_qs(second_part)
			customerid = params['customerid']
			customerid = ''.join(customerid)
		except:
			customerid = None			




		pagination_string = ""
		current_page = request.GET.get('page' ,1)
		limit = 5 * int(current_page)
		offset = int(limit) - 5

		if not customerid:
			contact_list = Pocket.objects.values('customer_id','pocket_fname','pocket_lname','pocket_mname','pocket_mobileno','pocket_alternate_mobileno','pocket_email','pocket_dob','pocket_photo','pocket_verified','pocket_qrcode','pocket_registration_timestamp')[offset:limit]
			total_contacts = Pocket.objects.values('customer_id','pocket_fname','pocket_lname','pocket_mname','pocket_mobileno','pocket_alternate_mobileno','pocket_email','pocket_dob','pocket_photo','pocket_verified','pocket_qrcode','pocket_registration_timestamp').count()  
			print("block 1 - %s"%(customerid))
		else:
			print("block 2 - %s"%(customerid))
			contact_list = Pocket.objects.filter(Q(customer_id__exact = customerid) | Q(pocket_mobileno__exact = customerid)).values('customer_id','pocket_fname','pocket_lname','pocket_mname','pocket_mobileno','pocket_alternate_mobileno','pocket_email','pocket_dob','pocket_photo','pocket_verified','pocket_qrcode','pocket_registration_timestamp')[offset:limit]
			total_contacts = Pocket.objects.filter(Q(customer_id__exact = customerid) | Q(pocket_mobileno__exact = customerid)).values('customer_id','pocket_fname','pocket_lname','pocket_mname','pocket_mobileno','pocket_alternate_mobileno','pocket_email','pocket_dob','pocket_photo','pocket_verified','pocket_qrcode','pocket_registration_timestamp').count()



		total_pages = int(total_contacts) / 5

		if int(total_contacts) % 5 != 0:
			total_pages += 1 
			pagination_string = make_pagination_html(current_page, total_pages)




		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'pockets': contact_list, 'pagination': pagination_string  }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/pocket_users_list.html' , context )
	else:
		return redirect('/grid/login/')








#=================================================================================================================================

def serviceFeeListAdmin(request):
	if "mobile" in request.session:
		callurl = "http://"+ HOSTNAME + "/api/web/service-fee-list/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()
		service_fee_list_details = data['service_fee_list_details']
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'service_fee_list_details':service_fee_list_details  }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/service_fee_list.html' , context )
	else:
		return redirect('/grid/login/')






#=========================================================================================

def keyListAdmin(request):
	if "mobile" in request.session:
		callurl = "http://"+ HOSTNAME + "/api/web/key-list/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()
		aes_details = data['list_details']
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'list_details': aes_details}
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/aes_keys_list.html' , context )
	else:
		return redirect('/grid/login/')




#========== login ui ==================================================================================================================
def loginAdmin(request):
	if "mobile" in request.session:
		return redirect('/grid/home/')
	else:
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/login.html' , context )




#============================================================================================
def usersListAdmin(request):
	if "mobile" in request.session:
		callurl = "http://"+ HOSTNAME + "/api/web/pocket-list/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()

		pockets = data['pockets']
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'pockets': pockets }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/users_list.html' , context )
	else:
		return redirect('/grid/login/')		



#====================================================================================================================================

def userProfileAdmin(request , userid= None):
	if "mobile" in request.session:
		print("%s"%(userid))

		callurl = "http://"+ HOSTNAME + "/api/web/pocket-user-profile-data?userid="+userid +"/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()
		print(data)		

		p = Pocket.objects.filter(Q(customer_id__exact = userid)).first()
		deposits = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10003')).select_related()
		transfers = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10001')).select_related()
		payments = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10002')).select_related()
		withdraws = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10004')).select_related()
		
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'p': p , 'deposits': deposits ,'payments':payments , 'transfers':transfers , 'withdraws': withdraws }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/user_profile.html' , context )
	else:
		return redirect('/grid/login/')


#==================================================================================================================================

def drawerUserProfileviewAdmin(request , userid = None):
	if "mobile" in request.session:
		callurl = "http://"+ HOSTNAME + "/api/web/pocket-user-profile-data?userid="+userid +"/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()
		print("%s %s"%(userid , data))


		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL  }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/pocket_user_profile.html' , context )
	else:
		return redirect('/grid/login/')




#======================================================================================================================================

def pocketUserProfileviewAdmin(request ):
	if "mobile" in request.session:
		all_params = request.get_full_path()
		first_part , second_part  = all_params.split('?')
		params = parse_qs(second_part)
		userid = params['userid']
		userid = ''.join(userid)

		cat = CustomerAccounting.objects.filter(Q(userid__exact = userid) & Q(user_type_id__exact = '101')).select_related()
		try:
			user_current_balance = cat[0].customer_balance
		except:
			user_current_balance = 0.00
		

		print("pocket id : %s "% (userid))
		p = Pocket.objects.filter(Q(customer_id__exact = userid)).first()
		print(p.pocket_fname)
		deposits = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10003') & Q(sender_userid__exact=userid)).order_by('-transaction_timestamp').select_related()
		transfers = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10001') & (Q(sender_userid__exact=userid) | Q(receiver_userid__exact=userid)) ).order_by('-transaction_timestamp').select_related()
		payments = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10002') & Q(sender_userid__exact=userid)).order_by('-transaction_timestamp').select_related()
		withdraws = CustomerTransaction.objects.filter(Q(transaction_type__exact = '10004') & Q(sender_userid__exact=userid)).order_by('-transaction_timestamp').select_related()


		trans_dates = CustomerTransaction.objects.values('transaction_timestamp').distinct()
		print(trans_dates)
		
		
		withdraws_list = []
		for wl in withdraws:
			pr = customerNameDetail(wl.receiver_userid)
			ss = customerNameDetail(wl.sender_userid)
			if(ss == ''):
				ss_name = ''
				ss_photo = ''
				ss_phone = ''
			else:
				ss_name,ss_photo,ss_phone = ss.split('|')


			if(pr==0):
				name = ''
				photo = ''
				phone = ''
			else:
				name,photo,phone = pr.split('|')


			withdraws_data_handler = {
				'customer_transaction_id': wl.customer_transaction_id ,
				'sender_name': ss_name ,
				'sender_phone': ss_phone ,
				'receiver_name': name ,
				'receiver_phone': phone ,
				'receiver_photo': photo ,
				'transaction_amount': wl.transaction_amount ,
				'service_fee': wl.service_fee_amount ,
				'reference_txt': wl.reference_txt ,
				'transaction_timestamp': wl.transaction_timestamp
			}
			withdraws_list.append(withdraws_data_handler)






		transfers_list = []
		for tl in transfers:
			pr = customerNameDetail(tl.receiver_userid)
			ss = customerNameDetail(tl.sender_userid)
			print("receiver:%s sender:%s"%(tl.receiver_userid, tl.sender_userid))
			if(ss ==0):
				ss_name = ''
				ss_photo = ''
				ss_phone =''
			else:
				ss_name,ss_photo,ss_phone = ss.split('|')



			if(pr==0):
				name = ''
				photo =''
				phone = ''
			else:
				name,photo,phone = pr.split('|')


			print("r:%s s:%s"%(name,ss_name))
			transfers_data_handler = {
				'customer_transaction_id': tl.customer_transaction_id ,
				'sender_name': ss_name ,
				'sender_phone': ss_phone ,
				'receiver_name': name ,
				'receiver_phone': phone ,
				'receiver_photo': photo ,
				'transaction_amount': tl.transaction_amount ,
				'service_fee': tl.service_fee_amount ,				
				'reference_txt': tl.reference_txt ,
				'transaction_timestamp': tl.transaction_timestamp
			}
			transfers_list.append(transfers_data_handler)




		deposits_list = []
		if deposits.count() > 0:
			for dp in deposits:
				pr = customerNameDetail(dp.receiver_userid)
				ss = customerNameDetail(dp.sender_userid)
				print("receiver:%s sender:%s"%(dp.receiver_userid, dp.sender_userid))
				if(ss ==0):
					ss_name = ''
					ss_photo = ''
					ss_phone =''
				else:
					ss_name,ss_photo,ss_phone = ss.split('|')



				if(pr==0):
					name = ''
					photo =''
					phone = ''
				else:
					name,photo,phone = pr.split('|')


				print("r:%s s:%s"%(name,ss_name))
				deposits_data_handler = {
					'customer_transaction_id': dp.customer_transaction_id ,
					'sender_name': ss_name ,
					'sender_phone': ss_phone ,
					'receiver_name': name ,
					'receiver_phone': phone ,
					'receiver_photo': photo ,
					'transaction_amount': dp.transaction_amount ,
					'service_fee': dp.service_fee_amount ,
					'reference_txt': dp.reference_txt ,
					'transaction_timestamp': dp.transaction_timestamp
				}
				deposits_list.append(deposits_data_handler)



		payments_list = []
		for pp in payments:
			pr = customerNameDetail(pp.receiver_userid)
			ss = customerNameDetail(pp.sender_userid)
			if(ss ==0):
				ss_name = ''
				ss_photo = ''
				ss_phone =''
			else:
				ss_name,ss_photo,ss_phone = ss.split('|')

			if(pr==0):
				name = ''
				photo =''
				phone = ''
			else:
				name,photo,phone = pr.split('|')			



			payments_data_handler = {
				'customer_transaction_id': pp.customer_transaction_id ,
				'sender_name': ss_name ,
				'sender_phone': ss_phone ,
				'receiver_name': name ,
				'receiver_phone': phone ,
				'receiver_photo': photo ,
				'transaction_amount': pp.transaction_amount ,
				'service_fee': pp.service_fee_amount ,
				'reference_txt': pp.reference_txt ,
				'transaction_timestamp': pp.transaction_timestamp
			}
			payments_list.append(payments_data_handler)









		
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'current_balance':user_current_balance , 'pocket_fname': p.pocket_fname,'pocket_mname': p.pocket_mname , 'pocket_lname': p.pocket_lname , 'pocket_mobileno': p.pocket_mobileno ,'pocket_dob': p.pocket_dob,'pocket_photo': p.pocket_photo ,'pocket_verified': p.pocket_verified ,'pocket_qrcode': p.pocket_qrcode ,'pocket_father_name': p.pocket_father_name , 'pocket_alternate_mobileno': p.pocket_alternate_mobileno , 'pocket_email': p.pocket_email , 'pocket_gender': p.pocket_gender , 'pocket_voterlD': p.pocket_voterlD , 'pocket_nid': p.pocket_nid ,'pocket_registration_timestamp': p.pocket_registration_timestamp , 'deposits': deposits_list ,'payments': payments_list , 'transfers': transfers_list , 'withdraws': withdraws_list }
		return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/pocket_user_profile.html' , context )
	else:
		return redirect('/grid/login/')









#========== login action ========================================================================================================
def loginAction(request):
	context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL}
	if request.method == 'POST':			
		data = request.POST
		if not data['mobile']:
			context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'error':'Mobile Number cannot be emmpty' }			
			request.session['mob_val'] = data['mobile']
			return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/login.html' , context )
		elif not data['cred']:
			context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'error':'Password cannot be emmpty' }
			request.session['mob_val'] = data['mobile']
			return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/login.html' , context )

		elif(data['mobile'] != 'uumoadmin'):
			context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'error':'Invalid Username' }
			return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/login.html' , context )
		elif(data['cred'] != 'admin#123#'):
			context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL , 'error':'Invalid Password' }
			return render(request , '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/login.html' , context )
		else:
			print("%s"%(data))
			request.session['mobile'] = data['mobile']
			request.session['status'] = 1
			print("session data: %s "%(data['mobile']))

			callurl = "http://"+ HOSTNAME + "/api/web/dashboard-data/"
			r = requests.get(callurl , data=request.GET)
			data = r.json()
			print(data)
			total_pocket = data['total_pocket']
			total_cashbox = data['total_cashbox']
			total_deposits = data['total_deposits']
			total_transfers = data['total_transfers']
			total_payments = data['total_payments']
			total_withdraws = data['total_withdraws']
			total_withdraw_service_fees = data['total_withdraw_service_fees']
			total_deposit_service_fees = data['total_deposit_service_fees']
			total_payment_service_fees = data['total_payment_service_fees']
			total_transfer_service_fees = data['total_transfer_service_fees']


			print("%s %s %s %s"%(total_withdraw_service_fees,total_deposit_service_fees,total_payment_service_fees,total_transfer_service_fees))


			context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'total_pocket': total_pocket , 'total_cashbox':total_cashbox , 'total_deposits': total_deposits , 'total_transfers': total_transfers , 'total_payments':total_payments , 'total_withdraws': total_withdraws, 'total_withdraw_service_fees': total_withdraw_service_fees , 'total_deposit_service_fees': total_deposit_service_fees , 'total_payment_service_fees': total_payment_service_fees , 'total_transfer_service_fees': total_transfer_service_fees}

			return render(request, '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/index.html' , context)
	else:
		return render(request, '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/index.html' , context)
        




#====================================================================================================================================

def loginHomeAdmin(request):
	if "mobile" in request.session:
		callurl = "http://"+ HOSTNAME + "/api/web/dashboard-data/"
		r = requests.get(callurl , data=request.GET)
		data = r.json()
		total_pocket = data['total_pocket']
		total_cashbox = data['total_cashbox']
		total_deposits = data['total_deposits']
		total_transfers = data['total_transfers']
		total_payments = data['total_payments']
		total_withdraws = data['total_withdraws']

		total_withdraw_service_fees = data['total_withdraw_service_fees']
		total_deposit_service_fees = data['total_deposit_service_fees']
		total_payment_service_fees = data['total_payment_service_fees']
		total_transfer_service_fees = data['total_transfer_service_fees']

		print("session data: %s " %( request.session['mobile'] ) )
		context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL ,'total_pocket': total_pocket , 'total_cashbox':total_cashbox , 'total_deposits': total_deposits , 'total_transfers': total_transfers , 'total_payments':total_payments , 'total_withdraws': total_withdraws , 'total_withdraw_service_fees': total_withdraw_service_fees , 'total_deposit_service_fees': total_deposit_service_fees , 'total_payment_service_fees': total_payment_service_fees , 'total_transfer_service_fees': total_transfer_service_fees  }
		return render(request, '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/index.html' , context)
	else:
		return redirect('/grid/login/')




#===============================================================================================================================

def logoutAction(request):
	request.session.flush()
	return redirect('/grid/login/')



#================================================================================================================================
def testPage(request):
	context = {'BASE_DIR': settings.BASE_DIR , 'PROJECT_ROOT':  settings.PROJECT_ROOT , 'STATIC_URL': settings.STATIC_URL }
	return render(request, '/home/ec2-user/uumoo_devgrid/uumoogrid/uumoogrid/ui/webadmin/pages/test.html' , context)	





#============================================ Helper Function  ======================================================================
def customerNameDetail(userid):	
	c = Pocket.objects.filter( customer_id__exact=userid).select_related()
	if(c.count() > 0 ):
		return c[0].pocket_fname+' '+c[0].pocket_lname+'|'+c[0].pocket_photo+'|'+c[0].pocket_mobileno
	else:
		cc = Cashbox.objects.filter(cashbox_customer_id__exact=userid).select_related()
		if(cc.count() >0):
			bc = Business.objects.filter(Q(business_id__exact = cc[0].cashbox_business_id )).select_related()
			if(bc.count() == 0 ):
				fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				photo = cc[0].cashbox_photo
				mobile = cc[0].cashbox_mobileno
			else:
				mobile = cc[0].cashbox_mobileno
				if(bc[0].business_name == ''):
					fname = cc[0].cashbox_fname+' '+cc[0].cashbox_lname
				else:
					fname = bc[0].business_name


				if(bc[0].business_logo == ''):
					photo = cc[0].cashbox_photo
				else:
					photo = bc[0].business_logo



			return fname +'|'+ photo+'|'+mobile
			#return cc[0].cashbox_fname+' '+cc[0].cashbox_lname+'|'+cc[0].cashbox_photo+'|'+cc[0].cashbox_mobileno
		else: 
			return 0



#================================== Pagination Maker ========================================================
def make_pagination_html(current_page, total_pages):
	pagination_string = ""
	if int(current_page) > 1:
		pagination_string += '<a href="?page=%s">previous</a>' % (int(current_page) -1)
		pagination_string += '<span class="current"> Page %s of %s </span>' %(current_page, total_pages)
	if int(current_page) < int(total_pages):
		pagination_string += '<a href="?page=%s">next</a>' % ( int(current_page) + 1)


	return pagination_string

